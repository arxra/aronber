# Structure

Here, all courses have their own folder.
Depending on the course, the folder might contain subfolders of the course material,
or they might just contain assignments.

## II2202

Course on scientific writing.
Its folder contains scripts for generating and simulating IoT devices sending
payloads over either TCP+TLS or QUIC, our project topic.
The simulation software creatively referred to as `bencher`, and is written in rust heavily
inspired by example clients and servers for the protocols.
