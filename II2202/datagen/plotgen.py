import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

pattern = ["//", "..", "++"]


def mean(df0, df1, dftt):
    plot([df0.mean().rename("QUIC 0RTT"), df1.mean().rename("QUIC handshake"), dftt.mean().rename("TLS+TCP")], "mean")


def median(df0, df1, dftt):
    plot(
        [df0.median().rename("QUIC 0RTT"), df1.median().rename("QUIC handshake"), dftt.median().rename("TLS+TCP")],
        "median",
    )


def plot(df, title):
    # Getting the patterns on the graphs were not easy,
    # I blame pythons loose types for the fault and the solution.
    # https://stackoverflow.com/questions/59585813/how-to-to-add-stacked-bar-plot-hatching-in-pandas-or-how-to-get-barcontaine
    ax = pd.concat(df, axis=1).plot.bar()
    bars = [t for t in ax.containers if isinstance(t, mpl.container.BarContainer)]

    for (bar, pat) in zip(bars, range(0, len(bars))):
        for patch in bar:
            patch.set_hatch(pattern[pat])
    ax.legend()
    plt.ylabel("# Packets sent")
    plt.xlabel("Drop risk (%)")
    plt.yticks(np.arange(0, 24, step=2.0))
    plt.grid(linestyle='--')
    plt.title(title)

    plt.savefig(f"{title}.png")


if __name__ == "__main__":
    # Set the hatch width to smaller so the graphs are clearer
    mpl.rcParams['hatch.linewidth'] = 0.05  # previous pdf hatch linewidth
    mpl.rcParams['hatch.linewidth'] = 0.5  # previous svg hatch linewidth

    cols = range(1, 6)  # The first col is the packetID, which is irrelevant
    df0 = pd.read_csv("quic-0rtt.csv", usecols=cols)
    df1 = pd.read_csv("quic-handshake.csv", usecols=cols)
    dftt = pd.read_csv("tcp+tls-handshake.csv", usecols=cols)

    mean(df0, df1, dftt)
    median(df0, df1, dftt)
