import csv
from enum import Enum
from typing import List

import numpy as np


class Proto(Enum):
    # Either off:
    # quick with 0-rtt handshake,
    # Quic but you need to perform the handshake,
    # TLS+TCP where the handshake is always needed,
    Q0 = 0
    Q1 = 1
    TT = 2

    # We don't need to resend all the packets, but if either of these do not arrive,
    # wee need to resend that many packets
    def seq(self):
        if self == Proto.Q0:
            return [2]
        elif self == Proto.Q1:
            return [3, 2]
        elif self == Proto.TT:
            return [3, 4, 2]
        return []

    def string(self):
        if self == Proto.Q0:
            return "QUIC + 0-RTT"
        elif self == Proto.Q1:
            return "QUIC with handshake"
        elif self == Proto.TT:
            return "TCP+TLS"
        return ""


def generate_total_packets(
    exp_droped_packs: int = 1,
    proto: Proto = Proto.Q0,
) -> int:
    """
    Generates the total amount of packets
    :param exp_dropped_pack: dropped packages for protocol
    :param proto:
    """

    result = 0

    def try_seq(element: int) -> bool:
        # if exp_droped_packs == 0:
        #     return True

        odds: float = (1 - (exp_droped_packs / 100)) ** element
        rand: float = np.random.rand()
        # print( f"failed the odds check (odds: {odds}, rand: {rand}, proto: {proto.string()}, droprate: {exp_droped_packs}")
        if rand > odds:
            return False
        return True

    for element in proto.seq():
        while True:
            result += element
            if try_seq(element):
                break
    # print(f"generated {result} packets for {proto}: drop:{exp_droped_packs}")
    return result


def create_data_file(
    file_str: str,
    proto: Proto,
    droprates: List[int],
    payloads: int = 1000,
):
    tot_packs = {}

    for i in range(0, payloads):
        dropped: List[int] = []
        for d in droprates:
            dropped.append(
                generate_total_packets(exp_droped_packs=d, proto=proto)
            )  # assumes they are ran in droprate order
        tot_packs[i] = dropped

    with open(file_str + ".csv", "w") as file:
        writer = csv.writer(file)
        writer.writerow(["Simulated droprates"] + [str(d) for d in droprates])
        for i in range(0, payloads):
            # each row will be the packet number followed by the packets sent for each droprate
            writer.writerow([str(i)] + tot_packs[i])


if __name__ == "__main__":
    dropps = [0, 1, 5, 10, 25]
    create_data_file("quic-0rtt", Proto.Q0, dropps)
    create_data_file("quic-handshake", Proto.Q1, dropps)
    create_data_file("tcp+tls-handshake", Proto.TT, dropps)

    #  We need data vs the normallydistrubuted tie for packets to travel over the link.
