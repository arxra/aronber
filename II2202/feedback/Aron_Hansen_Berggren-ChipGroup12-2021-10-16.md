# Opposition report
Opponent’s name: Aron Hansen Berggren
Opposition for group: Chip group 12

Keep in mind that your feedback is essential to each of these other groups,
as your feedback will enable them to improve their report and guide them as to issues to raise in their oral presentation.
Providing and receiving feedback is an essential part of your participation in the course.

## Review question: 1. Critique the organization, structure, and layout of the report
- Relevant keywords (good).
- See comments about the results.

## Review question: 2. Critique the abstract
- If the streams API was released in 2014 (java8), is it fair to call it "newly introduced" in 2021?
- The overall structure is good, was very easy to follow.
- Good English.
- There are still placeholders here.

## Review question: 3. Critique the literature study
- "In comparison, the native mode compiles Java code into a standalone binary
executable or a native shared library and the Java byte code that is processed during the native image build
includes all application classes, dependencies, third party dependent libraries, and any JDK classes that are
required." is a very long sentence. The following line suffers even worse from this.
Tip: Try reading the whole line without taking a breath. If you can't, then the line should most likely be split and/or rephrased.

## Review question: 4. Critique the method (or methods) used
- Clear tables on hardware and software used.
- Clear how the numbers were generated, the link to the source on GitHub is also nice.
- How the scripts were run is missing.

## Review question: 5. Critique the results and analysis
- How the scripts were run is a Methods part, not a result.
- Interesting that the "regular" JVM is not included, only the JVM mode of graalvm.
- Still placeholder comments about the work.
- Nice with short results, we could learn a bit or 2 about that.
- The Y axis of the figures and the figure descriptions are redundant and make the graphs look bad.
Would suggest changing the graph labels to just Duration [ms] and move that information to the figure caption.

## Review question: 6. Critique the discussion
- Still placeholder comments about the work.
- The reference to the book feels a bit out of place without page reference.
- The equation looks like a Result, which could now be referenced instead.
There is plenty of space in the results section for it and since the numbers are from those graphs that would make it more meaningful.

## Review question: 7. Critique the conclusions: Are they relevant, meaningful, and follow from the discussion? Who should act based upon these conclusions, and what should they do?
The conclusion is good, it shortly summarize the claims, why you believe in them and why we should as well. 
They summarize the discussion quite well and I have no issues with it.
It is also clear what is missing: Parallelism.

## Review question: 8. Critique the planned future work (if any)
While not listed as a separate section, the parallelism in the conclusion is a natural follow up.

## Review question: 9. Are the references appropriate, complete, and used correctly? Are there any missing references?
- [3] is just a reference to the release site of java, which does not contain notes for java8.
A more accurate reference would be too the ["new in this release" page of java8.](https://www.oracle.com/java/technologies/javase/8-whats-new.html)
- [7] is leading to a 400+ page book, page numbers or at least a chapter had been nice.
- [11] is missing its accessed tag.

## Review question: 10. Now is the time to identify anything false, incorrect, misleading, or unclear. What issues did you find?
 - There are only benefits of GraalVM being listed, it would be nice with a part of what you loose out on by using it (if any).
 - In the appendix, a relatively short code snippet is cut in half on a new page with white space in the bottom of they page.
 It could clearly fit in one page without being split.
 - The pdf links to the GitHub repository are broken and take you to the title page.
 This is bad when it is referred to as simply `*` in the appendix, as you don't know which page to look for the link. 
 With it being named just as `*` my reaction was to go back and search for it instead of scrolling all the way to the bottom to find the correct link.
 - The English of the report is excellent, but some sentences could be shortened.
