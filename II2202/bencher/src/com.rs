use std::{fs, path::Path};

use anyhow::Context;
use rustls::ServerCertVerified;
use tokio_rustls::webpki;

pub const ALPN_QUIC_HTTP: &[&[u8]] = &[b"hq-29"];

pub fn generate_self_signed_cert(
    cert_path: &Path,
    key_path: &Path,
) -> anyhow::Result<(quinn::Certificate, quinn::PrivateKey)> {
    // Generate dummy certificate.
    let certificate = rcgen::generate_simple_self_signed(vec!["localhost".into()]).unwrap();
    let serialized_key = certificate.serialize_private_key_der();
    let serialized_certificate = certificate.serialize_pem().unwrap();

    // Write to files.
    fs::write(cert_path, &serialized_certificate).context("failed to write certificate")?;
    fs::write(key_path, &serialized_key).context("failed to write private key")?;

    let cert = quinn::Certificate::from_pem(&serialized_certificate.into_bytes())?;
    let key = quinn::PrivateKey::from_der(&serialized_key)?;
    Ok((cert, key))
}
