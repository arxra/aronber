use std::{path::PathBuf, usize};

use argh::FromArgs;

#[derive(PartialEq, Debug, Clone)]
pub enum Protocol {
    Tcp,
    Quic,
}

impl argh::FromArgValue for Protocol {
    fn from_arg_value(value: &str) -> Result<Self, String> {
        match value {
            "tcp" | "Tcp" | "TCP" => Ok(Self::Tcp),
            "quic" | "Quic" | "QUIC" => Ok(Self::Quic),
            _ => Err("Could not parse protocol".to_string()),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum Mode {
    Server,
    Client,
}

impl argh::FromArgValue for Mode {
    fn from_arg_value(value: &str) -> Result<Self, String> {
        match value {
            "client" | "Client" => Ok(Self::Client),
            "server" | "Server" => Ok(Self::Server),
            _ => Err("Could not parse mode".to_string()),
        }
    }
}

#[derive(FromArgs, Clone, Debug)]
/// A program which benchmarks simulatied IoT devices using either quic or TCP
pub struct Options {
    /// protocol
    #[argh(positional, short = 'p')]
    pub proto: Protocol,

    /// the mode of the application
    #[argh(positional, short = 'm')]
    pub mode: Mode,

    /// bind addr for server, remote server addr for client
    #[argh(positional, short = 'a')]
    pub addr: String,

    /// bind port for server, remote server addr for client
    #[argh(positional, long = "port")]
    pub port: u16,

    /// cert file
    #[argh(option, short = 'c')]
    pub cert: PathBuf,

    /// key file
    #[argh(option, short = 'k')]
    pub key: PathBuf,

    /// key file
    #[argh(option, short = 'd', long = "devices", default = "10")]
    pub devs: usize,

    /// how many messages to send per devices
    #[argh(option, default = "5")]
    pub runs: usize,

    /// should the quic server allow stateless retries?
    #[argh(option, default = "true")]
    pub stateless_retry: bool,

    /// generate certificates at locations instead of reading
    #[argh(option, short='n', default="false")]
    pub gen_cert: bool
}
