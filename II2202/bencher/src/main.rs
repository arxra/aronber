#![allow(unused_variables)]

mod client;
mod com;
mod run_menu;
use anyhow::anyhow;
use std::net::ToSocketAddrs;
use tokio::fs;
mod server;

use run_menu::Options;

use crate::{
    client::{spawn_quic, spawn_tcp, ClientOpts},
    run_menu::Protocol,
};

#[tokio::main(worker_threads = 1)]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();
    let args: run_menu::Options = argh::from_env();
    let opts = ClientOpts::new(args.clone())?;
    if args.gen_cert {
        com::generate_self_signed_cert(&args.cert, &args.key)?;
    }

    if args.create_certs{
        com::generate_self_signed_cert(&args.cert, &args.key)?;
    }

    run_match(args, opts).await?;

    Ok(())
}

async fn run_match(args: Options, opts: ClientOpts)-> anyhow::Result<()> {
    let data = b"Hello";
    match args.proto {
        Protocol::Tcp => {
            let tcp = server::Tcp { opts: args.clone() };
            match args.mode {
                run_menu::Mode::Server => {
                    tcp.spawn_server().await?;
                }
                run_menu::Mode::Client => {
                    let mut futures = Vec::new();
                    for a in 0..args.devs {
                        let fut = spawn_tcp(opts.clone(), data);
                        let fut = tokio::spawn(fut);
                        futures.push(fut);
                    }
                    let mut time: std::time::Duration = std::time::Duration::ZERO;
                    for fut in futures {
                        time = time.checked_add(fut.await??).unwrap();
                    }
                    println!("time for tcp: {:?}", time);
                }
            };
        }
        Protocol::Quic => match args.mode {
            run_menu::Mode::Server => server::spawn_quic(&args).await?,
            run_menu::Mode::Client => {
                let mut futures = Vec::new();
                let remote = (args.addr.clone(), args.port)
                    .to_socket_addrs()?
                    .next()
                    .ok_or_else(|| anyhow!("Could not reolve address"))?;

                let cert_der = quinn::Certificate::from_der(&fs::read(&args.cert).await?)?;
                let certs = vec![cert_der.clone()];

                for a in 0..args.devs {
                    // FIXME: we are reading the cert for earch client atm. We could just pass the values as references
                    let fut = spawn_quic(args.clone(), data);
                    futures.push(tokio::spawn(fut));
                }
                let mut time: std::time::Duration = std::time::Duration::ZERO;
                for fut in futures {
                    time = time.checked_add(fut.await??).unwrap();
                }
                println!("time for quic: {:?}", time);
            }
        },
    };
    Ok(())
}
