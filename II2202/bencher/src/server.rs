use anyhow::{Context, Result};
use futures_util::{StreamExt, TryFutureExt};

use rustls::{internal::pemfile::certs, Certificate, NoClientAuth, ServerConfig};
use std::{fs::File, io::BufReader, net::ToSocketAddrs, path::Path, sync::Arc};
use tokio::{
    io::{self, AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, TcpStream},
};
use tokio_rustls::{TlsAcceptor, TlsStream};
use tracing::{debug, error, info, instrument};

use crate::com::ALPN_QUIC_HTTP;
use crate::run_menu::Options;

/// Loads a der formatted private key
async fn load_key(path: &Path) -> rustls::PrivateKey {
    let key_bytes = tokio::fs::read(path).await.unwrap();
    rustls::PrivateKey(key_bytes)
}

/// load pem formatted certificate
fn load_certs(path: &Path) -> io::Result<Vec<Certificate>> {
    certs(&mut BufReader::new(File::open(path)?))
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidInput, "invalid cert"))
}

/// Spawns a quic server, just await it and it will keep allowing connections.
#[instrument]
pub async fn spawn_quic(opts: &Options) -> Result<()> {
    info!("Starting quic server...");
    let mut transport_config = quinn::TransportConfig::default();
    transport_config.max_concurrent_uni_streams(0_u8.into())?;
    let mut server_config = quinn::ServerConfig::default();
    server_config.transport = Arc::new(transport_config);
    let mut server_config = quinn::ServerConfigBuilder::new(server_config);
    server_config.protocols(ALPN_QUIC_HTTP);
    server_config.enable_keylog();

    if opts.stateless_retry {
        server_config.use_stateless_retry(true);
    }

    let key = std::fs::read(&opts.key).context("failed to read private key")?;
    let key = if opts.key.extension().map_or(false, |x| x == "der") {
        quinn::PrivateKey::from_der(&key)?
    } else {
        quinn::PrivateKey::from_pem(&key)?
    };
    let cert_chain = std::fs::read(&opts.cert).context("failed to read certificate chain")?;
    let cert_chain = if opts.cert.extension().map_or(false, |x| x == "der") {
        quinn::CertificateChain::from_certs(Some(
            quinn::Certificate::from_der(&cert_chain).unwrap(),
        ))
    } else {
        quinn::CertificateChain::from_pem(&cert_chain)?
    };
    server_config.certificate(cert_chain, key)?;

    let mut endpoint = quinn::Endpoint::builder();
    endpoint.listen(server_config.build());

    let addr = &((opts.addr.as_str(), opts.port)
        .to_socket_addrs()?
        .next()
        .expect("bindport not available"));

    let (endpoint, mut incoming) = endpoint.bind(addr)?;

    while let Some(conn) = incoming.next().await {
        info!("connection incoming");
        tokio::spawn(handle_connection(conn).unwrap_or_else(move |e| {
            error!("connection failed: {reason}", reason = e.to_string())
        }));
    }
    Ok(())
}

// This is already running in a seperate green thread from the server
async fn handle_connection(conn: quinn::Connecting) -> Result<()> {
    let quinn::NewConnection {
        connection,
        mut bi_streams,
        ..
    } = conn.await?;
    async {
        info!("established");
        // Each stream initiated by the client constitutes a new request.
        while let Some(stream) = bi_streams.next().await {
            let (mut writer, read) = match stream {
                Err(quinn::ConnectionError::ApplicationClosed { .. }) => {
                    info!("connection closed");
                    return Ok(());
                }
                Err(e) => {
                    return Err(e);
                }
                Ok(s) => s,
            };

            let data = read.read_to_end(50).await.unwrap_or_default();
            let res = writer.write_all(b"World").await;
            writer.finish().await.unwrap();
            info!(
                "Got message from remote: {:?}",
                std::str::from_utf8(&data).unwrap()
            );
        }
        Ok(())
    }
    .await?;
    debug!("Connection handled");
    Ok(())
}

#[derive(Clone, Debug)]
pub struct Tcp {
    pub opts: Options,
}

impl Tcp {
    pub async fn spawn_server(&self) -> Result<()> {
        let mut config = ServerConfig::new(NoClientAuth::new());
        let key = load_key(&self.opts.key).await;
        config
            .set_single_cert(load_certs(&self.opts.cert)?, key)
            .map_err(|err| io::Error::new(io::ErrorKind::InvalidInput, err))?;
        let acceptor = TlsAcceptor::from(Arc::new(config));
        let addr = (self.opts.addr.as_str(), self.opts.port)
            .to_socket_addrs()?
            .next()
            .ok_or_else(|| io::Error::from(io::ErrorKind::AddrNotAvailable))?;
        let listener = TcpListener::bind(&addr).await?;

        loop {
            let (stream, peer_addr) = listener.accept().await?;
            let acceptor = acceptor.clone();
            let stream = acceptor.accept(stream).await?;
            // Handle the connection in a "green" tokio thread, give it ownership of the connection
            tokio::spawn(handle_tcp_call(tokio_rustls::TlsStream::Server(stream)));
        }
    }
}

async fn handle_tcp_call(mut stream: TlsStream<TcpStream>) -> Result<()> {
    let mut buf = Vec::new();
    stream.read_to_end(&mut buf).await?;
    stream.write(b"World").await?;
    stream.shutdown().await?;
    Ok(())
}
