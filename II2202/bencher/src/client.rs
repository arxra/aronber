use anyhow::{anyhow, Result};
use std::io::BufReader;
use std::net::ToSocketAddrs;
use std::sync::Arc;
use std::time::Duration;
use std::time::Instant;
use std::{fs::File, net::SocketAddr};
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;
use tokio_rustls::{webpki::DNSNameRef, TlsConnector};
use tracing::debug;

use crate::com::{SkipCertificationVerification, ALPN_QUIC_HTTP};
use crate::run_menu::Options;

#[derive(Clone)]
pub struct ClientOpts {
    opts: Options,
    domain: DNSNameRef<'static>,
    addr: SocketAddr,
    config: tokio_rustls::rustls::ClientConfig,
}

impl std::fmt::Debug for ClientOpts {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ClientOpts")
            .field("opts", &self.opts)
            .field("domain", &self.domain)
            .field("addr", &self.addr)
            .finish()
    }
}

impl ClientOpts {
    pub fn new(opts: Options) -> Result<Self> {
        // generate the data we will send first
        let domain = "localhost".to_string();
        let data = format!("GET / HTTP/1.0\r\nHost: {}\r\n\r\n", domain);
        let addr = (opts.addr.as_str(), opts.port)
            .to_socket_addrs()?
            .next()
            .ok_or_else(|| anyhow!("Failed to parese data"))?;
        let mut config = tokio_rustls::rustls::ClientConfig::new();
        let mut pem = BufReader::new(File::open(&opts.cert)?);
        config
            .root_store
            .add_pem_file(&mut pem)
            .map_err(|_| anyhow!("invalid cert"))?;
        let domain =
            DNSNameRef::try_from_ascii_str("localhost").map_err(|_| anyhow!("invalid dnsname"))?;

        Ok(Self {
            opts,
            domain,
            addr,
            config,
        })
    }
}

pub async fn spawn_tcp(opts: ClientOpts, data: &[u8]) -> Result<Duration> {
    // Set up variables for runs
    let config = Arc::new(opts.config);

    // Only count time for each "run"
    let start = Instant::now();
    for _ in 0..opts.opts.runs {
        let connector = TlsConnector::from(Arc::new(opts.config.clone()));
        let tls_stream = TcpStream::connect(opts.addr).await?;
        debug!("Stream open, attaching TRLS");
        let mut tcp_stream = connector.connect(opts.domain, tls_stream).await?;
        debug!("TLS Stream open, sendin data: {:?}", data);
        tcp_stream.write_all(data).await?;
        tcp_stream.shutdown().await?;
    }
    Ok(start.elapsed())
}

pub async fn spawn_quic(opts: Options, data: &[u8]) -> Result<Duration> {
    let host = "localhost";

    debug!("Before cert read");
    // let cert = quinn::Certificate::from_der(&fs::read(&opts.cert).await?)?;
    let cert = quinn::CertificateChain::from_pem(&fs::read(&opts.cert).await?)?
        .iter()
        .next()
        .unwrap()
        .clone();

    let mut endpoint = quinn::Endpoint::builder();
    let mut client_config = quinn::ClientConfigBuilder::default();
    client_config.enable_0rtt();
    client_config.add_certificate_authority(quinn::Certificate::from(cert))?;
    client_config.protocols(ALPN_QUIC_HTTP);
    client_config.add_certificate_authority(cert)?;
    let cfg = client_config.build();
    let addr = &"[::]:0".parse()?;

    let start = Instant::now();
    for _ in 0..runs {
        // Bind endpoint to a UDP socket
        let mut endpoint = quinn::Endpoint::builder();
        endpoint.default_client_config(cfg.clone());
        let (endpoint, _) = endpoint.bind(addr)?;
        debug!("Endpoint bound");

        // Open a connection to the server
        // let new_conn = endpoint.connect_with(cfg.clone(), &remote, host)?.await?;
        let new_conn = endpoint.connect(&remote, host)?.await?;
        debug!("Connected");

        debug!("connected at {:?}", start.elapsed());
        let quinn::NewConnection {
            connection: conn, ..
        } = new_conn;

        let (mut send, recv) = conn.open_bi().await?;
        debug!("Opened a unidirectional stream in client");

        send.write_all(data).await?;
        debug!("Sent data to server: {:?}", data);
        send.finish().await?;

        let res = recv.read_to_end(50).await?;
        debug!("From server: {:?}", std::str::from_utf8(&res));
    }

    Ok(start.elapsed())
}
