# tiny-duty-bencher

This is a application which aims to benchmark the duty cycle for using TCP+TLS or QUIC for IoT devices.
To achieve meaning full numbers, it measures the amount of time from being scheduled to send a request until the server responds with accepting the data.

## Building

This is a pure rust application, as such it is built using cargo:
`cargo build --release`
Doing this will compile the binary for your current platform in `./target/release/` as `bencher`.

## Running

The application can easily be started using mode, protocol and certificates as parameter's after it has been built:

`SSLKEYLOGFILE="/tmp/keylog.txt" ./target/release/bencher quic server localhost 9000 --cert "local.pem" --key "local.der" -n true`

Adding the `-n true` flag will create new certificates as listed in Certificates below.
The environment variable `SSLKEYLOGFILE` allows us to take that path and give it to network analysis tools such as wireshark,
in which keys for decrypting the streams will be saved.

See `./bencher --help` for more information.

## Tokio

We are using "green threads", since we need a async runtime for our network connections anyways we'll let tokio manage this.
The main time will still be waiting for network and timers.
["Tasks in Tokio are very lightweight. Under the hood, they require only a single allocation and 64 bytes of memory. Applications should feel free to spawn thousands, if not millions of tasks."](https://tokio.rs/tokio/tutorial/spawning)

## Quinn

We are using `quinn::TransportConfig`, which has the following disclaimer:

```
In some cases, performance or resource requirements can be improved by tuning these values to suit a particular application and/or network connection.
In particular, data window sizes can be tuned for a particular expected round trip time, link capacity, and memory availability.
Tuning for higher bandwidths and latencies increases worst-case memory consumption, but does not impair performance at lower bandwidths and latencies.
The default configuration is tuned for a 100Mbps link with a 100ms round trip time.
```

Since we do not care about best possible throughput given we have small payloads, we will not be tuning these.
See end of line 3, "`.. does not impair performance at lower bandwidths and latencies`".
We'll take the word of the protocol implementers here..
The intent is to simulate IoT devices where this amount of latency is not unheard of.
However, it requires a network simulation layer between the server and the client to achieve these conditions,
such as loss rates to simulate noisy wireless applications and delays for far away remote IoT master servers.

## Certificates

You can pass any names for the certificates.
As long as the files exist (you may touch them first) the certificates can be generated
with the -n flag set to true: `-n true` thanks to the ["rcgen crate"](https://crates.io/crates/rcgen).
