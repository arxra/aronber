import se.kth.id1020.*;

import java.util.*;

public class CompleteBFS {
    Graph g;
    boolean edgeWeights;
    ArrayList<Vertex> lastPassage = new ArrayList<>();


    CompleteBFS(Graph g, boolean edgeWeights) {
        this.g = g;
        this.edgeWeights = edgeWeights;
    }

    public double findPathTo(String from, String to) {
        boolean[] vis = new boolean[g.numberOfVertices()];
        double[] distM = new double[g.numberOfVertices()];
        ArrayList<Vertex> nodes = new ArrayList<>();
        Arrays.fill(vis, false);
        Arrays.fill(distM, 1.0);
        Vertex work;

        for (Vertex vertex : g.vertices())
            if (vertex.label.equals(from)) {
                nodes.add(vertex);

                while (!nodes.isEmpty()) {
                    work = nodes.remove(0);
                    nodes.remove(work);

                    for (Edge e : g.edges()) {
                        if (!vis[e.from] && e.to == work.id) {
                            vis[e.from] = true;
                            nodes.add(g.vertex(e.from));
                            distM[e.from] = distM[e.to] + 1.0;
                            if (g.vertex(e.from).label.equals(to))
                                return distM[e.from];

                        }

                    }
                }
            }
        return 0.0;
    }
}