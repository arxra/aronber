import se.kth.id1020.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class CountGraphsBFS {
    int count = 0;

    public CountGraphsBFS(Graph g) {
        boolean[] vis = new boolean[g.numberOfVertices()];
        Queue<Vertex> queueue = new LinkedList<>();
        Arrays.fill(vis, false);


        if (g.equals(null)) return;
        for (Vertex v : g.vertices()) {
            /* if already visited, don't do anything */
            if (!vis[v.id]) {
                count++;
                vis[v.id] = true;
                queueue.add(v);
            /* Go though all it's neighbors */
                while (!queueue.isEmpty()) {
                    Vertex pop = queueue.remove();
                    for (Edge e : g.edges()) {
                        if (e.to == pop.id && !vis[e.from]) {
                            queueue.add(g.vertex(e.from));
                            vis[e.from] = true;
                        }
                    }
                }
            }
        }
    }
}










