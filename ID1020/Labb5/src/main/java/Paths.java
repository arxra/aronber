import se.kth.id1020.Graph;
import se.kth.id1020.DataSource;

public class Paths {
    public static void main(String[] args) {
        Graph g = DataSource.load();
        // work on g
        String from = "Renyn";
        String to = "Parses";
        CompleteBFS CBFS = new CompleteBFS(g, false);
        Dijkstra Dij = new Dijkstra(g);

        CountGraphsBFS k = new CountGraphsBFS(g);
        System.out.print(k.count);

        System.out.println("\nfrom " + from + " to " + to + " it's :" + CBFS.findPathTo(from, to));
        System.out.println("\nfrom " + from + " to " + to + " it's :" + Dij.findPathTo(from, to));
    }
}