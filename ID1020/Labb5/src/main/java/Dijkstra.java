import se.kth.id1020.*;

import java.util.*;

public class Dijkstra {
    Graph g;
    PriorityQueue<queueEmnt> queue = new PriorityQueue<>();

    Dijkstra(Graph g) {
        this.g = g;
    }

    private class queueEmnt implements Comparable<queueEmnt> {
        int id;
        int parr;
        double dist;

        queueEmnt(int id, int parr, double dist) {
            this.id = id;
            this.parr = parr;
            this.dist = dist;
        }

        public queueEmnt(int id, double distance) {
            this.id = parr = id;
            this.dist = distance;
        }

        @Override
        public int compareTo(queueEmnt queueEmnt) {
            return Double.compare(dist, queueEmnt.dist);
        }
    }

    public double findPathTo(String from, String to) {
        int[] parent = new int[g.numberOfVertices()];
        double[] distM = new double[g.numberOfVertices()];
        Arrays.fill(distM, Double.POSITIVE_INFINITY);
        int taregt = 0;
        int start = 0;

        for (Vertex v : g.vertices()) {
            if (v.label.equals(from)) {
                queue.offer(new queueEmnt(v.id, 0.0));
                start = v.id;
            } else if (v.label.equals(to))
                taregt = v.id;
        }

        while (!queue.isEmpty() && distM[taregt] == Double.POSITIVE_INFINITY) {
            int work = queue.peek().id;
            int parr = queue.peek().parr;
            double dist = queue.poll().dist;

            if(distM[work] != Double.POSITIVE_INFINITY) continue;

            distM[work] = dist;
            parent[work] = parr;

            for (Edge e : g.edges())
                if (e.from == work && distM[e.to] == Double.POSITIVE_INFINITY) {
                    queue.add(new queueEmnt(e.to, work, e.weight + dist));
                }
        }

        int nodesInPath = 1;
        while (taregt != start) {
            nodesInPath++;
            taregt = parent[taregt];
        }
        return nodesInPath;
    }
}