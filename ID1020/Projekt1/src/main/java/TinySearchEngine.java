import se.kth.id1020.TinySearchEngineBase;
import se.kth.id1020.util.Attributes;
import se.kth.id1020.util.Document;
import se.kth.id1020.util.Sentence;
import se.kth.id1020.util.Word;

import java.util.*;

public class TinySearchEngine implements TinySearchEngineBase {
    final String[] parses = {"count", "popularity", "occurrence"};
    HashMap<String, ArrayList<docEntry>> index = new HashMap<>();
    private Stack<List<docEntry>> parse = new Stack<>();
    private List<Document> result = new ArrayList<>();

    @Override
    public void preInserts() {

    }

    @Override
    public void insert(Sentence sentence, Attributes attributes) {
        for (Word w : sentence.getWords()) {
            if (!index.containsKey(w.word))
                index.put(w.word.toLowerCase(), new ArrayList<>());

            Boolean found = false;
            if (!index.get(w.word.toLowerCase()).isEmpty())
                for (docEntry e : index.get(w.word.toLowerCase()))
                    if (e.attr.document.equals(attributes.document)) {
                        e.count++;
                        found = true;
                    }
            if (!found)
                index.get(w.word.toLowerCase()).add(new docEntry(w, attributes));
        }
    }

    @Override
    public void postInserts() {

    }

    @Override
    public List<Document> search(String query) {

        result.clear();
        parse.clear();
        final char[] operators = {'+', '|', '-'};
        Stack<Character> ops = new Stack<>();
        Boolean gonSort = false;
        Boolean wasOP = false;
        Boolean wasWord = false;

        for (String s : query.split("\\s+")) {
            s = s.toLowerCase();
            if (gonSort)
                sort(s);
            else if (s.equals("orderby"))
                gonSort = true;

            for (char c : operators)
                if (c == s.charAt(0)) {
                    ops.push(s.charAt(0));
                    wasOP = true;
                    wasWord = false;
                    break;
                }
            if (!wasOP) {
                parse.add(index.get(s));

                if (wasWord)
                    while (parse.size() > 1)
                        parse.add(quaryParser(parse.pop(), parse.pop(), ops.pop()));
                wasWord = true;
            }

            wasOP = false;
        }
        for (List<docEntry> l : parse) {
            for (docEntry d : l)
                result.add(d.attr.document);
        }
        return result;
    }


    @Override
    public String infix(String s) {
        return null;
    }

    void sort(String sort) { //Should only be called when the last element in the parse stack is left, otherwise it's a faulty query.
        for (String e : parses)
            if (e.equals(sort)) {
                if (sort.equals(parses[0]))
                    bubbenSort(parse.peek(), docEntry.countSort);
                else if (sort.equals(parses[1]))
                    bubbenSort(parse.peek(), docEntry.popSort);
                else if (sort.equals(parses[2]))
                    bubbenSort(parse.peek(), docEntry.occurSort);
            }
    }

    void bubbenSort(List<docEntry> arg, Comparator<docEntry> compiz) {   //bubbelsort using diffrent type of comparitors for dffrent sorts.
        Boolean swapped = true;                                          //Because using the same code for multiple things is more efficient.

        while (swapped) {
            swapped = false;
            for (int i = 0; i < arg.size() - 1; )
                if (compiz.compare(arg.get(i), arg.get(++i)) > 0) {
                    docEntry temp = arg.get(i);
                    arg.set(i, arg.get(i - 1));
                    arg.set(i - 1, temp);
                    swapped = true;
                }
        }
    }

    void removeDupilcates(List<docEntry> lst) {
        HashSet<docEntry> s = new HashSet<>();
        for (int i = 0; i < lst.size(); )
            if (s.contains(lst.get(i))) lst.remove(i);
            else s.add(lst.get(i++));
    }

    private List<docEntry> quaryParser(List<docEntry> pop, List<docEntry> pop1, char op) {

        List<docEntry> docs = new ArrayList<>();

        switch (op) {
            case ('|'):
                docs.addAll(pop);
                docs.addAll(pop1);
                removeDupilcates(docs);
                break;
            case ('-'):
                for (docEntry doc : pop1) {
                    docs.add(doc);
                    for (docEntry notdoc : pop)
                        if (doc.attr.document.equals(notdoc.attr.document))
                            docs.remove(doc);
                }
                break;
            case ('+'):
                for (docEntry d1 : pop)
                    for (docEntry d2 : pop1)
                        if (d1.attr.document.equals(d2.attr.document))
                            docs.add(d1);
                break;

        }
        removeDupilcates(docs);
        return docs;

        /*
        for (int i = 0; i < arg.size(); i++)
            if (arg.get(i).equals("orderby")) {
                for (int add = 0; add < i; add++)
                    if (index.containsKey(add)) docEntryList.addAll(index.get(add));

                if (arg.size() > i + 2 && arg.get(i + 2).equals("desc"))
                    reverse = true;

                removeDupilcates(docEntryList);
                if (i + 1 < arg.size() && arg.get(i + 1).equals(parses[0]))
                    bubbenSort(docEntryList, docEntry.countSort);
                else if (i + 1 < arg.size() && arg.get(i + 1).equals(parses[1]))
                    bubbenSort(docEntryList, docEntry.popSort);
                else if (i + 1 < arg.size() && arg.get(i + 1).equals(parses[2]))
                    bubbenSort(docEntryList, docEntry.occurSort);

                for (docEntry d : docEntryList)
                    docs.add(d.attr.document);
                if (reverse)
                    Collections.reverse(docs);
                return docs;
            }
        for (String k : arg) {
            int mid = findIndex(k);
            if (mid != -1)
                docEntryList.addAll(index.get(mid).entry);
        }
        removeDupilcates(docEntryList);
        for (docEntry d : docEntryList)
            docs.add(d.attr.document);
            */
    }

    static class docEntry {

        static Comparator<docEntry> countSort = new Comparator<docEntry>() {
            public int compare(docEntry fem, docEntry t1) {
                return Integer.compare(fem.count, t1.count);
            }
        };
        static Comparator<docEntry> occurSort = new Comparator<docEntry>() {
            public int compare(docEntry fem, docEntry t1) {
                return Integer.compare(fem.attr.occurrence, t1.attr.occurrence);
            }
        };
        static Comparator<docEntry> popSort = new Comparator<docEntry>() {
            public int compare(docEntry fem, docEntry t1) {
                return Integer.compare(fem.attr.document.popularity, t1.attr.document.popularity);
            }
        };
        Word word;
        Attributes attr;
        int count = 1;

        docEntry(Word word, Attributes attr) {

            this.word = word;
            this.attr = attr;
        }
    }
}