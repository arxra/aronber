public class Node {
    Node(char data) {
        this.data = data;
    }

    Node() {
    }

    Node[] branches = new Node['z'+1];
    char data;
    Integer val = 0;
    int passed = 1;

}

class trieentry implements java.util.Map.Entry<String, Integer>, Comparable<trieentry> {
    private String key;
    private Integer value;


    public trieentry(String key, Integer value) {
        this.key = key;
        this.value = value;

    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }

    public void setKey(String key){
        this.key=key;
    }

    public Integer setValue(Integer fem) {
        this.value = fem;
        return fem;
    }
    @Override
    public int compareTo(trieentry other) {
        return value.compareTo(other.value);
    }
}