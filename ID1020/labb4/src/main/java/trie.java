import java.util.*;

public class trie {
    Node root = new Node();

    public void Put(String k) {
        Node currnode = root;
        for (char item : k.toLowerCase().toCharArray()) {
            int pos = halp.toint(item);
            if (pos > 'z'-'a')
                return;

            if (currnode.branches[pos] == null)
                currnode.branches[pos] = new Node(item);
            else if (halp.toint(item) <= 'z')
                currnode.branches[pos].passed++;
            currnode = currnode.branches[pos];
        }
        currnode.val++;
    }

    public Node GetNode(String k) {
        Node currnode = root;
        for (char item : k.toCharArray())
            if (currnode.branches[halp.toint(item)] != null)
                currnode = currnode.branches[halp.toint(item)];
        if (currnode != null)
            return currnode;
        return new Node();
    }

    public int Get(String k) {
        return GetNode(k).val;
    }

    public int Count(String k) {  //implement
        return GetNode(k).passed;
    }

    public int Distinct(String k) {
        return Distinct(GetNode(k));
    }

    public int Distinct(Node nood) {
        int returnval = 0;
        if (nood.branches == null)
            return 1;
        else
            for (Node item : nood.branches)
                if (item != null)
                    returnval += Distinct(item);
        if (nood.val > 0)
            returnval += 1;
        return returnval;
    }

    public trieIterator getIter(String i) {
        return new trieIterator(GetNode(i));
    }

    public class trieIterator implements java.util.Iterator<Map.Entry<String, Integer>> {
        Stack<NodeShell> stack = new Stack<NodeShell>();
        trieentry brb = null;

        public trieIterator(Node nood) {
            if (nood.equals(root))
                stack.push(new NodeShell(nood, ""));
            else
                stack.push((new NodeShell(nood, "" + nood.data)));
        }

        public trieentry advance() {
            NodeShell nextshell = stack.pop();

            for (Node n : nextshell.node.branches)
                if (n != null) stack.push(new NodeShell(n, nextshell.string + n.data));

            if (nextshell.node.val != 0)
                brb = new trieentry(nextshell.string, nextshell.node.val);
            else
                advance();
            return brb;
        }

        public boolean hasNext() {
            return !stack.isEmpty();
        }

        public trieentry next() {
            return advance();
        }

        public void remove() {
            throw new UnsupportedOperationException("Not supported yet. Nor will be.");
        }
    }
}
