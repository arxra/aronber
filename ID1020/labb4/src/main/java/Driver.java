import edu.princeton.cs.introcs.In;

import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

public class Driver {
    public static void main(String[] args) {
        trie triäd = new trie();
        URL url = ClassLoader.getSystemResource("pg98.txt");

        if (url != null) {
            System.out.println("Reading from: " + url);
        } else {
            System.out.println("Couldn't find file: pg98.txt");
        }

        In input = new In(url);

        while (!input.isEmpty()) {
            String line = input.readLine().trim();
            String[] words = line.split("(\\. )|:|,|;|!|\\?|( - )|--|(\' )| ");
            String lastOfLine = words[words.length - 1];
            if (lastOfLine.endsWith(".")) {
                words[words.length - 1] = lastOfLine.substring(0, lastOfLine.length() - 1);
            }

            for (String word : words) {
                String word2 = word.replaceAll("\"|\\(|\\)", "");

                if (word2.isEmpty()) {
                    continue;
                }
                // Add the word to the trie
                triäd.Put(word2);
            }
        }
        trie.trieIterator it = triäd.getIter("");
        trieentry a;

        trieentry[] mostcommon = new trieentry[10];
        trieentry[] mostcommon2 = new trieentry[10];

        Arrays.fill(mostcommon, new trieentry("", 0));
        Arrays.fill(mostcommon2, new trieentry("", 0));


        while (it.hasNext()) {
            a = it.next();
            for (int t =0; t< mostcommon.length; t++){
                if (a.getValue()> mostcommon[t].getValue()){
                    mostcommon[t]=a;
                    Arrays.sort(mostcommon);
                    break;
                }
            }
            for (int t =0; t< mostcommon2.length; t++){
                if (a.getValue()> mostcommon[t].getValue() && a.getKey().length() ==2){
                    mostcommon2[t]=a;
                    Arrays.sort(mostcommon2);
                    break;
                }
            }


            System.out.println(a.getKey() + "\t" + a.getValue());
        }

        System.out.println();
        for (trieentry k : mostcommon)
            System.out.println(k.getKey() + "  " + k.getValue());
        System.out.println();
        for (trieentry k : mostcommon2)
            System.out.println(k.getKey() + "  " + k.getValue());

        System.out.println("an count:" + triäd.Count("an"));
        System.out.println("if count:" + triäd.Count("if"));
        System.out.println("it count:" + triäd.Count("it"));
        System.out.println("th count:" + triäd.Count("th"));

        System.out.println("i dist:" + triäd.Distinct("i"));
        System.out.println("a dist:" +triäd.Distinct("a"));
        System.out.println("t dist:" +triäd.Distinct("t"));
        System.out.println("s dist:" +triäd.Distinct("s"));
    }
}
