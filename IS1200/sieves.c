#include <stdio.h>
#include <stdlib.h>
#include <bits/stdc++.h>

// ➜  IE1200 gcc sieves.c -O3 && time ./a.out 1000000 > primes
// ./a.out 1000000 > primes  0.05s user 0.01s system 85% cpu 0.067 total
// ➜  IE1200 gcc sieves.c -O3 && time ./a.out 10000000 > primes
// [2]    18712 segmentation fault  ./a.out 10000000 > primes

#define COLUMNS 6

void print_number(int n) {
	static int nPrinted = 0;
	printf("%10d ", n);
	if(++nPrinted % COLUMNS == 0)
		printf("\n");
}

void print_sieves(int n) {
	char isPrime[n];

	for(int i = 0; i <= n; ++ i)
		isPrime[i-1] = i % 2; // Sieve multiples of 2

	if(n > 1) print_number(2);

	for(int i = 3; i <= n; i += 2) {
		if(isPrime[i-1]) {
			for(int j = i; j <= n; j += i) //Sieve out multiples
				isPrime[j-1] = 0;

			print_number(i);
		}
	}
}

int main(int argc, char *argv[]) {
	if(argc > 1)
		print_sieves(atoi(argv[1]));
	else
		printf("Please state an integer number.\n");
	return 0;
}