#include <stdio.h>

char* text1 = "This is a string.";
char* text2 = "Yet another thing.";

int list1[80/sizeof(int)];
int list2[80/sizeof(int)];

int count = 0;

void printlist(const int* lst){
    printf("ASCII codes and corresponding characters.\n");
    while(*lst){
        printf("0x%03X '%c' ", *lst, (char)*lst);
        lst++;
    }
    printf("\n");
}

void endian_proof(const char* c){
    printf("\nEndian experiment: 0x%02x,0x%02x,0x%02x,0x%02x\n", 
        (int)*c,(int)*(c+1), (int)*(c+2), (int)*(c+3));
}

// Endian experiment: 0x23,0x00,0x00,0x00
// Incrementing pointer moves to higher bits,
// my MacBook Air is little-endian.

void copycodes(const char *s, int *l, int *c) {
    while(*s) *l = *s, ++s, ++l, ++*c;
}

void work() {
    copycodes(text1, list1, &count);
    copycodes(text2, list2, &count);
}

int main(void){
    work();

    printf("\nlist1: ");
    printlist(list1);
    printf("\nlist2: ");
    printlist(list2);
    printf("\nCount = %d\n", count);

    endian_proof((char*) &count);
}
