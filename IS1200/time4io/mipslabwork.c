/* mipslabwork.c

   This file written 2015 by F Lundevall

   This file should be changed by YOU! So add something here:

   This file modified 2015-12-24 by Ture Teknolog 

   Latest update 2015-08-28 by F Lundevall

   For copyright and licensing, see file COPYING */

#include <stdint.h>   /* Declarations of uint_32 and the like */
#include <pic32mx.h>  /* Declarations of system-specific addresses etc */
#include "mipslab.h"  /* Declatations for these labs */

int mytime = 0x5957;

char textstring[] = "text, more text, and even more text!";

/* Interrupt Service Routine */
void user_isr( void ) {
	return;
}

volatile int *porte = 0xbf886110;
volatile int *trise = 0xbf886100;

/* Lab-specific initialization goes here */
void labinit( void ) {
	*trise &= 0x00;
	*porte &= 0x00;
	*(int*)TRISD |= 0xef << 5;
}

int time = 0;

/* This function is called repetitively from the main program */
void labwork() {
	/*if(getbtns()) {
		int i = 0;
		for(; (1<<i) < getbtns(); ++ i);
		mytime = (mytime & ~((0xf) << (i+1)*4)) | (getsw() << (i+1)*4);
	}*/
	delay(1000);
	if(getbtns()&1<<0) mytime = (mytime & ~((0xf) << 4)) | (getsw() << 4);
	if(getbtns()&1<<1) mytime = (mytime & ~((0xf) << 8)) | (getsw() << 8);
	if(getbtns()&1<<2) mytime = (mytime & ~((0xf) << 12)) | (getsw() << 12);
	time2string(textstring, mytime);
	display_string(3, textstring);
	display_update();
	tick(&mytime);
	display_image(96, icon);
}
