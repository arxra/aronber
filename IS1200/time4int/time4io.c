#include <stdint.h>
#include <pic32mx.h>
#include "mipslab.h"

int getsw() {
	return (*(int*) PORTD >> 8) & 0xf;
}

int getbtns() {
	return (*(int*) PORTD >> 5) & 0x7;
}
