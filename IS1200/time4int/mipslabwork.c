/* mipslabwork.c

   This file written 2015 by F Lundevall

   This file should be changed by YOU! So add something here:

   This file modified 2015-12-24 by Ture Teknolog 

   Latest update 2015-08-28 by F Lundevall

   For copyright and licensing, see file COPYING */

#include <stdint.h>   /* Declarations of uint_32 and the like */
#include <pic32mx.h>  /* Declarations of system-specific addresses etc */
#include "mipslab.h"  /* Declatations for these labs */

int mytime = 0x5957;
int prime = 1234567;
int count = 0;

char textstring[] = "text, more text, and even more text!";

/* Interrupt Service Routine */
void user_isr(void) {
	time2string(textstring, mytime);
	display_string(3, textstring);
	display_update();
	tick(&mytime);
	++count;
}

volatile int *porte = 0xbf886110;
volatile int *trise = 0xbf886100;

/* Lab-specific initialization goes here */
void labinit(void) {
	*trise &= 0x00;
	*porte &= 0x00;
	TRISD |= 0xef << 5;
	T2CON = 0;				// Reset timer
	T2CONSET = 7 << 4;		// 1:256 prescaling
	PR2 = 31250; 			// 8,000,000/256 = 31,250, will be 1/10 second period
	TMR2 = 0;				// Reset counter
	T2CONSET = 0x08000;		// Start counter
	IPCCLR(2) = 0x1C;		// Clear pri for interrupt
	IPCSET(2) = 0x08;		// Set pri for interrupt
	IPCCLR(0) = 0x3;		// Clear spri for interrupt
	IPCSET(0) = 0x2;		// Set spri for interrupt
	
}

/* This function is called repetitively from the main program */
void labwork(void) {
	if(count % 10) return; 
	prime = nextprime(prime);
	display_string(0, itoaconv(count));
	display_update();
}
