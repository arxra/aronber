#include <stdio.h>
#include <stdlib.h>

// ➜  IE1200 gcc sieves-heap.c -O3 && time ./a.out 36000000 > primes
// ./a.out 36000000 > primes  1.67s user 0.16s system 95% cpu 1.929 total
// ➜  IE1200 tail -n 1 primes
//    35999939   35999941   35999947   35999969   35999983   35999993

#define COLUMNS 6

void print_number(int n) {
	static int nPrinted = 0;
	printf("%10d ", n);
	if(++nPrinted % COLUMNS == 0)
		printf("\n");
}

void print_sieves(int n) {
	int *isPrime = malloc(sizeof(int) * (n+1));

	for(int i = 0; i <= n; ++ i)
		isPrime[i] = i % 2; // Sieve multiples of 2

	if(n > 1) print_number(2);

	for(int i = 3; i <= n; i += 2) {
		if(isPrime[i]) {
			for(int j = i; j <= n; j += i) //Sieve out multiples
				isPrime[j] = 0;

			print_number(i);
		}
	}
	free(isPrime);
}

int main(int argc, char *argv[]) {
	if(argc > 1)
		print_sieves(atoi(argv[1]));
	else
		printf("Please state an integer number.\n");
	return 0;
}