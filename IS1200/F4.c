int exp(int b, int e) {
	if(e & 1){
		if(e == 1) return b;
		return b * exp(b, e-1);
	} else {
		if(!e) return 1;
		int r = exp(b, e/2);
		return r*r;
	}
}

int fact(int n){
	//return tgamma(n+1);
	int i = 1;
	for(int j = 2; j <= n; ++ j)
		i *= j;
	return i;
}

int factf(int n) {
	if(n == 0)
		return 1;
	return n * factf(n-1);
}

struct tuple {
	int a, b;
}

void swap(int *a, int *b) {
	*a^=*b, *b ^= *a, *a ^= *b;
}

int* p, a;
