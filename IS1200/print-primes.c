#include <stdio.h>
#include <stdlib.h>

// ➜  IE1200 gcc print-primes.c -O3 && time ./a.out 5800000 > primes
// ./a.out 5800000 > primes  1.91s user 0.05s system 98% cpu 1.990 total

#define COLUMNS 6

int is_prime(int n){
	if(n % 2 == 0) return 0;
	for(int i = 3; i*i <= n; i += 2)
		if(n % i == 0) return 0;
	return 1;
}

void print_number(int n) {
	static int nPrinted = 0;
	printf("%10d ", n);
	if(++nPrinted % COLUMNS == 0)
		printf("\n");
}

void print_primes(int n){
	int nPrinted = 1;
	if(n > 1) print_number(2);
	for(int i = 3; i <= n; i += 2)
		if(is_prime(i))
			print_number(i);

	if(nPrinted % COLUMNS) printf("\n");
}

int main(int argc, char *argv[]){
	if(argc > 1)
		print_primes(atoi(argv[1]));
	else
		printf("Please state an integer number.\n");
	return 0;
}

 
