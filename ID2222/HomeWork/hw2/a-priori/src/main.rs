use anyhow::Result;
use clap::{App, Arg};
use itertools::Itertools;
use std::collections::BTreeMap;
use std::fs;

fn read_file() -> Result<Vec<Vec<usize>>> {
    let cont = fs::read_to_string("../data/data.dat")?;

    Ok(cont
        .lines()
        .map(|b| {
            b.split_whitespace()
                .map(|a| a.parse::<usize>().unwrap())
                .collect()
        })
        .collect())
}

#[allow(dead_code)]
fn print_counts<T>(counts: BTreeMap<T, usize>)
where
    T: std::fmt::Debug,
{
    println!("counts: ");
    for a in counts {
        println!("{:?}", a);
    }
}

fn count(file: &[Vec<usize>]) -> BTreeMap<usize, usize> {
    let mut counts: BTreeMap<usize, usize> = BTreeMap::new();
    for l in file {
        for num in l {
            let mut sum = 1;
            if let Some(n) = counts.get(num) {
                sum += n
            };
            counts.insert(*num, sum);
        }
    }
    counts
}

fn droppoff<T>(counts: &mut BTreeMap<T, usize>, dropoff: usize)
where
    T: Clone + std::cmp::Ord,
{
    let mut drop = Vec::new();
    for (num, c) in counts.clone() {
        if c < dropoff {
            drop.push(num)
        }
    }
    for c in drop {
        counts.remove(&c);
    }
}

fn main() -> Result<()> {
    let matches = App::new("homework 2")
        .version("0.1")
        .arg(
            Arg::with_name("confidence")
                .short("c")
                .long("confidence")
                .takes_value(true)
                .help("The confidence degree as a decimal number for associate rules (default: 0.2)"),
        )
        .arg(
            Arg::with_name("support")
                .short("s")
                .long("support")
                .takes_value(true)
                .help("The minimum amount of support required to be considered as common (default: 1000)"),
        )
        .get_matches();

    let file = read_file()?;
    let mut counts = count(&file);

    let support_num = matches.value_of("support").unwrap_or("1000").parse()?;
    let confidence = matches.value_of("confidence").unwrap_or("0.2").parse()?;
    let larget_permutation = 3;
    droppoff(&mut counts, support_num);
    // print_counts(counts.clone());

    let mut common_sets = BTreeMap::new();
    for l in &file {
        let mut row_common = Vec::new();
        for v in l {
            if counts.get(v).is_some() {
                row_common.push(v);
            }
        }
        // Insert all permutations of row_common
        for outer_perm in 2..=larget_permutation {
            for perm in row_common.iter().copied().combinations(outer_perm) {
                let mut val = 1;
                if let Some(n) = common_sets.get(&perm.clone()) {
                    val += n;
                }
                common_sets.insert(perm, val);
            }
        }
    }
    droppoff(&mut common_sets, support_num);
    println!("In total are there {} common pairs: ", common_sets.len());
    for pair in common_sets.clone() {
        println!("{:?}", pair);
    }

    // BONUS
    let mut associate_rules = Vec::new();

    for (elem, count) in common_sets {
        for (i, index) in elem.clone().iter().zip(0..elem.len()) {
            let support: f32 = (count as f32) / (*counts.get(i).unwrap() as f32);
            if support > confidence {
                let mut elem = elem.clone();
                elem.remove(index);
                associate_rules.push((*i, elem, support));
            }
        }
    }
    println!(
        "Found the following {} associate rules:",
        associate_rules.len()
    );
    for rule in associate_rules {
        println!("{} --> {:?}   {}", rule.0, rule.1, rule.2);
    }

    Ok(())
}
