cutoff = 1000
ksize = 5
confidence = 0.2
file = 'data/data.dat'
data = []
common_sets = []


def safeAdd(counts, num):
    if counts.get(num):
        counts[num] += 1
    else:
        counts[num] = 1


def dropCutoff(counts):
    dropoffs = []
    for k in counts:
        if counts[k] < cutoff:
            dropoffs.append(k)
    for l in dropoffs:
        counts.pop(l)


def chooseCommon(counts, l):
    common = []
    for e in l:
        if str(e) in counts:
            common.append(e)
    return common

def addCommonSets(s, i):
    subset = list(map(int, s.replace('[', '').replace(']', '').split(", ")))
    for l in data:
        if isSubset(subset, l):
            for e in l:
                if not e in subset and str(e) in common_sets[0]:
                    new_set = subset + [e]
                    new_set.sort()
                    safeAdd(common_sets[i], str(new_set))


# check if list1 is a sublist of list2
def isSubset(list1, list2):
    if(all(x in list2 for x in list1)):
        return True
    else:
        return False

def main():
    # Read the data to integer arrays
    for line in open(file):
        item = []
        line = line.rstrip().split(' ')
        for i in line:
            item.append(int(i))
        data.append(item)

    # print(len(data))


    counts = {}
    # Count the occurances
    for l in data:
        for num in l:
            safeAdd(counts, str(num))

    print(f"Counted each element, found ({len(counts)}) elements.")

    # Drop off those not worthy
    dropCutoff(counts)

    print(f"Dropped items from the counted dict using cutoff ({cutoff}).")
    common_sets.append(counts)

    for i in range(ksize):
        item_set_count = {}
        common_sets.append(item_set_count)
        for freq in common_sets[i]:
            print(freq)
            addCommonSets(freq, i+1)
            dropCutoff(common_sets[i+1])
    for x in common_sets:
        print(len(x))

    ##########--BONUS--##############
    # Generating association rules with confidence at least c from the itemsets found in the first step.

main()
