from itertools import combinations

cutoff = 100
confidence = 0.2
file = 'data/data.dat'
data = []


def safeAdd(counts, num):
    if counts.get(num):
        counts[num] += 1
    else:
        counts[num] = 1


def dropCutoff(counts):
    dropoffs = []
    for k in counts:
        if counts[k] < cutoff:
            dropoffs.append(k)
    for l in dropoffs:
        counts.pop(l)

def chooseCommon(counts):
    common = []
    for freq in counts:
        if freq in l:
            common.append(freq)
    return common


# Read the data to integer arrays
for line in open(file):
    item = []
    line = line.rstrip().split(' ')
    for i in line:
        item.append(int(i))
    data.append(item)

# print(data)


counts = {}
# Count the occurances
for l in data:
    for num in l:
        safeAdd(counts, num)

print(f"Counted each element, found ({len(counts)}) elements.")
print(counts)

# Drop off those not worthy
dropCutoff(counts)

print(f"Dropped items from the counted dict using cutoff ({cutoff}).")


# Second pass time!
# We now all the frequent enough items to use for sensible comparisons.
pair_count = {}
for l in data:
    pairs = []
    common = chooseCommon(counts)
    for x in common:
        for y in common:
            if x < y:
                pairs.append((x, y))

    for p in pairs:
        safeAdd(pair_count, p)

dropCutoff(pair_count)

print(f"Common pairs ({len(pair_count)}): {pair_count}")

# Third pass time!
tripples_count = {}
for l in data:
    tripples = []
    common = chooseCommon(counts)

    for x in common:
        for y in common:
            for z in common:
                if x < y and x < z and y < z:
                    tripples.append((x, y, z))

    for p in tripples:
        safeAdd(tripples_count, p)

dropCutoff(tripples_count)

print(f"Common tripples: {tripples_count}")

###########--BONUS--##############
# Generating association rules with confidence at least c from the itemsets found in the first step.
def subsets(l: tuple[int, int], n: int):
    return list(combinations(l, n))


assocatie_rules = []
for elem in pair_count:
    for i in elem:
        supp = pair_count[elem] / counts[i]
        if supp >= confidence:
            e = set(elem)
            e.remove(i)
            assocatie_rules.append((i, e, supp))

print("found the following rules:")
for (i, e, supp) in assocatie_rules:
    print(f"support {i}-> {e} {round(supp, 3)}")

