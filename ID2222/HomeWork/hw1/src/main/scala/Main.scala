import collection.immutable.SortedSet
import scala.collection.mutable.ArrayBuffer

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, SparkSession}
import org.apache.spark.sql.catalyst.ScalaReflection

case class Document(name: Long, value: String, hashes: SortedSet[Int])
case class DocumentPairs(doc1: Document, doc2: Document, similarity: Double)

object Main extends App {

  // Variables we might want to chnage easily.
  val file_path = "./data/"
  val test = "this text is going to be shinlged, ut we do not yet know how"
  val shinglelen = 5
  val signaturelen = 100
  val precision = 1000
  val simtreshold = 0.08

  // Getting the minhash subset is easy if the set is sorted,
  // we simply take the firs n elements.
  def MinHashing(minHash: SortedSet[Int]): SortedSet[Int] = {
    minHash.take(signaturelen)
  }

  // Shingling in scala is really easy: We can just take a string and apply
  // a sliding window over it if we take it as a list of char's.
  def Shingling(text: String): SortedSet[Int] = {
    // Shuld return a ist of hashed shingles.
    val hashes = text.toList
      .sliding(shinglelen)
      .map(_.mkString.distinct.hashCode)
      .toSeq

      // we return our sortedset, which keeps our hashes sorted.
    SortedSet(hashes: _*)
  }

  // Comparing the jaccard similarity of two sets is really straght forward,
  // unless the second set is empty, at which point we say they are not similar at all.
  def CompareSets(set1: Set[Int], set2: Set[Int]): Double = {
    if (set1.nonEmpty & set2.nonEmpty) {
      set1.intersect(set2).size.toDouble / set1.union(set2).size.toDouble
    } else {
      0.0
    }
  }

  // Comparing signatures is the same as comparing sets.
  def CompareSignatures(set1: Set[Int], set2: Set[Int]): Double = {
    CompareSets(set1, set2)
  }


  // Create a spark session, which is preffered over spark context since spark 2.0
  val spark = SparkSession
    .builder()
    .config("spark.master", "local")
    .getOrCreate()

  // implicits include decoders and encoder, but its still hard to get your data straight
  import spark.implicits._

  // Read the files in the data directory and map it to the case class we want to work with.
  val data = spark.read
    .text(file_path)
    .withColumn("id", monotonically_increasing_id())
    .collect()
    .map(row =>
      Document(
        s"${row.get(1)}".toLong,
        row.get(0).asInstanceOf[String],
        Shingling(row.get(0).asInstanceOf[String])
      )
    )

  // We use a arraybuffer to save results in the iteration.
  var ans: ArrayBuffer[DocumentPairs] = ArrayBuffer()
  // For each document we want to comare it with all the following not yet comapred documents
  // Byt doing this, we check each possible combination of documents once.
  for (firstdoc <- 0 to data.size - 2) {
    for (secdoc <- firstdoc + 1 to data.size - 1) {
      //Geneare the similatiry
      val simlu = CompareSignatures(
        data(firstdoc).hashes,
        data(secdoc).hashes
      )
      // IF the similarity os over the threashold,
      // add it to the array of similar docs as a document pair.
      if (simlu >= simtreshold) {
        ans += DocumentPairs(data(firstdoc), data(secdoc), simlu)
      }
    }
  }
  // Print out the results
  println(s"The following set was deemed equal enough: ")
  for (a <- ans) {
    println(
      s"${a.doc1.name} ~${(math floor a.similarity * precision) / precision}~ ${a.doc2.name}"
    )
  }

  // Stop the spark session. This removes some "end of application" errors.
  spark.stop

}
