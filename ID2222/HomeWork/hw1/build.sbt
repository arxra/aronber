
// The simplest possible sbt build file is just one line:

scalaVersion := "2.12.15"
val sparkVersion = "3.2.0"



/* idePackagePrefix := Some("com.felixseifert.socialweatherstreamer") */

libraryDependencies ++= Seq(
    "org.apache.spark"  %% "spark-core"                 % sparkVersion,
    "org.apache.spark"  %% "spark-streaming"            % sparkVersion,
    "org.apache.spark"  %% "spark-sql"                  % sparkVersion,
    )
// That is, to create a valid sbt build, all you've got to do is define the
// version of Scala you'd like your project to use.

// ============================================================================

// Lines like the above defining `scalaVersion` are called "settings". Settings
// are key/value pairs. In the case of `scalaVersion`, the key is "scalaVersion"
// and the value is "2.13.3"

// It's possible to define many kinds of settings, such as:

name := "homework-1"
organization := "ch.epfl.scala"
version := "1.0"

