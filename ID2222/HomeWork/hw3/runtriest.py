from triest_base import TriestBase
from triest_fd import TriestFD

def main():
	file = 'data/web-Stanford.txt'
	t = 0
	time = 0
	basemode = 0
	estimate = []
	est_iter = 5

	for i in range(est_iter):
		if basemode:
			triest = TriestBase()
		else:
			triest = TriestFD()

		with open(file) as data:
			for line in data:
				if line.startswith('#'):
					continue
				(u,v) = line.split('\t')
				triest.run(int(u),int(v))
				# t += 1	
				# if t % 23124 == 0:
				# 	time += 1
				# 	print(f'{time}/100')
		estimate.append(triest.getCounter())
		print(f'Found {triest.global_counter} global triangels in the subgraf in iteration: {i+1}.')

	print(f'Estimated triangles per iteration: {estimate}')
	estimate.sort()
	# for e in estimate:
	# 	print(e)
	print(f'number of estimated triangles (median): {estimate[int(est_iter/2)]}')
	print(f'number of estimated triangles (mean): 	{int(sum(estimate)/len(estimate))}')

main()