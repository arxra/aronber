import random
from math import comb

class TriestFD:

	def __init__(self):
		self.edges = {}
		self.sample_graph = {}
		self.local_counter = {}
		self.global_counter = 0
		self.t = 0
		self.m = 15000
		self.di = 0
		self.do = 0
		self.s = 0

	def coinFlip(self, treshold):
		p = random.random()
		if p < treshold:
			return True
		else:
			return False

	def sampleEdge(self,u,v):
		self.s += 1
		if self.do + self.di == 0:
			if self.s <= self.m:
				return True
			elif self.coinFlip(self.m/self.t):
				rand_edge = random.choice(list(self.edges))
				self.removeEdge(rand_edge)
				return True
			else:
				return False
		elif self.coinFlip(self.di/(self.di+self.do)):
			self.di -= 1
			return True
		else:
			self.do -= 1
			return False



	def addEdge(self,u,v):
		# add the key but with no value as the existence of 
		# the key indicates the existence of the edge
		self.edges[(u,v)] = None
		if self.sample_graph.get(u):
			self.sample_graph[u].add(v)
		else:
			self.sample_graph[u] = {v}

		if self.sample_graph.get(v):
			self.sample_graph[v].add(u)
		else:
			self.sample_graph[v] = {u}

	def removeEdge(self, edge):
		(u,v) = edge
		self.edges.pop(edge)
		if self.sample_graph.get(u):
			if v in self.sample_graph[u]:
				self.sample_graph[u].remove(v)
		if self.sample_graph[v]:
			if u in self.sample_graph[v]:
				self.sample_graph[v].remove(u)
		self.updateCounters('-',u,v)

	def updateCount(self,op,node):
		if op == '+':
			if node in self.local_counter:
				self.local_counter[node] += 1
			else:
				self.local_counter[node] = 1

		elif op == '-':
			if node in self.local_counter:
				self.local_counter[node] -= 1
				if self.local_counter[node] <= 0:
					self.local_counter.pop(node)


	def updateCounters(self,op,u,v):
		common_nodes = self.sample_graph[u].intersection(self.sample_graph[v])
		for c in common_nodes:
			if op == '+':
				self.global_counter += 1
			elif op == '-':
				self.global_counter -= 1

			self.updateCount(op,c)
			self.updateCount(op,u)
			self.updateCount(op,v)

	def getCounter(self):
		mt = len(self.edges)
		w = min(mt, self.s + self.di + self.do)
		ksub = 0
		for j in range(3):
			ksub += comb(self.s,j)*comb(self.di+self.do, w-j)/comb(self.s+self.di+self.do, w)
		k = 1 - ksub
		estimation = self.global_counter*self.s*(self.s-1)*(self.s-2)/(k*mt*(mt-1)*(mt-2))
		return int(estimation)

	def run(self,u,v):
		if (v,u) in self.edges:
			return
		self.t += 1
		if self.sampleEdge(u,v):
			self.addEdge(u,v)
			self.updateCounters('+',u,v)

			


