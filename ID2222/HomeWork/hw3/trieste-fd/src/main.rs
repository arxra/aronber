use crate::triest::Treist;
use anyhow::Result;
use clap::{App, Arg};

mod my_reader;
mod op;
mod triest;

fn main() -> Result<()> {
    let matches = App::new("homework 3")
        .version("0.1")
        .arg(Arg::with_name("file_path").required(true).index(1))
        .arg(
            Arg::with_name("memmory")
                .short("m")
                .long("memmory")
                .takes_value(true)
                .help("The M parameter to triest. Default: 100000"),
        )
        .arg(
            Arg::with_name("iterations")
                .short("i")
                .long("iter")
                .takes_value(true)
                .help("The amount of tests to do. The mean is presented"),
        )
        .get_matches();
    let path = matches.value_of("file_path").unwrap();
    let m: usize = matches
        .value_of("memmory")
        .unwrap_or("100000")
        .parse()
        .unwrap();
    let iter: usize = matches
        .value_of("iterations")
        .unwrap_or("1")
        .parse()
        .unwrap();

    let mut triangles = 0.0;
    for i in 0..=iter {
        if iter > 1 {
            println!("Iteration {}", i);
        }
        let mut triest = Treist::new(path, m);
        triest.calculate_triangles();
        // triest.status();
        triangles += triest.estimate();
    }

    println!("Estimated Triangles mean: {}", triangles/iter as f64);

    Ok(())
}
