use std::ops::{Add, Sub};

#[derive(Debug)]
pub enum Op {
    Add,
    Sub,
}
impl Op {
    pub(crate) fn apply_int(&self, s: isize, arg: isize) -> isize {
        match self {
            Op::Add => s.add(arg),
            Op::Sub => s.sub(arg),
        }
    }
    pub(crate) fn apply(&self, s: usize, arg: usize) -> usize {
        match self {
            Op::Add => s.add(arg),
            Op::Sub => s.sub(arg),
        }
    }
    pub(crate) fn apply_mut(&self, s: &mut usize, arg: usize) -> usize {
        match self {
            Op::Add => s.add(arg),
            Op::Sub => s.sub(arg),
        }
    }
}
