use anyhow::anyhow;
use anyhow::Result;
use std::{
    fs::File,
    io::{self, prelude::*},
};

use crate::op::Op;

#[derive(Debug)]
pub struct BufReader {
    reader: io::BufReader<File>,
    buffer: String,
}

impl BufReader {
    /// Given a file path, creates a buffered reader on that file.
    pub fn open(path: impl AsRef<std::path::Path>) -> io::Result<Self> {
        let file = File::open(path)?;
        let reader = io::BufReader::new(file);
        let buffer = String::new();

        Ok(Self { reader, buffer })
    }

    fn read_line(&mut self) -> Result<Option<()>> {
        self.buffer.clear();

        let read_res = self.reader.read_line(&mut self.buffer);
        match read_res {
            Ok(a) => {
                if a == 0 {
                    Ok(None)
                } else if self.buffer.starts_with('#') {
                    self.read_line()
                } else {
                    Ok(Some(()))
                }
            }
            Err(e) => Err(anyhow!("{}", e)),
        }
    }

    /// Reads one line and returns it as a touple represetning a un-directed edge.
    ///
    /// WARN: This has very low tightness.
    pub fn read_edge(&mut self) -> Option<Result<(Op, (usize, usize))>> {
        if let Ok(None) = self.read_line() {
            return None;
        }

        let values: Vec<usize> = self
            .buffer
            .split_whitespace()
            .map(|a| a.parse::<usize>().unwrap())
            .collect();

        Some(Ok((Op::Add, (values[0], values[1]))))
    }
}
