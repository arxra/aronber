use crate::my_reader::BufReader;
use crate::op::Op;

use itertools::Itertools;
use num_integer::binomial;
use petgraph::{graphmap::GraphMap, visit::EdgeIndexable, Undirected};
use rand::{distributions::Uniform, prelude::*, thread_rng};

use std::collections::HashMap;

#[derive(Debug)]
pub(crate) struct Treist {
    file: BufReader,
    rng: ThreadRng,

    ss: GraphMap<usize, (), Undirected>,
    counters: HashMap<usize, usize>,

    di: usize,  // Our inside deleted
    drm: usize, // Our outside detleted
    t: usize,   // Global time (events)
    tau: isize, // Global counter
    s: usize,   // The amount of edges in the system
    m: usize,   // Our allowed edge size
}

impl Treist {
    pub fn new(path: &str, m: usize) -> Self {
        let file = BufReader::open(path).unwrap();
        let rng = thread_rng();

        let di = 0; // Our deleted
        let drm = 0;
        let t = 0; // Global time (events)
        let tau = 0;
        let s = 0; //
        let ss = GraphMap::new();
        let counters = HashMap::new();
        Self {
            file,
            rng,
            di,
            drm,
            t,
            tau,
            s,
            ss,
            m,
            counters,
        }
    }

    /// Calculates a estimate for the amount of triangles in the file for this Triest.
    /// The larger the M value for the Triest, the better the estimation
    ///
    /// WARN: This process may take a lot of time if the graph is large
    pub fn calculate_triangles(&mut self) {
        while let Some(Ok((op, (u, v)))) = self.file.read_edge() {
            if self.ss.contains_edge(u, v) {
                continue;
            }
            self.t += 1;
            self.s = op.apply(self.s, 1);

            if let Op::Add = op {
                if self.sample_edge((u, v)) {
                    self.update_counter(Op::Add, (u, v));
                }
            } else if self.ss.contains_edge(u, v) {
                self.update_counter(Op::Sub, (u, v));
                self.ss.remove_edge(u, v);
                self.di += 1;
            } else {
                self.drm += 1;
            }
        }
    }
    fn sample_edge(&mut self, (u, v): (usize, usize)) -> bool {
        if self.di + self.drm == 0 {
            if self.ss.edge_count() < self.m {
                self.ss.add_edge(u, v, ());
                true
            } else if self.rng.gen_ratio(self.m as u32, self.t as u32) {
                let re_edge_ind = self.rng.sample(Uniform::new(0, self.ss.edge_count()));
                let (z, w) = self.ss.from_index(re_edge_ind);
                self.update_counter(Op::Sub, (z, w));
                self.ss.remove_edge(z, w);
                self.ss.add_edge(u, v, ());
                true
            } else {
                false
            }
        } else if self
            .rng
            .gen_ratio(self.di as u32, (self.drm + self.di) as u32)
        {
            self.ss.add_edge(u, v, ());
            self.di -= 1;
            true
        } else {
            self.drm -= 1;
            false
        }
    }

    fn update_counter(&mut self, op: Op, (u, v): (usize, usize)) {
        fn update(c: &mut HashMap<usize, usize>, op: &Op, v: usize) {
            if let Some(u) = c.get_mut(&v) {
                op.apply_mut(u, 1);
            } else if let Op::Add = op {
                c.insert(v, 1);
            } else {
                println!("Removed from a non exisiting counter!");
            }
        }
        let u_neigh = self
            .ss
            .neighbors(u)
            .chain(self.ss.neighbors(v))
            .duplicates();
        for c in u_neigh {
            self.tau = op.apply_int(self.tau, 1);
            [u, v, c].map(|v| update(&mut self.counters, &op, v));
        }
    }

    #[allow(dead_code)]
    pub fn status(&self) {
        println!("Edges in Sample: {}", self.ss.edge_count());
        println!("tau: {}", self.tau);
        println!("t: {}", self.t);
        println!("s: {}", self.s);
        println!("Seen edges over stored edges: {}", self.seen_over_edges());
    }

    fn seen_over_edges(&self) -> f64 {
        let mt = self.ss.edge_count();
        let s = self.s;
        (s * (s - 1) * (s - 2)) as f64 / (mt * (mt - 1) * (mt - 2)) as f64
    }

    pub fn estimate(&self) -> f64 {
        // We have 0 triangles if we have less than 3 elements
        if self.t < 3 {
            return 0.0;
        }

        let wt = self.m.min(self.s + self.di + self.drm);

        // This is expected to be 0 as we always get di+drm = 0 without removals
        let kt_sum = if self.di + self.drm < wt {
            0.0
        } else {
            (0..=2)
                .into_iter()
                .map(|j| {
                    (binomial(self.s, j) * binomial(self.di + self.drm, wt - j)) as f64
                        / binomial(self.s + self.di + self.drm, wt) as f64
                })
                .sum()
        };

        // Then KT is always 1
        let kt: f64 = 1.0 - kt_sum;
        // And tau/kt = tau
        let tau_kt = self.tau as f64 / kt;

        tau_kt * self.seen_over_edges()
    }
}
