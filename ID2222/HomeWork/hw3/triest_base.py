import random

class TriestBase:

	def __init__(self):
		self.edges = {}
		self.sample_graph = {}
		self.local_counter = {}
		self.global_counter = 0
		self.t = 0
		self.m = 10000

	def coinFlip(self):
		p = random.random()
		if p < self.m/self.t:
			return True
		else:
			return False

	def sampleEdge(self,u,v):
		if self.t <= self.m:
			return True
		elif self.coinFlip():
			rand_edge = random.choice(list(self.edges))
			self.removeEdge(rand_edge)
			return True
		else:
			return False


	def addEdge(self,u,v):
		self.edges[(u,v)] = None
		if self.sample_graph.get(u):
			self.sample_graph[u].add(v)
		else:
			self.sample_graph[u] = {v}

		if self.sample_graph.get(v):
			self.sample_graph[v].add(u)
		else:
			self.sample_graph[v] = {u}

	def removeEdge(self, edge):
		(u,v) = edge
		self.edges.pop(edge)
		if self.sample_graph.get(u):
			if v in self.sample_graph[u]:
				self.sample_graph[u].remove(v)
		if self.sample_graph[v]:
			if u in self.sample_graph[v]:
				self.sample_graph[v].remove(u)
		self.updateCounters('-',u,v)

	def updateCount(self,op,node):
		if op == '+':
			if node in self.local_counter:
				self.local_counter[node] += 1
			else:
				self.local_counter[node] = 1

		elif op == '-':
			if node in self.local_counter:
				self.local_counter[node] -= 1
				if self.local_counter[node] <= 0:
					self.local_counter.pop(node)


	def updateCounters(self,op,u,v):
		common_nodes = self.sample_graph[u].intersection(self.sample_graph[v])
		for c in common_nodes:
			
			if op == '+':
				self.global_counter += 1
			elif op == '-':
				self.global_counter -= 1
			
			self.updateCount(op,c)
			self.updateCount(op,u)
			self.updateCount(op,v)

	def getCounter(self):
		estimate = max(1, self.t*(self.t-1)*(self.t-2)/(self.m*(self.m-1)*(self.m-2)))
		return int(estimate*self.global_counter)

	def run(self,u,v):
		if (v,u) in self.edges:
			return
		self.t += 1
		if self.sampleEdge(u,v):
			self.addEdge(u,v)
			self.updateCounters('+',u,v)


