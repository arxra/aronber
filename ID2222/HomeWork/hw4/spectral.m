

% Changeable parameters:
k = 4; % clusters    
sigma = 1;

% Program
E = readmatrix("~/Downloads/example1.dat");
col1 = E(:,1);
col2 = E(:,2);
max_ids = max(max(col1,col2));
As= sparse(col1, col2, 1, max_ids, max_ids); 
A = full(As);

[v,D] = eig(A);
sort(diag(D));

Af = CalculateAffinity(A, sigma);

D = diag(sum(Af, 2)); % D is the diagonal matrix where i,i elemen it sum of i'th row
L = (D^(-0.5))*A*(D^(-0.5)); % L = D**-.5*A*D**-.5

% Find the k largest eigenvectors of L (Largest Magnitude)
% -- X = matrix V whose columns are the corresponding eigenvectors.
% -- D = diagonal matrix D containing the eigenvalues on the main diagonal
[X,D] = eigs(L, k, 'LM');
sort(X);

%%% 4 %%%
% Normalize X per row
Y = normalize(X,2);

%%% 5 %%%
idx = kmeans(X,k);

%%% 6 %%%
color = lines(6); % Generate color values
figure;
plot(A(idx==1,1),A(idx==1,2),'r.','MarkerSize',12)
hold on
plot(A(idx==2,1),A(idx==2,2),'b.','MarkerSize',12)
title 'Cluster Assignments'
hold off

for i = 1:size(A,1)
    for j = 1:size(A,2)
        if A(i,j) == 1
            % Plot the node
            gscatter(i, j, idx(i,1), color(1:6, :), 'o')
        end
    end
end

function [affinity] = CalculateAffinity(data, sigma)
    % set the parameters
    for i=1:size(data,1)    
        for j=1:size(data,1)
            dist = sqrt((data(i,1) - data(j,1))^2 + (data(i,2) - data(j,2))^2); 
            affinity(i,j) = exp(-dist/(2*sigma^2));
        end
    end
end

