import java.net.*;
import java.io.*;

public class HTTPAsk {
    static private class TCPClient {

        static String askServer(String hostname, int port, String ToServer) throws IOException {

            Socket cs = new Socket(hostname, port); //Spawn ClientSocket
            StringBuilder ans = new StringBuilder(); //Stringbuilder with server answers
            cs.setSoTimeout(500);

            DataOutputStream ts = new DataOutputStream(cs.getOutputStream()); //Create a outputstream to the socket
            BufferedReader fs = new BufferedReader(new InputStreamReader(cs.getInputStream())); //Gotta read data as well
            ts.writeBytes(ToServer + "\n"); //Sends the ToServer String

            String temp;
            while ((temp = fs.readLine()) != null)
                ans.append(temp).append("\r\n");

            cs.close(); //Cleanup aka socketclosing
            return ans.toString(); //Return the answer as a string from stringbuilder
        }

        public static String askServer(String hostname, int port) throws IOException {
            return askServer(hostname, port, "");
        }
    }

    public static void main(String[] args) {
        Boolean run = true;
        while (run)
            try {
                int i = Integer.parseInt(args[0]);
                if (i > 0 && i < 65536)
                    serverProcess(i);
                else
                    System.out.println("Port not in valid range of port numbers.");
            } catch (Exception e) {
                System.out.println("Not a valid port.");
                run = false;
            }
    }

    private static String response(int code, String resp) {
        StringBuilder builder = new StringBuilder();
        builder.append("HTTP/1.1 ");
        switch (code) {
            case 200:
                builder.append("200 OK").append(" Content-Type: text/html");
                break;
            case 400:
                builder.append("400 Bad Request");
                break;
            case 404:
                builder.append("404 Not Found");
                break;
        }
        builder.append("\r\n\r\n");
        if (code == 200)
            builder.append(resp);
        return builder.toString();
    }


    private static void serverProcess(int arg) {
        try (ServerSocket serversocket = new ServerSocket(arg)) {
            Socket con = serversocket.accept();
            con.setSoTimeout(500);

            //Text streams
            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            DataOutputStream toclient = new DataOutputStream(con.getOutputStream());

            //Build a message
            StringBuilder fromClient = new StringBuilder();
            StringBuilder toClient = new StringBuilder();
            String fc;

            //variables
            String[] hostN = null;
            String[] portN = null;
            String[] query = {"", ""};

            //Read the request.
            while ((fc = reader.readLine()) != null) {
                if (fc.equals("")) {
                    break;
                }
                fromClient.append(fc);
            }

            //Dynamic request parsing
            String[] parse;
            String parser;
            try {
                parse = (fromClient.toString().split(" "));
                parser = parse[1].split("ask?")[1];
                for (String l : parser.split("&")) {
                    if (l.contains("hostname"))
                        hostN = l.split("=");
                    else if (l.contains("port"))
                        portN = l.split("=");
                    else if (l.contains("query") || l.contains("string"))
                        query = l.split("=");
                }
            } catch (Exception e) {
                toClient.append(response(400, e.toString()));
            }

            //If the request goes through, make a return request.
            if (toClient.toString().equals(""))
                try {
                    if (hostN == null || portN == null)
                        throw new Exception();

                    toClient.append(response(200, TCPClient.askServer(hostN[1], Integer.parseInt(portN[1]), query[1])));

                } catch (Exception e) {
                    toClient.append(response(400, ""));
                }

            //Finally, return the response
            toclient.writeBytes(toClient.toString());
            con.close();

        } catch (Exception e) {
            System.out.print("Error, fault in socket");
        }

    }
}

