import java.io.*;
import java.net.*;

public class HTTPEcho {
   public static void main( String[] args) {
       try (ServerSocket serversocket = new ServerSocket(Integer.parseInt(args[0]))) {
           while (true){
               Socket socket = serversocket.accept();
               BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
               DataOutputStream toclient = new DataOutputStream(socket.getOutputStream());
               String fc;
               StringBuilder response = new StringBuilder();
               toclient.writeBytes("HTTP/1.1 200 OK\r\n");
               toclient.writeBytes("Content-Type: text/html\r\n\r\n");

               response.append("<html>");

               while ((fc = reader.readLine()) != null){
                    if(fc.equals(""))
                        break;
                   response.append(fc).append(System.getProperty("line.separator"));
               }
               toclient.writeBytes(response.append("</html>").toString());

               //cleanup
               socket.close();
               toclient.flush();
               toclient.close();
           }
       } catch (Exception e){
          System.out.println(e.toString());
       }
   }
}