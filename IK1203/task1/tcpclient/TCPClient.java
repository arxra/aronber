package tcpclient;
import java.net.*;
import java.io.*;

public class TCPClient {
    
    public static String askServer(String hostname, int port, String ToServer) throws  IOException {

        Socket cs = new Socket(hostname, port); //Spawn ClientSocket
        StringBuilder ans = new StringBuilder(); //Stringbuilder with server answers

        DataOutputStream ts = new DataOutputStream(cs.getOutputStream()); //Create a outputstream to the socket
        BufferedReader fs = new BufferedReader(new InputStreamReader(cs.getInputStream())); //Gotta read data as well
        ts.writeBytes(ToServer + "\n"); //Sends the ToServer String

        String temp;
        while((temp = fs.readLine()) != null)
            ans.append(temp).append("\n");

        cs.close(); //Cleanup aka socketclosing
        return ans.toString(); //Return the answer as a string from stringbuilder
    }

    public static String askServer(String hostname, int port) throws  IOException {
        return askServer(hostname, port, "");
    }
}

