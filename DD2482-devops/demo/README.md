# DD2482 demo

## Content

This demo is all about the parallelization of tests using cypress.

### How to parallel

Ci setup is straight forward, IF either all the test nodes run their own copy of the tested firmware or it is intermediately deployed to a publicly available
space.
Running the  full backend on each testnode is probably _not_ what you want as it makes scaling hard and duplicates _a lot_ of work on the backend.
Having your own ci runners help, as we can easily grant them access to a deployed server without exposing our application.

### Handling our backend

Since we have multiple tests testing different aspects of our service at the same time, they cannot share resources.
Just imagine if we have a single test user that is allowed admin priviliges in one set of tests, and none in another.
This would introduce randomness into our tests and their results, at which point we might as well not run any tests.

### Leaving our environment

Cleaning before each test runs, making sure there are no duplicate resources enables us to visit the test system after the tests to see what happened.
Compare this to the common way of cleaning after tests, where we do not get any information after.

### Parallelization != Speed

Cost of developing and changing infra to accomedate parallelization of tests.

