# Install Cypress, then run all tests (in parallel)
stages:
  - build
  - test
  - publish


services:
  - docker:19.03.13-dind
# Set environment variables for folders in "cache" job settings for npm modules and Cypress binary
variables:
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"

# Cache using branch name
# https://gitlab.com/help/ci/caching/index.md
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .cache/*
    - cache/Cypress
    - node_modules
    - build

# Install NPM dependencies and Cypress
install:
  image: cypress/browsers:node14.15.0-chrome86-ff82
  stage: build

  script:
    - yarn install
    # check Cypress binary path and cached versions
    - npx cypress cache path
    - npx cypress cache list
    - yarn build:ci

api-tests:
  image: cypress/browsers:node14.15.0-chrome86-ff82
  stage: test
  parallel: 2
  script:
    - yarn start:ci & npx wait-on http://localhost:3000
    - npx cypress run --record --parallel --key $CY_KEY --browser firefox --spec "cypress/tests/api/*"

build-docker:
  stage: publish
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t registry.gitlab.com/arxra/cypress-realworld-app .
    - docker push registry.gitlab.com/arxra/cypress-realworld-app
