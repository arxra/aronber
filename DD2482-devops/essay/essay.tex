\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{geometry}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{fit}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{fancyhdr}
\usepackage[parfill]{parskip}
\usepackage{ulem}

\title{The Rust Buildsystem}
\author{Aron Hansen Berggen\\ Yannik Sander }
\date{April 2021}

\usepackage[style=ieee,backend=biber]{biblatex}
\addbibresource{references.bib}

\usepackage{graphicx}

\begin{document}

\maketitle

\newpage

\section{Introduction}
There is a lot of talk about how DevOps supposedly solves all your problems when developing software, from the features planned to how they are used in production and everything in between.
Even open-source projects are expected to follow DevOps these days, and the Rust programming language is no exception. \\
But for such a massive project as a advanced programming language which compiles to every mainstream platform with tooling, documentation and hundreds of contributors, how does DevOps scale? 
What are the compromises you have to make along the way to make this all work?
This is what will be explored and explained in the upcoming sections.
We start off by explaining what we are looking at before diving into the tools that enable this massive open source  project to work.


\subsection{What is Rust}
% https://github.com/rust-lang/rust
According to the official Rust language website\cite{RustProgrammingLanguage}, Rust is a systems level programming language focusing on performance, reliability and productivity.
It extends these in the following ways:

\begin{itemize}
    \item Performance:\\
    Memory efficient without any garbage collector or run-time.
    \item Reliability:\\
    Extensive type system and ownership models to guarantee memory and thread safety at compile time.
    \item Productivity:\\
    The tooling for working with rust such as documentation, build tools and compiler errors are feature complete and reliable.
\end{itemize}

These points together with a strong type-system and its expansive ecosystem are pointed out as the reasons for Rust being voted as the most loved programming language 4 years in a row in the yearly surveys by Stack Overflow\cite{gouldingWhatRustWhy2020}.

\subsection{What is a buildpipe}
%  https://dev.azure.com/rust-lang/rust
% This is "hard to read" according to the hemingway editor.
% TODO: refactor the below into more readable sentences.
A building pipeline takes some input and performs a set of steps to output some product at the end.
This often includes code reviews, automated tests, code compilation, packaging of compiled code and further validation of the packages before publishing. 
Throughout this process one of the most important aspects is to monitor these processes and act on that feedback.\cite{ImportanceDevOpsPipeline2020,katzEssentialStepsBuilding}

\subsection{What are GitHub Actions}
According to the official GitHub page on GitHub Actions\cite{FeaturesGitHubActions}, they are project-defined automatic actions for building, testing, and deploying code based on events such as pull requests or other external events.\\

One engineer, Jonas Hecht describes GitHub Actions in a blog post\cite{StopRewritingPipelines2021} as the next generation CI/CD.
He goes further to explain why. We have had CI/CD as file configurations for a good while now, but GitHub Actions takes it one step further with the easily accessible pre-built actions available to everyone on GitHub.
He describes them as the missing Lego blocks for building a complete CI/CD pipeline, with close to single click
interactions with code analysis and deployment tools as examples.


\section{The \sout{Rabbit Hole} Rust Build pipeline}

This section presents the specifics of the Rust project's build process.
It goes without saying that we cannot focus on every aspect of it.
Yet, we strive to present a comprehensive picture of the most important pieces.
We will focus on how Rust facilitates the \textbf{Bors} bot and its development pattern while also introducing the social interactions bot \textbf{rustbot}.

\subsection{Rustbot}\label{sec:rustbot}
% https://github.com/rust-lavong/triagebot
The Rustbot can be summarised as the people managing bot of the Rust development process.
% Reading its documentation\cite{TriagebotRustForge} explains how it achieves this.
It manages teams and people not just in the Rust GitHub organisations but also elsewhere, keeping the right people up to date without broadcasting information to everyone. 
The Rustbot also keeps track of work-in-progress issues such as who is working on it, meaning issues and code review style tasks are assigned by interacting with the bot.
It also makes it easy to ping teams within the Rust organisation, for example the LLVM\footnote{LLVM is a open source, BSD-licensed compiler framework which the Rust compiler depends on.}-team working on the compiler infrastructure.
This makes it easy for the teams to assist without those in need of help knowing whom directly to contact.

\subsection{Developing with Bors}

Inspecting the \texttt{rust-lang/rust} repository two things become apparent: 

First, none of the main branches shows any trace of being a \textit{development branch}.
On the contrary, their naming (\texttt{stable, beta, master}) rather suggests a connection with the release cycle.
Secondly supporting the first insight, the commit history appears very automated as well. Many commits are structured as follows:

\begin{quote}
    \texttt{Auto merge of \#84353 - estebank:as-ref-mir, r=davidtwco}
\end{quote}

Under closer inspection these commits become apparent as \textbf{merge-commits}, merging a set of changes into the master branch.
All of these commits are merged by Bors and refer to pull requests.

We see: Bors automates the merging of pull-requests. Indeed \textit{none} of the commits made directly onto the master branch are by any other committer than Bors.

Yet, where do these commits come from and more importantly are they even verified? Lets follow our white lagomorph...

\subsubsection{The Pull Request}

When opening a pull request on the rust repository, if you did not ask for any specific reviewer, the \texttf{rust-highfive} bot assigns a rust team member to it. This affects the assignee on GitHub as well as leaving a comment picked up by rustbot.
If a maintainer reviewed your code they might comment in one of the following ways:

\begin{itemize}
    \item \texttt{r+} In Bors terms this is equivalent to pressing the green "Merge" button and comes with the same responsibility
    \item Instruct the author to do changes
    \item \texttt{r? @username} Ask for another review
\end{itemize}

In case of the first action (\texttt{r+}) we see bors commenting with something like:

\begin{quote}
    ⌛ Testing commit f43ee8e with merge e888a57...
\end{quote}

This, given the test runs successfully and the change is approved, will cause Bors to merge the PR.

So far so good, but a question still remains:

\begin{quote}
What is tested, where and how? After all, no traces of tests are present in the repository.
\end{quote}

\subsubsection{A bot's playground}

The whole CI of the rust project is managed in a fork of the main repository. That fork lives in \texttt{rust-lang-ci/rust}. It ensures that the main repository, especially its master branch, always builds without failure.





%\section{The Rust Build Pipeline}
%% The Rust infrastructure team has all its documentation about the ci in https://forge.rust-lang.org/infra/docs/rustc-ci.html
%
%In this section we are looking at the specifics of the Rust projects build methods and tools.
%We start by looking at their git branches in \ref{sec:branches}.
%These are interesting as they are set up more sophisticated than usually seen in simpler open-source projects.
%After knowing the branches, we take a look at the automated actions in Section~\ref{sec:actions}.
%Last we look at the automated bots which manage these branches based on the actions in \ref{sec:bots}.
%
%
%\subsection{Branches}\label{sec:branches}
%% We should probably explain the different branches used, or the bots will not make sense. But in what order?
%% defs: https://forge.rust-lang.org/infra/docs/rustc-ci.html#which-branches-we-test
%% In particular:
%% - auto
%% - master
%% - Optional(try)
%
%
%% Aron: I'm not sure if we should cover branches or bots first, as they both build on eachother.
%
%% TODO: replace with...some intro
%Rust uses a set of branches which one should know about for the development to make sense:
%\begin{enumerate}
%    \item Auto
%    \item Master
%    \item Beta
%    \item Stable
%\end{enumerate}
%The \textit{auto} branch\label{sec:auto} is where the tests are run before being "merged".
%If the tests on the \textit{auto} branch succeed, \textit{master} will be fast-forwarded to the successful commit\cite{RustlangRustCI}.
%The other branches are used in the release cycle below.
%
%\subsubsection{Release Cycles}
%One of the reasons for these branches and the importance of the reliable pipeline is the release interval of Rust, which is 6 weeks.
%This is relatively quick release cycle compared to other popular languages such as Go (release every 6 months\cite{ReleaseHistoryGo}), Java (release every 3 months\cite{authorWhenNextJava}), Python (Inconsistent releases about every 1-1.5 years\cite{PythonDeveloperGuide}) and Javascript (yearly\cite{JavaScriptVersions}).
%There has been much debate on how often Rust changes, which community member Steve Klabnik summarise quite well in his blog post "How often does Rust change?"\cite{HowOftenDoes}.
%In his conclusion he argues that small changes get more attention when the release cycle is smaller, and that features are available sooner.
%
%\subsection{Bots}\label{sec:bots}
%Just having a set of branches and strict policies does not make your project agile or DevOps.
%The Rust project have two different bots for bridging this gap.
%We will first take a look at the technical bot \textbf{Bors} in Section~\ref{sec:bors}.
%Then, the human-relations bot \textbf{Rustbot} is introduced in Section~\ref{sec:rustbot}.
%
%
%\subsubsection{Bors}\label{sec:bors}
%% https://bors.rust-lang.org
%% Bors is a instanse of homu https://github.com/rust-lang/homu
%% Bors purpose in life is to perform the merge, but before the merge re-run the tests. T his is because of the high velocity of rust making the tests ran at the time of the pr irrelevant by the time the actuall merge suppose to occur (https://github.com/rust-lang/homu#why-is-it-needed)
%The Bors bot's purpose in life is explained in the project's Readme on GitHub\cite{RustlangHomu2021}, which states its job as threefold:
%\begin{itemize}
%    \item Bundle low risk changes.
%    \item Forcing merges to the rust \textit{auto} branch(Section~\ref{sec:auto}).
%    \item Re-running tests before merging changes.
%\end{itemize}
%    
%Since there are a lot of low-risk changes such as documentation changes occurring, these can be ran together.
%%To do this, pull-requests are marked as "rollup"s, which instructs Bors to run the pull-request with other rollups.
%For this purpose maintainers instruct Bors to create so-called "rollup" pull-requests.
%These rollups encompass a set of changes that can be tested together without interference but without wasting the resources of invoking the build process needlessly often.
%Once Bors has been instructed to test a pull-request or rollup, it is placed in a queue to be ran sequentially.
%This is because there are many interconnected parts at play, and concurrent changes could introduce bugs as both tests pass independently.

\begin{figure}[H]
    \centering
        \begin{tikzpicture}[
            roundnode/.style={circle, draw=green!60, fill=green!5, very thick, minimum size=7mm},
            squarednode/.style={rectangle, draw=red!60, fill=red!5, very thick, minimum size=5mm},
        ]
        \node[roundnode]    (start)
        {New pull-request};
        \node[squarednode]   (disc)     [right=of start]
        {Discussions};
        \node[squarednode]   (test)     [right=of disc]
        {Run test};
        \node[squarednode]   (res)      [right=of test]
        {Results};
        \node[roundnode]    (merg)      [right=of res]
        {Merge};
        \node[roundnode]    (closed)    [below=of disc]
        {Closed PR};
        
        \node[draw,dotted,label=above:{Bors},fit=(test) (res) (merg)] {}; % This little box sure helped a lot!
        \node[draw,dotted,label=above:{Rustbot},fit=(disc)] {}; % This little box sure helped a lot!
        
          \draw[->] (start.east) -- (disc.west);
        \draw[->] (disc.east) -- (test.west);
        \draw[->] (test.east) -- (res.west);
        \draw[->] (res.east) -- (merg.west);
        
        \draw[->] (res.north) .. controls +(up:10mm) and +(left:0mm) .. (disc.north);
    
        \draw[->] (disc.south) -- (closed.north);
        \draw[->] (merg.south) .. controls +(down:20mm) and +(right:15mm) .. (closed.east);
    \end{tikzpicture}
    \caption{The PR workflow, where Bors operates.}
  \label{fig:borsflow}
\end{figure}

%To summarise Bors, it is a bot that extends the traditional task of a CI runner to automate the task of re-running the tests before merging code.
%This is to ensuring tests work not just at the time of the pull-request but also at the time the changes are merged in by running everything sequentially.


\subsubsection{Rustbot}\label{sec:rustbot}
% https://github.com/rust-lavong/triagebot
The Rustbot can be summarised as the people managing bot of the Rust development process.
Reading its documentation\cite{TriagebotRustForge} explains how it achieves this.
It manages teams and people not just in the Rust GitHub organisations but also elsewhere, keeping the right people up to date without broadcasting information to everyone. 
The Rustbot also keeps track of work-in-progress issues such as who is working on it, meaning issues and code review style tasks are assigned by interacting with the bot.
It also makes it easy to ping teams within the Rust organisation, for example the LLVM\footnote{LLVM is a open source, BSD-licensed compiler framework which the Rust compiler depends on.}-team working on the compiler infrastructure.
This makes it easy for the teams to assist without those in need of help knowing whom directly to contact.





\subsection{Action}\label{sec:actions}
% We probably want to explain how the build-pipe is started and where it runs before explaining the bots which glue it together
By analysing the actions file\cite{RustlangRusta} we see that all the GitHub actions ran on the Rust repository are triggered by actions done by Bors.
These build rust using a set of containers and caches before running tests and finally publishing results.
The containers are there for reproducible builds and the cache used is \textit{sccache}, since building the compiler and its tool from scratch would be a huge waste of resources.
The two most commonly ran set off actions are the tests ran on pull-requests and pushes to the \textit{auto} branch.
Pull-request actions include running all the automated tests and rebuilding the compiler (~40 minutes).
The pushes to the \textit{auto} branch trigger the large builds with over 50 different build-targets\cite{RustCIMoving} that make up the supported platforms (~3 hours).



%\{Cancelbot}
% https://forge.rust-lang.org/infra/docs/rustc-ci.html#cancelbot-to-keep-the-queue-short
% https://github.com/rust-lang/rust-central-station#archived-repository
% last link says it is no longer used.


\section{The contribution process}
% How bots and actions are linked together for humans
% https://rustc-dev-guide.rust-lang.org/getting-started.html#contributor-procedures
Following the official contribution guide to rust\cite{IntroductionGuideRustc} it looks fairly similar to other contribution guides with the addition of interaction with the bots, as long as you have simple bug fixes or corrections.
You are expected to have run a style check on your code before you create your pull request, as otherwise it will fail in the build-pipe, having wasted resources.
The reviews are done by a random member of the rust team unless a specific team(member) is requested through Rustbot.
Team members can also take tasks from each other with the Rustbots \lstinline{claim} command (the same is true about issues, they are self assigned with the same command).
\\

%If you have suggestions for new features or API changes to the language however, your pull-request implementing this may very likely be closed.
This is, unless your changes introduce a new feature or otherwise major addition to the language in which case the idea has to go through an RFC\footnote{Request For Comments} process first:
The RFC process is where the changes are publicly discussed among members of the responsible team of the affected areas and other members of the community.
This keeps the changes of Rust traceable over time, as well as help keep the API more consistent.\\

However, there is also a middle ground. Some changes are not API breaking nor fixes.
They can enter the intermediate phase directly, the FCP\footnote{Final Comment Period} which normal marks the end of the RFC. These are announced at the release of each new version, and normally closed the upcoming release.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}[
            roundnode/.style={circle, draw=green!60, fill=green!5, very thick, minimum size=7mm},
            squarednode/.style={rectangle, draw=red!60, fill=red!5, very thick, minimum size=5mm},
        ]
        \node[squarednode]  (rfc) {RFC};
        \node[squarednode]  (fcp) [right=of rfc]    {FCP};
        \node[squarednode]  (pr) [right=of fcp]     {Pull Request};
        
        \draw[->] (rfc.east) -- (fcp.west);
        \draw[->] (fcp.east) -- (pr.west);
    \end{tikzpicture}
    \caption{Changes go through discussion before being implemented.}
    \label{fig:contrisizes}
\end{figure}

A major change such as a keyword update would require a RFC, then have a announced FCP before finally someone can implement a associated pull-request.
Meanwhile, documentation changes would be reviewed and bundled in a rollup by Bors (Section~\ref{sec:bors}).
All these processes for different levels of changes help keep the project pipeline efficient.

\section{Conclusion}

After shining a light on different parts of rust's build process.

\newpage
\printbibliography
\end{document}

