/* matrix summation using pthreads

features: uses a barrier; the Worker[0] computes
the total sum from partial sums computed by Workers
and prints the total sum to the standard output

usage under Linux:
gcc matrixSum.c -lpthread
a.out size numWorkers

*/
#ifndef _REENTRANT 
#define _REENTRANT 
#endif 
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <sys/time.h>
#define MAXSIZE 10000  /* maximum matrix size */
#define MAXWORKERS 10   /* maximum number of workers */

pthread_cond_t go;        /* condition variable for leaving */
int numWorkers;           /* number of workers */ 
volatile int numArrived = 0;       /* number who have arrived */
volatile int min[3];
volatile int max[3];
pthread_mutex_t minmax[2];
pthread_mutex_t totallock;
pthread_mutex_t botlock;
int totalstore = 0;

double start_time, end_time; /* start and end times */
int size, stripSize;  /* assume size is multiple of numWorkers */
int sums[MAXWORKERS]; /* partial sums */
int total = 0;
int matrix[MAXSIZE][MAXSIZE]; /* matrix */

void *Worker(void *);

/* timer */
double read_timer() {
    static bool initialized = false;
    static struct timeval start;
    struct timeval end;
    if( !initialized )
    {
        gettimeofday( &start, NULL );
        initialized = true;
    }
    gettimeofday( &end, NULL );
    return (end.tv_sec - start.tv_sec) + 1.0e-6 * (end.tv_usec - start.tv_usec);
}

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {
    int i, j;
    long l; /* use long in case of a 64-bit system */
    pthread_attr_t attr;
    pthread_t workerid[MAXWORKERS];
    srand(time(NULL)); // randomize the rand() seerand giving same result

    /* set global thread attributes */
    pthread_attr_init(&attr);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

    /* read command line args if any */
    size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
    numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
    if (size > MAXSIZE) size = MAXSIZE;
    if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;

    /* initialize the matrix */
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            matrix[i][j] = 1;//rand()%99;
        }
    }
    min[0]= matrix[0][0];
    max[0]= matrix[0][0];

    /* print the matrix */
#ifdef DEBUG
    for (i = 0; i < size; i++) {
        printf("[ ");
        for (j = 0; j < size; j++) {
            printf(" %d", matrix[i][j]);
        }
        printf(" ]\n");
    }
#endif

    /* do the parallel work: create the workers */
    start_time = read_timer();
    for (l = 0; l < numWorkers; l++)
        pthread_create(&workerid[l], &attr, Worker, (void *) l);
    /* Wait for the workers */
    for (l = 0; l < numWorkers; l++)
        pthread_join(workerid[l], NULL);
    /* get end time */
    end_time = read_timer();
    /* print results */
    printf("The total is %d\n", totalstore);
    printf("The execution time is %g sec\n", end_time - start_time);
    printf("Min: %d@(%d,%d)\n", min[0], min[1], min[2]);
    printf("Max: %d@(%d,%d)\n", max[0], max[1], max[2]);
}

/* Each worker sums the values in one strip of the matrix.
   After a barrier, worker(0) computes and prints the total */
void *Worker(void *arg) {
    long myid = (long) arg;
    int i, j, first, last;
    int total = 0;

#ifdef DEBUG
    printf("worker %d (pthread id %d) has started\n", myid, pthread_self());
#endif

    /* sum values in my strip */
    while(true){
    pthread_mutex_lock(&botlock);
    if(numArrived < size){
        i=numArrived;
        numArrived++;
        pthread_mutex_unlock(&botlock);
    } else {
        pthread_mutex_unlock(&botlock);
        return NULL;
    }
    total=0;
    for (j = 0; j < size; j++){
        total += matrix[i][j];
        if(matrix[i][j] < min[0]){
            pthread_mutex_lock(&minmax[0]);
            if(matrix[i][j] < min[0]){
                min[0] = matrix[i][j];
                min[1] = i;
                min[2] = j;
            }
            pthread_mutex_unlock(&minmax[0]);
        }
        else if(matrix[i][j] > max[0]){
            pthread_mutex_lock(&minmax[1]);
            if(matrix[i][j] > max[0])
                max[0] = matrix[i][j];
            max[1] = i;
            max[2] = j;
            pthread_mutex_unlock(&minmax[1]);
        }
    }
    pthread_mutex_lock(&totallock);
    totalstore += total;
    pthread_mutex_unlock(&totallock);
    }
    return NULL;
}
