#include <cstdio>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <cmath>


sem_t  rest,  eat ;
volatile int food = 0;
int gather = 10;

void* parent(void* arg){
    while(true){
        sem_wait(&rest);
        std::cout << "Time to produce food!\n";
        std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) 200.0);
        std::cout << "found some worms!\n";
        food += gather;
        sem_post(&eat);
    }
}

void* baby(void* args){
    while(true){
        sem_wait(&eat);
        if(food==0){
            std::cout << "Wake upp mommy!\n";
            sem_post(&rest);
        } else {
            food--;
            sem_post(&eat);
            std::cout << "Bird ate some food, resting time!\n";
            std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) std::fmod((double)rand(), 300.0));
        }
    }
}

int main(int argc, char* argvs[]){
    int n = std::atoi(argvs[1]);
    gather = std::atoi(argvs[2]);
    food = gather;

    sem_init(&rest, 0, 0);
    sem_init(&eat,0,1);

    pthread_t eaters[n];

    pthread_create(&parentt, NULL, &parent, NULL);
    for(int i = 0; i < n; i++)
        pthread_create(&eaters[i], NULL, &baby, NULL);

    parent(NULL); // Loop forever

}
