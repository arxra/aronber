#pragma GCC target("avx,avx2")
#pragma GCC optimize("Ofast")
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <cmath>
#include <queue>
#include <map>
#include <set>
#include <thread>
#include <mpi.h>

#define WIN(v) while(cin >> v)
#define MAX(a, b) (((a) < (b)) ? (b) : (a))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define LISTS 3

using namespace std;

class personlist{
  protected:
    MPI_Status status;
  public:
    vector<int>ids;
    int dest, test, count,listenid, tag =1;

    void listen(){
      int count;
      while(1){
        count = 0;
        MPI_Recv(&test, 1, MPI_INT, listenid, tag, MPI_COMM_WORLD, &status);
        MPI_Recv(&count, 1, MPI_INT, listenid, tag, MPI_COMM_WORLD, &status);
        printf("Recieved: %d, count: %d\n", test, count);
        if(contains(test)){
          if(count == 3)
            cerr << "Found in all: " << test << " \n";
          else if (count == 5)
            MPI_Finalize();
          else{
            count ++;
            MPI_Send(&test, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
            MPI_Send(&count, 1, MPI_INT, dest, tag, MPI_COMM_WORLD);
            printf("Sent: %d (%d, tag:%d)\n", dest, test, tag);
          }
        } 
      }
    }

    void sendall (){
      int k = 1;
      for(int t : ids){
        MPI_Send(&t, 1, MPI_INT, dest, tag,MPI_COMM_WORLD);
        MPI_Send(&k, 1, MPI_INT, dest, tag,MPI_COMM_WORLD);
        printf("Sent: %d [%d, count:%d]\n", dest, t, k);
      }
    }

    bool contains(int k){
      cout << "Searching for  " << k << endl;
      return find(ids.begin(), ids.end(), k) !=ids.end();
    }
};

void yorktown(int id){
  personlist myslist;
  myslist.ids= {0,1,2,3,5,7,11,13,17,19,23,27};
  myslist.dest = (id+1)%3;
  myslist.listenid = (id + LISTS -1) %LISTS;
  printf("(%d)yourktown, send to %d and recive from %d\n", id, myslist.dest, myslist.listenid);
  myslist.sendall();
  myslist.listen();
}


void students(int id){
  personlist myslist;
  myslist.ids = {0,5,10,15,25,30};
  myslist.dest = (id+1)%3;
  myslist.listenid = (id + LISTS -1) %LISTS;
  printf("(%d)Students, send to %d and recive from %d\n", id, myslist.dest, myslist.listenid);
  myslist.listen();
}

void welfare(int id){
  personlist myslist;
  myslist.ids= {0,3,5,27,33};
  myslist.dest = (id+1)%3;
  myslist.listenid = (id + LISTS -1) %LISTS;
  printf("(%d)Welfare, send to %d and recive from %d\n", id,myslist.dest, myslist.listenid);
  myslist.listen();

}


int main(int argc, char ** argv) {
  int rank, size;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  if(size != LISTS)
    return 1;

  switch(rank){
    case(0):
      yorktown(0);
      break;
    case(1):
      students(1);
      break;
    case(2):
      welfare(2);
      break;
    default:
      MPI_Finalize();

  }



  while(1);
  MPI_Finalize();
  return 0;
}
