#include <mutex>
#include <condition_variable>

struct ourSemaphore
{
private:
   std::mutex mutex_;
   std::condition_variable condition_;
   unsigned long count_ = 0; // Initialized as locked.

public:
   //ourSemaphore(ourSemaphore &&);
   inline void notify() {
      std::lock_guard<decltype(mutex_)> lock(mutex_);
      ++count_;
      condition_.notify_one();
   }

   inline void wait() {
      std::unique_lock<decltype(mutex_)> lock(mutex_);
      while(!count_) // Handle spurious wake-ups.
      condition_.wait(lock);
      --count_;
      condition_.notify_one();
   }
};

class elevatorMessage{
   public:
   int target = 0;
   char op = '0';
   bool up = true;
   float pos = 0.0;

   bool operator()(const elevatorMessage &other, const elevatorMessage &thi)
      {return (other.up?thi.target<other.target : thi.target > other.target);}
};
