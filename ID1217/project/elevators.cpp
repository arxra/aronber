/*
*  Elevators send 2 codes:
*    p:
*      In the elevator, the button is pressed for going to a floor.
*    b:
*      a button outside the elvators is pressed for going up or down.
* */
#include <iostream>
#include <stdlib.h>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <cstring>
#include <string>
#include <vector>
#include <cmath>
#include <queue>
#include <map>
#include <set>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <thread>
#include <mutex>
#include <SFML/Network.hpp>

#include "elevators.h"

#define MAX(a, b) (((a) < (b)) ? (b) : (a))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define PORT 4711
#define CHAR 48
#define MAXEL 5
#define MARGIN 0.05


class ourMonitor{
public:
  sf::TcpSocket socket;
  int elevators=MAXEL;
  int top=MAXEL;

  std::vector<std::deque<elevatorMessage>> stops;    // where each elevator is going
  std::vector<std::deque<elevatorMessage>> messages; //not yet handled messages.
  std::vector<std::mutex> lockStops{MAXEL};
  std::vector<elevatorMessage> lastPos;              //last known position of each elevator
  std::vector<ourSemaphore> elevSem{MAXEL};                 //wait for messages to be assigned
  std::mutex sendLock;
  //establishes the conection via tcp socket to the java program
  ourMonitor(){ //constructor
    sf::IpAddress localIp("127.0.0.1");
    socket.setBlocking(true);

    //wait for the connection to be opend by the elevators
    do {
      socket.connect(localIp, PORT);
    } while (socket.getRemoteAddress() == sf::IpAddress::None);

    //char message[100] = {'m',' ','1',' ','1', '\n'};
    //if(socket.send(message, 100) != sf::Socket::Done)
    //std::cerr << "Failed to send data to socket!\n";
  }
  // sends the message it recives after locking
  void send (char* message, int size){
    std::lock_guard<std::mutex>guard(sendLock);
    printf("sending '%s'", message);
    socket.send(message, size);
    return;
  }
  //checks if our elevator is at its destination
  bool isAtStop(elevatorMessage stop, float pos){
    if(abs(stop.target - pos)<=0.05)//should be tuned
      return true;
    return false;
  }

  //starts a cyckle for opening and closing the elevator door
  void DoorOpeningCycle(int elevatorID){
    char elevator = (char)elevatorID+1+CHAR;
    char message[6] = {'d',' ',elevator,' ','1','\n'};
    send(message,6);
    sleep(5);
    char message2[7] = {'d',' ',elevator,' ','-','1','\n'};
    send(message2,7);
    sleep(3);
  }

  //sends the comand to move the coresponding elevator in the directions of its next stop
  // Should be locked from the outside
  bool moveElevator(int eID){
    char elevator = (char)eID+1+CHAR;
    if((stops[eID].front().target - lastPos[eID].pos) > 0.05){
      char message[6] = {'m',' ',elevator,' ','1', '\n'};
      send(message,6);
      return true;
    }else{
      char message2[7] = {'m',' ',elevator,' ','-','1', '\n'};
      send(message2,7);
      return false;
    }
  }

  //sends the comand to stop the coresponding elevator
  void stopElevator(int elevatorID){
    char message[6] = {'m',' ',(char)(elevatorID+1+CHAR),' ','0', '\n'};   //bad cast
    send(message,6);
  }
  // creates the message forchanging the indicaror in the respective elevators
  void indicator(int elevatorID, int pos){
    char elevator = (char)elevatorID+1+CHAR;
    char num = (char)pos+CHAR;
    char message[6] = {'s',' ',elevator,' ',num, '\n'};
    send(message,6);
  }

  //Check which elevator is the closes one to add to add route
  int FindClosestElevator(elevatorMessage mes){
    int shortestpath[elevators];
    for (int i = 0; i < elevators; i++){  //for each elevator queue
      lockStops[i].lock();               //lock before inserting
      shortestpath[i] = 1000;
      if(mes.target == lastPos[i].target)//if we are at that floor, just open doors
      return i;
      else if(stops[i].empty()){
        shortestpath[i] = abs(mes.target - lastPos[i].target);
      }else if(mes.up == lastPos[i].up && lastPos[i].up?(lastPos[i].target<mes.target):(lastPos[i].target>mes.target)){
        // If we are going in the same direction, are moving and are passing the target.
        int mindist = 1000, ps = 0;
        for(auto k : stops[i]){
          if(mindist > abs(k.target - mes.target) + ps){
            mindist = abs(k.target-mes.target)+ps;
            if(k.target == mes.target)
            break;
            ps++;}
          }
          shortestpath[i] = mindist;
        } else {
          shortestpath[i]= 0;
          bool swosh = mes.up;
          for(auto k: stops[i]){
            if(k.up == swosh){
              shortestpath[i] += abs(stops[i].back().target - lastPos[i].target) + 1;
            }else {
              swosh = !swosh;
              shortestpath[i] += abs(stops[i].back().target - lastPos[i].target) + 3;
            }
          }
        }
        lockStops[i].unlock();        //unlock before next iteration
      }

      int shortnow = 1000, t=0, r=0;
      for(int k : shortestpath){
        if(k < shortnow)
        shortnow = k, t=r;
        r++;
      }
      for(auto k : stops[t])
      if(k.target == mes.target && k.up==mes.up){
        printf("Found already in elevator queue\n");
        return -1;
      }
      return t;
    }

    void wait(int id){//wait for a message to be placed in the queue;
      elevSem[id].wait();
    }

    void listen(){
      printf("Hey listen\n");
      char message[100];
      std::size_t recBytes;
      elevatorMessage tmp = {0, '0'};
      do{
        std::memset(message,' ',sizeof(message));
        socket.receive(message, 100 , recBytes);//handle b;
        std::cout << "d: "<< message << std::endl;

        if(isalpha(message[0])){
          tmp.target = message[4] - CHAR;
          tmp.op = message[0];
          tmp.up = (message[2] == '1'?true:false);
          //done setting up regarding our message;

          if(tmp.op == 'b'){//not done
            tmp.target = message[2] - CHAR;
            tmp.up =(message[4] == '1'?true:false);
            int target = FindClosestElevator(tmp); //where to?
            printf("Found %d as target elevator\n", target +1);
            if (target == -1)
            continue;
            std::lock_guard<std::mutex> lock(lockStops[target]);        //lock before sorting
            {
              messages[target].push_back(tmp);    // give in parameters
            }
            elevSem[target].notify(); //awake the recieving thread
          }
          else if(tmp.op == 'f'){
            char tmpC[4];
            for (int i = 4; i<8;i++)
            tmpC[i-4] = message[i];

            tmp.pos = (float)atof(tmpC);
            messages[message[2]-CHAR-1].push_back(std::move(tmp)); //-char takes the char to an corresponging int
            elevSem[message[2]-CHAR-1].notify(); //awake the recieving thread
          }
          else if(tmp.op == 'p'){
            std::lock_guard<std::mutex> lock(lockStops[message[2]-CHAR - 1]);        //lock before sorting
            if((message[4]- CHAR) == 3){
              if(message[5]-CHAR == 2){
                tmp.target=32000;
                messages[message[2]-CHAR-1].push_front(tmp);
              }
            }else{
              messages[message[2]-CHAR-1].push_back(tmp);
            }
            elevSem[ message[2]-CHAR-1].notify(); //awake the recieving thread
          }
          else
          printf("Operation not recognized\n");
        }
      }while(true);
    }
  };


  ourMonitor ourMon;



  auto elevator(int elevatorID){
    //elevatorMessage stop = {0, '0'}; //where are we going?
    elevatorMessage tmp = {0, '0'};
    elevatorMessage currmes = {0, '0'};
    ourMon.lastPos[elevatorID]=(tmp);
    ourMon.lastPos[elevatorID].up = true;
    bool still = true, paused = false, lastdirr = true;
    int floorIndicatoir = 0;
    printf("elevator thread %d spawned\n",elevatorID+1 );

    while(true){
      ourMon.wait(elevatorID);
      if(ourMon.messages[elevatorID].size() > 0 ){
        currmes = ourMon.messages[elevatorID].front();
        switch(currmes.op){
          case 'b':

          case 'p': // from within elevator
          if(currmes.target == 32000){//code for STOP! Handeld first -> quickest.
            still=true;
            ourMon.stopElevator(elevatorID);
            paused = !paused;
          }else{
            bool uniq = true;
            ourMon.lockStops[elevatorID].lock();        //lock before inserting
            if(ourMon.stops[elevatorID].size() > 0){
              for(auto s : ourMon.stops[elevatorID])
              if (s.target == currmes.target&& s.up == currmes.up)
              uniq = false;
            }
            if(uniq){
              if(ourMon.stops[elevatorID].size() == 0){
                if(currmes.op == 'p')
                currmes.up = currmes.target > ourMon.lastPos[elevatorID].pos;
                ourMon.stops[elevatorID].emplace_back(std::move(currmes));
              } else {
                int i =0, part=0;
                for(; i < (int)ourMon.stops[elevatorID].size(); i++){
                  if(ourMon.lastPos[elevatorID].pos < currmes.target &&
                    ourMon.lastPos[elevatorID].up == (ourMon.stops[elevatorID][i].up == currmes.up)  ){
                      if(ourMon.stops[elevatorID][i].target > currmes.target && ourMon.stops[elevatorID][i].up){
                        if(currmes.op == 'p')
                        currmes.up = true;
                        ourMon.stops[elevatorID].emplace(ourMon.stops[elevatorID].begin()+i, currmes);
                        break;
                      }
                      part++;
                    }
                    else if(ourMon.lastPos[elevatorID].pos > currmes.target &&
                      (ourMon.lastPos[elevatorID].up == (ourMon.stops[elevatorID][i].up == currmes.up))){
                        //!ourMon.lastPos[elevatorID].up== ourMon.stops[elevatorID][i].up
                        if(ourMon.stops[elevatorID][i].target < currmes.target && !ourMon.stops[elevatorID][i].up){
                          if(currmes.op == 'p')
                          currmes.up = false;
                          ourMon.stops[elevatorID].emplace(ourMon.stops[elevatorID].begin()+i, currmes);
                          break;
                        }
                        part++;
                      }
                    }
                    if(i == (int)ourMon.stops[elevatorID].size()){
                      if(part != 0){
                        if(currmes.op == 'p')
                        currmes.up = currmes.target > ourMon.stops[elevatorID][part-1].target;
                        ourMon.stops[elevatorID].emplace(ourMon.stops[elevatorID].begin()+ part,currmes);
                      }else{
                        if(currmes.op == 'p')
                        currmes.up = currmes.target > ourMon.stops[elevatorID][i-1].target;
                        ourMon.stops[elevatorID].emplace_back(currmes);
                      }
                    }
                  }
                  ourMon.lockStops[elevatorID].unlock();        //lock before inserting
                }
                //stop = ourMon.stops[e-levatorID].front(); //set next stop to where we are going. Do not remove yet.
              }

              break;
              case 'f':
              if(ourMon.lastPos[elevatorID].pos != currmes.pos){
                ourMon.lastPos[elevatorID].pos = currmes.pos;
                ourMon.lockStops[elevatorID].lock();        //lock before inserting
                if(ourMon.isAtStop(ourMon.stops[elevatorID].front(),currmes.pos)){
                  ourMon.stopElevator(elevatorID);
                  ourMon.DoorOpeningCycle(elevatorID);
                  if(ourMon.stops[elevatorID].front().up != lastdirr){
                    for(int i = 1; i < (int)ourMon.stops[elevatorID].size(); i++){
                      if(abs(ourMon.stops[elevatorID][i].target - ourMon.lastPos[elevatorID].pos) < MARGIN &&
                        ourMon.stops[elevatorID][0].up == ourMon.stops[elevatorID][i].up){
                        ourMon.stops[elevatorID].erase(ourMon.stops[elevatorID].begin()+i);
                      }
                    }
                  }
                  still = true;
                  ourMon.stops[elevatorID].pop_front();
                  ourMon.lastPos[elevatorID].up = ourMon.stops[elevatorID].front().up;

                }
                ourMon.lockStops[elevatorID].unlock();        //lock before inserting
              }
              break;
              default:
              printf("Unhandled message parameter to elevator\n");

            }
            ourMon.messages[elevatorID].pop_front();//we are done with that message
          }
          if(!paused){
            ourMon.lockStops[elevatorID].lock();

            if(ourMon.stops[elevatorID].size() >0){ //lock before inserting
              //set next stop to where we are going. Do not remove here.
              if (still){
                lastdirr = ourMon.moveElevator(elevatorID);
                still= false;
              }
            }
            ourMon.lockStops[elevatorID].unlock();

            if(floorIndicatoir != round(ourMon.lastPos[elevatorID].pos)){
              floorIndicatoir = (int)round(ourMon.lastPos[elevatorID].pos);
              ourMon.indicator(elevatorID,floorIndicatoir);
            }
          }
        }
      }

      int main(int args, char ** argv) {
        //main input
        std::cout << std::setprecision(250);

        if(args <3){
          std::cerr << "Not enough arguments!\n";
          printf("Usage: %s #elevators #buildinghight\n", argv[0]);
          return 1;
        }

        ourMon.elevators = std::atoi(argv[1]);
        ourMon.top = std::atoi(argv[2]);


        //start all threads
        std::cout << "Starting all threads\n";

        ourMon.stops.resize(ourMon.elevators);
        ourMon.messages.resize(ourMon.elevators);
        ourMon.lastPos.resize(ourMon.elevators);

        std::thread k[MAXEL];

        for(int i = 0; i < ourMon.elevators; i++){
          k[i]=std::thread(elevator, i);
        }

        ourMon.listen();

        for(int i =0; i < ourMon.elevators; i++)
        k[i].join();
      }
