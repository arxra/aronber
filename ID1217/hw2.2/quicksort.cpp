#include <algorithm>
#include <iostream>
#include <vector>
#include <atomic>
#include <omp.h>

#define RUNS 10

using namespace std;

vector<int> tosort;
int maxthreads = 1;

struct lims{
  int low;
  int high;
};


void* quicksortpartition(void* arguments){

  struct lims *args = (struct lims*)arguments;
  int pivot = tosort[args->high];

  for(int i = args->low; i<args->high; i++){
    if(tosort[i] < pivot){
      swap(tosort[i], tosort[args->low]);
      args->low++;
    }
  }
  swap(tosort[args->low], tosort[args->high]);
  return args;
}

void* quicksort(void* arguments){

  auto args = (struct lims*) arguments;
  struct lims defargs = *args;
  if(defargs.high <= defargs.low ){
    return arguments;
  }
  struct lims newargs = *(struct lims*)quicksortpartition((void*) args);
  int p = newargs.low;

  struct lims argarr[2];
  argarr[0] = {defargs.low, p-1};
  argarr[1] = {p+1, defargs.high};

#pragma omp task
  quicksort(&argarr[1]);
  //only run argarr[1] as task, let main thread run other
#pragma omp task
  quicksort(&argarr[0]);
#pragma omp taskwait

  return arguments;
}


int main(int argc, char const *const*argv) {
  double start_time, end_time;
  int input;

  maxthreads = atoi(argv[1]);
  while(std::cin >> input){
    tosort.emplace_back(input);
  }

  struct lims arg = {0, (int)tosort.size()-1};

  for(int i = 1; i <= maxthreads; i *=2){
    omp_set_num_threads(i);
    for(int k = 0; k < RUNS; k++){
      std::random_shuffle(tosort.begin(), tosort.end());//shuffle the list first for best result
      start_time = omp_get_wtime();
#pragma omp parallel
      {
#pragma omp single nowait
        {
          quicksort(&arg);
        }
      }
      end_time = omp_get_wtime();
      cout << i << " " << end_time - start_time << endl;
      arg = {0, (int)tosort.size()-1};
    }
    std::cerr << "# done sorting on  " << i << " Threads" << endl;
  }

//for(auto k : tosort)
//    std::cout << k << endl;

  return 0;
}
