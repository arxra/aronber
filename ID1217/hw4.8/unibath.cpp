/* 8. The Unisex Bathroom Problem  (40 points)
   Suppose there is only one bathroom in a building.
   It can be used by any number of men or any number of women, but not at the same time. 
   Develop a monitor to synchronize the use of the bathroom. The monitor has four public procedures: 
   manEnter, manExit, womanEnter, and womanExit. 
   A man process calls manEnter to get permission to use the bathroom and calls manExit when finished.
   A woman process calls womanEnter and womanExit.
   Use the Signal and Continue signaling discipline (which is the default discipline).
   Your solution has to ensure the required exclusion and avoid deadlock, and ensure fairness, 
   i.e. ensure that any person (man or woman) which is waiting to enter the bathroom eventually gets to do so
   Implement a multithreaded application in Java or C++ to simulate the actions of the "man" and "woman" processes
   represented as concurrent threads and the bathroom represented as the developed monitor. 
   The output from your program should be a trace of the significant events that occur, 
   such as a person wanting to enter the bathroom, entering or exiting the room. 
   Each line of output should contain a time stamp, the identity of the person associated with the event, 
   and a short descriptive message. Write the trace to the standard output.
   */
#include <cstdio>
#include <pthread.h>
#include <semaphore.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <cmath>
#include <vector> 
#include <atomic>


#define GENDERS 2

double bathroomms = 4.20, nobathroomms=69.0;


class bathroom{
    //2 brues for 2 genders;
    protected:
        pthread_cond_t brueLock[GENDERS];
        volatile bool currentuser = true;
        volatile bool allowed = true;
        pthread_mutex_t countLock;
        volatile int inBathroom = 0;


        void Enter(bool gen){
            pthread_mutex_lock(&countLock);
            if(currentuser == gen && allowed){
                inBathroom++;
                pthread_mutex_unlock(&countLock);
                std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) std::fmod((double)rand(),bathroomms));
                return;
            }
            while(currentuser != gen || !allowed)
                pthread_cond_wait(&brueLock[gen], &countLock);
            inBathroom++;
            pthread_mutex_unlock(&countLock);
            std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) std::fmod((double)rand(),bathroomms));
        }
        void Exit(bool gen){
            pthread_mutex_lock(&countLock);
            inBathroom--;
            if(inBathroom ==0){
                currentuser = !currentuser;
                std::cout << "I was the last " << (gen?"male":"woman") << " in there, so I switched queue.\n";
                pthread_cond_broadcast(&brueLock[currentuser]);
            }
            pthread_mutex_unlock(&countLock);
        }

    public: 
        bathroom(){
            pthread_mutex_init(&countLock, NULL);
            for(int i = 0; i < GENDERS; i++)
                pthread_cond_init(&brueLock[i], NULL);
        }
        void mon(){
            while(true){
                std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) (5*nobathroomms));
                printf("bathroom awoke!\n");
                pthread_mutex_lock(&countLock);
                if(inBathroom == 0){
                    std::cout << "empty bathroom? switching roles! r: " << currentuser << "->";
                    allowed = true; 
                    currentuser = !currentuser;
                    std::cout <<  currentuser << std::endl;
                    std::cerr << "Signaling " << currentuser << " to start!\n";
                    pthread_cond_broadcast(&brueLock[currentuser]);
                }
                else
                    allowed = false;
                pthread_mutex_unlock(&countLock);
                //endwhile
            }
        }
        void manEnter(){
            Enter(true);
        }
        void manExit(){
            Exit(true);
        }
        void womanEnter(){
            Enter(false);
        }
        void womanExit(){
            Exit(false);
        }
};

struct gender {
    bool male;
    int nr;
};

bathroom br;

void* person(void *arg){
    gender *me = (gender*)arg;

    while(true){
        //you don't always nee the bathroom
        printf("%s[%d] I need to pee!\n", (me->male? "Man":"Woman"), me->nr);
        (me->male?br.manEnter():br.womanEnter()); // Man or woman enters bathroom
        std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) (bathroomms));
        printf("%s[%d] That was nice, I can now return to work!\n", (me->male? "Man":"Woman"), me->nr);
        (me->male?br.manExit():br.womanExit());   // Man or woman exits bathroom
        std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) (nobathroomms));
    }
    return NULL;
}

int main (int args, char* argv[]){

    if(args < 2){
        printf("Not enough arguments, ./%s #menandwomen\n", argv[0]);
        return 1;
    }

    int threads = std::atoi(argv[1]);
    gender threadArgs[GENDERS][threads];
    pthread_t people[GENDERS][threads];

    for(int i = 0; i < threads; i++){
        for(int ii = 0; ii<GENDERS; ii++){
            threadArgs[ii][i] = {(ii==0? true:false),i};
            pthread_create(&people[ii][i], NULL, person, (void *) &threadArgs[ii][i]);
        }
    }
    br.mon();
}

