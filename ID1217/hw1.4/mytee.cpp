#include <stdlib.h>
#include <iostream>
#include <pthread.h>
#include <fstream>
#include <vector>


std::vector<std::string> input;
std::string file;
/*
 * NOTE:
 * readlines and lastline are volatile because they might change between reads.
 * however, reading from a variable being written is UB.
 * change volatile for
 * std::atomic<int> 
 * for atomicity.
 * This does however tax the runtime.
 * */

/*
 *  NOTE:
 *  The program is actually faster if printout() is called after it's done reading.
 *  this is most likely due to the additional overhead of spinning, causing more 
 *  user and system time to be recorded by the "time" command. 
 *  However, it does utilize more cpu. On avarege, 150% when callin printout after
 *  and 200% when calling before. 
 *  Utilizing condition variables would have been optimal here. But this works.
 * */
volatile int readlines = 0;
volatile int lastline = -5;

void* printfile(void *){
  std::ofstream fd(file);
  for(int i =0; i != lastline +1 ;i++){
    while(readlines <= i)if(i==lastline || i == lastline +1)break;
    fd << input[i] << std::endl;
  }
  fd << std::endl;
  fd.close();
  void* j;
  return j;
}

void* printout(void*){
  for(int i =0; i!=lastline+1;i++) {
    while(readlines <= i)if(i==lastline || i == lastline +1)break;
    std::cout << input[i] << std::endl;
  }
  std::cout << std::endl;
  void* j;
  return j;
}

int main(int args, char **argv){

  file= argv[1];

  pthread_t tr[2];
  pthread_create(&tr[0], NULL, &printfile, NULL);
  pthread_create(&tr[1], NULL, &printout, NULL);

  std::string tmp;
  while(getline(std::cin, tmp)){
    input.emplace_back(std::move(tmp));
    readlines++;
  }
  lastline = readlines;

  pthread_join(tr[0], NULL);
  pthread_join(tr[1], NULL);
}
