/* matrix summation using OpenMP

   usage with gcc (version 4.2 or higher required):
   gcc -O -fopenmp -o matrixSum-openmp matrixSum-openmp.c 
   ./matrixSum-openmp size numWorkers

*/


double start_time, end_time;

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define MAXSIZE 10000  /* maximum matrix size */
#define MAXWORKERS 8   /* maximum number of workers */

int numWorkers;
int size; 
int matrix[MAXSIZE][MAXSIZE];
void *Worker(void *);
int min[3];
int max[3];

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {
    int i, j, total=0;

    /* read command line args if any */
    size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
    numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
    if (size > MAXSIZE) size = MAXSIZE;
    if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;

    omp_set_num_threads(numWorkers);

    /* initialize the matrix */
    printf("[");
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            matrix[i][j] = rand()%99;
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    printf("]");

    min[0] = matrix[0][0];
    min[1]=1;
    min[2]=2;

    max[0] = matrix[0][0];
    max[1]=1;
    max[2]=2;

    start_time = omp_get_wtime();
#pragma omp parallel for reduction (+:total) private(j)
    for (i = 0; i < size; i++)
        for (j = 0; j < size; j++){
            total += matrix[i][j];
            if(max[0] < matrix[i][j]){
#pragma omp critical
                if(max[0] < matrix[i][j]){
                    max[0] = matrix[i][j];
                    max[1] = i;
                    max[2] = j;
                }
            }
            else if(min[0] > matrix[i][j]){
#pragma omp critical
                if(min[0] > matrix[i][j]){
                    min[0] = matrix[i][j];
                    min[1] = i;
                    min[2] = j;
                }
            }
            // implicit barrier
        }

            end_time = omp_get_wtime();

            printf("the total is %d\n", total);
            printf("Min: %d, (%d, %d)\n", min[0], min[1],min[2]);
            printf("Max: %d, (%d, %d)\n", max[0], max[1],max[2]);
            printf("it took %g seconds\n", end_time - start_time);

}
