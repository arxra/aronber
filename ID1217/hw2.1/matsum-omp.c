#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define MAXSIZE 2000000  /* maximum matrix size */
#define MAXWORKERS 16   /* maximum number of workers */

/* read command line, initialize, and create threads */
int main(int argc, char *argv[]) {

  int min[3];
  int max[3];
  int numWorkers;
  int size; 
  int i, j, total=0;
  double start_time, end_time;
  int matrix[MAXSIZE][MAXSIZE];

  /* read command line args if any */
  size = (argc > 1)? atoi(argv[1]) : MAXSIZE;
  numWorkers = (argc > 2)? atoi(argv[2]) : MAXWORKERS;
  if (size > MAXSIZE) size = MAXSIZE;
  if (numWorkers > MAXWORKERS) numWorkers = MAXWORKERS;

  omp_set_num_threads(numWorkers);

  /* initialize the matrix */
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      matrix[i][j] = rand()%99;
    }
  }

  min[0] = matrix[0][0];
  min[1]=0;
  min[2]=0;

  max[0] = matrix[0][0];
  max[1]=0;
  max[2]=0;

  printf("test\n");
  start_time = omp_get_wtime();
#pragma omp parallel for reduction (+:total) private(j)
  for (i = 0; i < size; i++)
    for (j = 0; j < size; j++){
      total += matrix[i][j];
      if(max[0] < matrix[i][j]){
#pragma omp critical
        if(max[0] < matrix[i][j]){
          max[0] = matrix[i][j];
          max[1] = i;
          max[2] = j;
        }
      }
      else if(min[0] > matrix[i][j]){
#pragma omp critical
        if(min[0] > matrix[i][j]){
          min[0] = matrix[i][j];
          min[1] = i;
          min[2] = j;
        }
      }
      // implicit barrier
    }
  end_time = omp_get_wtime();
  printf("%d\t%d\t%f\n", numWorkers, size ,end_time-start_time);
  return 0;
}
