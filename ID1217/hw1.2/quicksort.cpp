#include <algorithm>
#include <iostream>
#include <vector>
#include <atomic>
#include <pthread.h>
#include <omp.h>

#define RUNS 10

using namespace std;

pthread_mutex_t threadslock;
int threads;
vector<int> tosort;
int maxthreads;

struct lims{
  int low;
  int high;
};


void* quicksortpartition(void* arguments){

  struct lims *args = (struct lims*)arguments;
  int pivot = tosort[args->high];

  for(int i = args->low; i<args->high; i++){
    if(tosort[i] < pivot){
      swap(tosort[i], tosort[args->low]);
      args->low++;
    }
  }
  std::swap(tosort[args->low], tosort[args->high]);
  return args;
}

void* quicksort(void* arguments){

  auto args = (struct lims*) arguments;
  struct lims defargs = *args;
  if(defargs.high <= defargs.low ){
    return arguments;
  }
  struct lims newargs = *(struct lims*)quicksortpartition((void*) args);
  int p = newargs.low;

  struct lims argarr[2];
  argarr[0] = {defargs.low, p-1};
  argarr[1] = {p+1, defargs.high};

  pthread_mutex_lock(&threadslock);
  if(threads < maxthreads && argarr[1].high - argarr[1].low > 10){
    threads++;
    pthread_mutex_unlock(&threadslock);
    pthread_t childrin;

    pthread_create(&childrin, NULL, quicksort, (void*)&argarr[1]);
    quicksort(&argarr[0]);
    pthread_join(childrin, NULL);

    pthread_mutex_lock(&threadslock);
    threads--;
    pthread_mutex_unlock(&threadslock);
  }else {
    pthread_mutex_unlock(&threadslock);
    for(int i =0; i < 2; i++)
      quicksort(&argarr[i]);
  }

  return arguments;
}


int main(int argc, char const *const*argv) {
  double start_time, end_time;
  int input;

  maxthreads = atoi(argv[1]);
  cout << "# Sorting on " << maxthreads << " threads" << endl;
  while(std::cin >> input){
    tosort.emplace_back(input);
  }

  struct lims arg = {0, (int)tosort.size()-1};

  for(int r = 1; r <= maxthreads; r *=2){
    maxthreads = r;
    for(int k = 0; k < RUNS; k++){
      std::random_shuffle(tosort.begin(), tosort.end());//shuffle the list first for best result
      start_time = omp_get_wtime();

      quicksort(&arg);

      end_time = omp_get_wtime();
      cout << r << " " << end_time - start_time << endl;
      arg = {0, (int)tosort.size()-1};
    }
    std::cerr << "# done sorting on  " << r << " Threads" << endl;
  }

  return 0;
}
