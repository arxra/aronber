#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>
#include <thread>
#include <chrono>
#include <cmath>

#define POTSIZE 5

sem_t pot, fill;
int potsize = POTSIZE;
int potfill = 0;
int filledtimes = 0;

void bear(){
    while(true){
        sem_wait(&fill); //consume the awake state
        if(potfill != potsize){
            std::cerr << "Bear: Pot was not exactly filled but the bear is awake: Error!\n";
        }else{
            std::cout << "Bear: Look at all that honey!\n";
            //std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) 250.0);
            potfill = 0;
            filledtimes++;
            std::cout << "Bear: Ate some tasty honey for the "<< filledtimes <<" time!\n";
        }
        sem_post(&pot);
    }
}

void* bee(void* arg){
    while(true){
        sem_wait(&pot);
        if(potfill == potsize){
            std::cout << "The pot is full!\n";
            sem_post(&fill);//awake the bear
        }else{
            potfill++;
            std::cout << "Bee: Filled the pot with more honey. it's now " << potfill << "/" << potsize << " full!\n";
            sem_post(&pot);
        }
    }
    //std::this_thread::sleep_for((std::chrono::duration<double, std::milli>) (std::fmod((double)rand(), 500.0)+200.0));
}

int main (int args, char* argv[]){

    int bees = atoi(argv[1]);
    potsize = atoi(argv[2]);

    sem_init(&pot, 0, 1);
    sem_init(&fill, 0, 0);

    pthread_t bee_threads[bees];

    for(int i =0;i<bees; i++){
        pthread_create(&bee_threads[i], NULL, bee, NULL);
    }
    bear();
}


