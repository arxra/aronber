import java.util.concurrent.Semaphore;

/**
 * The DeadLock program demonstrates how two or more threads deadlock while
 * trying to acquire the same set of resources. The resources consist of an
 * array of semaphores. Each thread (Contender) has a randomized sequence of
 * acquisition.
 * 
 * Interesting observation: If all threads have the same resource acquisition
 * sequence they do not lock. Why is that, you think?
 */
public class DeadLock {

  /**
   * An array of Semphore that represents a generic set of non-sharable resources.
   */
  private final Semaphore[] resources;

  /**
   * Creates a new DeadLock with the given number of resources. Each contender
   * must allocate all resources before it can complete a successful run. A run is
   * the (empty) representation of some operation for which the resources are
   * required.
   */
  public DeadLock(int nofResources) {
    resources = new Semaphore[nofResources];
    for (int i = 0; i < resources.length; i++)
      resources[i] = new Semaphore(1, true);
  }

  /**
   * A Contender represents a process which must allocate all resources in order
   * to complete a successful run.
   */
  private class Contender implements Runnable {

    // The Contender will acquire the resources in this order.
    private int[] acquisitionSequence = new int[resources.length];

    // The name of this Contender, for printing purposes.
    private String myName = "";

    // The number of successful runs.
    private int runCounter = 0;

    /**
     * Creates a new Contender with the given name and scramble flag.
     * 
     * @param name     The name of this contender (for printing)
     * @param scramble If true, the resource acquisition sequence is randomized
     */
    public Contender(String name, boolean scramble) {

      myName = name;

      for (int i = 0; i < acquisitionSequence.length; i++)
        acquisitionSequence[i] = i;

      if (scramble) {
        // Scramble the sequence randomly
        for (int i = 0; i < acquisitionSequence.length; i++) {
          // Select two random elements, x and y
          int x = (int) (Math.random() * acquisitionSequence.length);
          int y = (int) (Math.random() * acquisitionSequence.length);
          // Swap x and y
          int temp = acquisitionSequence[x];
          acquisitionSequence[x] = acquisitionSequence[y];
          acquisitionSequence[y] = temp;
        }
      }

      System.out.printf("%s has sequence %s%n", myName, java.util.Arrays.toString(acquisitionSequence));

      new Thread(this).start();
    }

    /**
     * Support method for sleeping.
     */
    private void snooze(long ms) {
      try {
        Thread.sleep(ms);
      } catch (InterruptedException irx) {
      }
    }

    private String nth(int n) {
      String s = "th";
      if (n == 1)
        s = "st";
      else if (n == 2)
        s = "nd";
      else if (n == 3)
        s = "rd";
      return s;
    }

    /**
     * Acquire all resources in the established sequence. Sleep for 0-99 ms between
     * each allocation.
     */
    private void acquireAllGreedily() {
      for (int i = 0; i < acquisitionSequence.length; i++) {
        int j = i + 1;
        int r = acquisitionSequence[i];
        System.out.printf("%s waits for resource %d (%d%s)%n", myName, r, j, nth(j));
        resources[r].acquireUninterruptibly();
        System.out.printf("%s has acquired resource %d (%d%s)%n", myName, r, j, nth(j));
        snooze((long) (100 * Math.random()));
      }
      System.out.printf("%s has acquired all resources%n", myName);
    }

    /**
     * Release all resources in the established sequence.
     */
    private void releaseAll() {
      for (int i = 0; i < acquisitionSequence.length; i++) {
        resources[acquisitionSequence[i]].release();
      }
      System.out.printf("%s has released all resources%n", myName);
    }

    /**
     * The infinite loop of this Contender. It tries to acquire all its resources,
     * release them, and then sleeps for 0-99 ms before starting over.
     */
    public void run() {

      System.out.printf("%s starting%n", myName);
      // Initial random sleep to decorrelate the threads.
      snooze((long) (1000 + 1000 * Math.random()));

      for (;;) {
        acquireAllGreedily();
        snooze((long) (100 * Math.random()));
        releaseAll();
        System.out.printf("%s has completed %d runs%n", myName, ++runCounter);
      }
    }
  }

  /**
   * Create and start the given number of contenders.
   * 
   * @param nofContenders The number of contenders (threads) to run
   * @param scramble      If true, each Contender randomizes its acquisition
   *                      sequence. If false, each Contender allocates each
   *                      resource in the same order.
   */
  public void execute(int nofContenders, boolean scramble) {
    for (int i = 0; i < nofContenders; i++) {
      new Contender(String.format("Contender%d", i + 1), scramble);
    }
  }

  public static void printHelp() {
    System.out.println("Usage: -h|[-r num][-c num][-s]");
    System.out.println("where:");
    System.out.println("-r num  The number of resources");
    System.out.println("-c num  The number of threads contending for all resources");
    System.out.println("-s      Turns off acquisition sequence scrambling");
    System.out.println("-h      Prints this help and exits");
  }

  public static void main(String[] args) {
    boolean scramble = true;
    int nofResources = 3;
    int nofContenders = 2;
    int state = 0;

    for (String ar : args) {
      switch (state) {

        case 0:
          if (ar.equals("-s"))
            scramble = false;
          else if (ar.equals("-r"))
            state = 1;
          else if (ar.equals("-c"))
            state = 2;
          else if (ar.equals("-h")) {
            printHelp();
            return;
          }
          break;

        case 1:
          nofResources = Integer.parseInt(ar);
          state = 0;
          break;

        case 2:
          nofContenders = Integer.parseInt(ar);
          state = 0;
          break;
      }
    }

    System.out.printf("Resources = %d, contenders = %d%n", nofResources, nofContenders);

    new DeadLock(nofResources).execute(nofContenders, scramble);
  }
}
