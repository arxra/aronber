import se.kth.edx.id2203.core.ExercisePrimitives.AddressUtils._
import se.kth.edx.id2203.core.Ports._
import se.kth.edx.id2203.validation._
import se.sics.kompics.network._
import se.sics.kompics.sl.{Init, _}
import se.sics.kompics.{KompicsEvent, ComponentDefinition => _, Port => _}
import scala.language.implicitConversions
import scala.collection.mutable.ListBuffer;

import se.sics.kompics.timer.{ScheduleTimeout, Timeout, Timer}
case class Prepare(proposalBallot: (Int, Int)) extends KompicsEvent;
case class Promise(
    promiseBallot: (Int, Int),
    acceptedBallot: (Int, Int),
    acceptedValue: Option[Any]
) extends KompicsEvent;
case class Accept(acceptBallot: (Int, Int), proposedValue: Any)
    extends KompicsEvent;
case class Accepted(acceptedBallot: (Int, Int)) extends KompicsEvent;
case class Nack(ballot: (Int, Int)) extends KompicsEvent;
case class Decided(decidedValue: Any) extends KompicsEvent;

/** This augments tuples with comparison operators implicitly, which you can use in your code, for convenience.
  * examples: (1,2) > (1,4) yields 'false' and  (5,4) <= (7,4) yields 'true'
  */
implicit def addComparators[A](x: A)(implicit o: math.Ordering[A]): o.OrderingOps = o.mkOrderingOps(x);

//HINT: After you execute the latter implicit ordering you can compare tuples as such within your component implementation:
(1,2) <= (1,4);

class Paxos(paxosInit: Init[Paxos]) extends ComponentDefinition {

  //Port Subscriptions for Paxos

  val consensus = provides[Consensus];
  val beb = requires[BestEffortBroadcast];
  val plink = requires[PerfectLink];

  //Internal State of Paxos
  val (rank, numProcesses) = paxosInit match {
    case Init(s: Address, qSize: Int) => (toRank(s), qSize)
  }

  //Proposer State
  var ts = 0; // Logical paxos round clock
  var proposedValue: Option[Any] = None;
  var promises: ListBuffer[((Int, Int), Option[Any])] = ListBuffer.empty;
  var numOfAccepts = 0;
  var decided = false;

  //Acceptor State
  var promisedBallot = (0, 0);
  var acceptedBallot = (0, 0);
  var acceptedValue: Option[Any] = None;

  def propose() = {
    /*
    INSERT YOUR CODE HERE
     */
    if (!decided) {
      ts += 1;
      numOfAccepts = 0;
      promises = ListBuffer.empty;
      trigger(BEB_Broadcast(Prepare((ts, rank))) -> beb);
    }
  }

  consensus uponEvent {
    case C_Propose(value) => {
      /*
      INSERT YOUR CODE HERE
       */
      proposedValue = Some(value);
      propose()
    }
  }

  beb uponEvent {

    case BEB_Deliver(src, prep: Prepare) => {
      /*
      INSERT YOUR CODE HERE
       */
      if (promisedBallot < prep.proposalBallot) {
        promisedBallot = prep.proposalBallot;
        trigger(
          PL_Send(
            src,
            Promise(promisedBallot, acceptedBallot, acceptedValue)
          ) -> plink
        );
      } else {
        trigger(PL_Send(src, Nack(prep.proposalBallot)) -> plink);
      }
    };

    case BEB_Deliver(src, acc: Accept) => {
      //Accept(acceptBallot: (Int, Int), proposedValue: Any)
      /*
      INSERT YOUR CODE HERE
       */
      if (promisedBallot <= acc.acceptBallot) {
        acceptedBallot = acc.acceptBallot;
        promisedBallot = acceptedBallot;
        acceptedValue = Some(acc.proposedValue);
        //  Accepted(acceptedBallot: (Int, Int));
        trigger(PL_Send(src, Accepted(acceptedBallot)) -> plink);
      } else {
        //  Nack(ballot: (Int, Int))
        trigger(PL_Send(src, Nack(acc.acceptBallot)) -> plink);
      }
    };

    case BEB_Deliver(src, dec: Decided) => {
      /*
      INSERT YOUR CODE HERE
       */
      if (!decided) {
        trigger(C_Decide(acceptedValue.get()) -> consensus);
        decided = true;
      }
    }
  }

  plink uponEvent {

    case PL_Deliver(src, prepAck: Promise) => {
      //Promise(promiseBallot: (Int, Int), acceptedBallot: (Int, Int), acceptedValue: Option[Any])
      if ((ts, rank) == prepAck.promiseBallot) {
        // we are delived a promise of our value!

        /*
        INSERT YOUR CODE HERE
         */
        promises += ((prepAck.acceptedBallot, prepAck.acceptedValue));
        if (promises.size == (1 + numProcesses / 2)) {
          // We have been promised values from a majority of all processes, which means we are redy to decide so we broadcast our accepted value.

          // Check for the largest recieved value so far
          var (maxBallot, value): ((Int, Int), Option[Any]) = ((0, 0), None);
          for ((p, v) <- promises) {
            if (p > (ts, rank)) {
              value = v;
            }
          }
          value match {
            case Some(v) => proposedValue = Some(v);
            case None    => {};
          }
          trigger(BEB_Broadcast(Accept((ts, rank), proposedValue)) -> beb);
        }
      }
    };

    case PL_Deliver(src, accAck: Accepted) => {
      if ((ts, rank) == accAck.acceptedBallot) {
        /*
               INSERT YOUR CODE HERE
         */
        numOfAccepts += 1;
        if (numOfAccepts == (1 + numProcesses / 2)) {
          proposedValue match {
            case Some(v) => {
              trigger(BEB_Broadcast(Decided(v)) -> beb)
            };
            case None => {
              println("Somehow decided on a None value???");
            }
          }
        }
      }
    };

    case PL_Deliver(src, nack: Nack) => {
      if ((ts, rank) == nack.ballot) {
        /*
                      INSERT YOUR CODE HERE
         */
        propose()
      }
    }
  }

};
