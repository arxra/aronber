/**/
removelast([s], []) :- true.

removelast([X,Y|T], [X|S]) :-
    removelast(T, S).
