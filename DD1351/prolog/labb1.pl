/**/
subset([], []).
subset([H|T], [H|R]) :- subset(T, R).
subset([_|T], R) :- subset(T, R).

/* Fråga 2 
Grunkan här är att Prolog alltid antar falskt när den tappar bort sig, eller inte ser vad som är svaret.

*/
mat(pasta).
mat(spaghetii).
mat(brokolli).
mat(stick):-!,fail.

isFood(X):-mat(X).

% ett annat kortare exempel. Aron gillar spel, men bara spelet DOTA.
gillar(aron,K):- isSpel(K).
spel(dota).
isSpel(K):-spel(K).
%livsstil(dota).
%
%
%Negation as failure:

stick(brown).
stick(sticky).
brick(bricky).
brick(orange).

things(X,Y,Z):-stick(X), brick(Y), mat(Z).

isFood(X,Y,Z):- \+ stick(X), \+ brick(Y), mat(Z); true.


%Find Last aka del 3 av labben
findlast([H],[],H):-!.
findlast([X|Y],[X|Ys],Z):-findlast(Y, Ys, Z).


%partstring, del 4 av labb. För när ens kod inte är tillräckligt random av sig själv liksom
notnull([]):-!,false.
notnull(_).
%notnull(Q,A):- A=Q.
partstring([],_).
partstring(X,Y):-append(Xs,_,X),append(_,Y,Xs), notnull(Y).


%permute/2
permute([],[]).
permute([Z|Zs],Xs) :- permute(Zs,Ys), select(Z,Xs,Ys).
