/**/
appendnew([],L,L).
appendnew([H|T],L,[H|R]):- append(T,L,R).

selectnew(X, [X|T],T).
selectnew(X, [Y|T],[Y|R]):- select(X,T,R).

removelast([_],[]).
removelast([F,H|T], [F|K]):- removelast([H|T], K).
