verify(Input) :-
  see(Input), read(T), read(L), read(S), read(F), seen,
  check(T, L, S, [], F).

%and
check(T, L, S, U, and(A, B)) :-
  check(T, L, S, U, A),
  check(T, L, S, U, B).

% or 1 and 2
check(T, L, S, U, or(A, B)) :-
  check(T, L, S, U, A);
  check(T, L, S, U, B).

% ax
check(T, L, S, _, ax(F)) :-
  member([S, H], T),
  verifyAll(T, L, [], F, H).

% ag1
check(_, _, S, U, ag(_)) :-
  member(S, U).
% ag2
check(T, L, S, U, ag(F)) :-
  \+ member(S, U),
  member([S, H], T),
  check(T, L, S, [], F),
  verifyAll(T, L, [S | U], ag(F), H).

%af1
check(T, L, S, U, af(F)) :-
  \+ member(S, U),
  check(T, L, S, [], F).
%af2
check(T, L, S, U, af(F)) :-
  \+ member(S, U),
  member([S, H], T),
  verifyAll(T, L, [S | U], af(F), H).

%ex
check(T, L, S, _, ex(F)) :-
  member([S, H], T),
  verifyExist(T, L, [], F, H).

%eg
check(_, _, S, U, eg(_)) :-
  member(S, U).
check(T, L, S, U, eg(F)) :-
  \+ member(S, U),
  member([S, H], T),
  check(T, L, S, [], F),
  verifyExist(T, L, [S | U], eg(F), H).

%ef
check(T, L, S, U, ef(F)) :-
  \+ member(S, U),
  check(T, L, S, [], F).
check(T, L, S, U, ef(F)) :-
  \+ member(S, U),
  member([S, H], T),
  verifyExist(T, L, [S | U], ef(F), H).

%¬p
check(_, L, S, _, neg(P)) :-
  member([S, Atoms], L),
  \+ member(P, Atoms).
%p
check(_, L, S, _, P) :-
  member([S, Atoms], L),
  member(P, Atoms).

%test that F true for all in H
verifyAll(_,_,_,_,[]).
verifyAll(T, L, U, F, [S | H]) :-
  check(T, L, S, U, F),
  verifyAll(T, L, U, F, H).

%test that F true for any in H
verifyExist(T, L, U, F, [S | H]) :-
  check(T, L, S, U, F);
  verifyExist(T, L, U, F, H).

% %helper functions
% ax(_,_,_,_,[]).
% ax(T, L, U, F, [S | H]) :-
%   check(T, L, S, [], F),
%   ax(T, L, U, F, H).

% ag(_,_,_,_,[]).
% ag(T, L, U, F, [S | H]) :-
%   check(T, L, S, U, ag(F)),
%   ag(T, L, U, F, H).

% af(_,_,_,_,[]).
% af(T, L, U, F, [S | H]) :-
%   check(T, L, S, U, af(F)),
%   af(T, L, U, F, H).

% ex(T, L, U, F, [S | H]) :-
%   check(T, L, S, [], F);
%   ex(T, L, U, F, H).

% eg(T, L, U, F, [S | H]) :-
%   check(T, L, S, U, eg(F));
%   eg(T, L, U, F, H).

% ef(T, L, U, F, [S | H]) :-
%   check(T, L, S, U, ef(F));
%   ef(T, L, U, F, H).
