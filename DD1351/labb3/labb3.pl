% For SICStus, uncomment line below: (needed for member/2)
%:- use_module(library(lists)).
% Load model, initial state and formula from file.
verify(Input) :-
    see(Input), read(T), read(L), read(S), read(F), seen,
    check(T, L, S, [], F).


% check(T, L, S, U, F)
% T - The transitions in form of adjacency lists
% L - The labeling
% S - Current state
% U - Currently recorded states
% F - CTL Formula to check.
%
% Should evaluate to true iff the sequent below is valid.
%
% (T,L), S |- F
%           U


% To execute: consult(’your_file.pl’). verify(’input.txt’).


% Literals
%check(_, L, S, [], X) :- ...
%check(_, L, S, [], neg(X)) :- ...


% And
%check(T, L, S, [], and(F,G)) :- ...
check(T,L,S,[], and(X,Y)):-
    check(T,L,S,[], X),
    check(T,L,S,[], Y).

%OR
check(T,L,S,[], or(X,Y)):-
    check(T,L,S,[], X);
    check(T,L,S,[], Y).


% AX
check(T,L,S,_,ax(F)):-
    member([S, X],T), 
    checkAll(T,L,[],F,X).
% EX
check(T,L,S,_,ex(F)):-
    member([S,X],T),
    checkExist(T,L,[],F,X).

% AG
check(_,_,S,U,ag(_)):-
    member(S,U).
check(T,L,S,U,ag(F)):-
    \+ member(S,U),
    member([S,X],T),
    check(T,L,S,[],F),
    checkAll(T,L,[S|U],ag(F), X).

% EG
% Form holds in one branch
check(_,_,Cur,Rec,eg(_)):-
    member(Cur,Rec).
check(Tran,Lab,Cur,Rec,eg(Form)):-
    \+ member(Cur,Rec),  % Not end
    member([Cur,Paths],Tran),  % Get Paths
    check(Tran,Lab,Cur,[],Form),  % Check that Form holds true
    checkExist(Tran,Lab,[Cur|Rec],eg(Form), Paths).  % Check eg(F) holds for any path

% AF
check(T,L,S,U,af(F)):-
    \+ member(S,U), % Not end
    check(T,L,S,[],F). % If F holds true, end
check(T,L,S,U,af(F)):-
    \+ member(S, U),  % Not end
    member([S, H], T),  % Get paths H
    checkAll(T, L, [S | U], af(F), H).  % Check ag(F) holds for all paths

% EF
check(T, L, S, U, ef(F)) :-
    \+ member(S, U),  % Not end
    check(T, L, S, [], F).  % if F holds true, end
check(T, L, S, U, ef(F)) :-
  \+ member(S, U),  % Not end
  member([S, H], T),  % Get atoms
  checkExist(T, L, [S | U], ef(F), H).  % Verify

% ¬P
check(_, L, S, _, neg(P)) :-
  member([S, Atoms], L),
  \+ member(P, Atoms).
% P
check(_, L, S, _, P) :-
  member([S, Atoms], L),
  member(P, Atoms).

% Par1: Transition map
% Par2: Labels
% Par3: Recorded true
% Par4: Formula to prove
% Par5: Path list
checkExist(T,L,U,F,[S|N]):-
    check(T,L,S,U,F);  % Check if true or continue
    checkExist(T,L,U,F,N).

checkAll(_,_,_,_,[]).
checkAll(Tran,Lab,Rec,Form,[Cur|Next]):-
    check(Tran,Lab,Cur,Rec,Form),  % Check if true then continue
    checkAll(Tran,Lab,Rec,Form,Next).
