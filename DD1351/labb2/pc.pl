verify(InputFileName):- see(InputFileName),
    read(Prems), read(Goal), read(Proof),
    seen,
    valid_proof_check(Prems, Goal, [],Proof).

valid_proof_check(Prems, Goal, [], Proof):-last(Proof, [_, Goal,_]), valid_proof(Prems, Goal, [], Proof).

memeber(X, [Y|T]) :- X = Y; memeber(X, T).

valid_proof(_, _, _, []).
valid_proof(Prems,Goal, Proofed,[Op|Tail]):-
    valid(Prems,Op,Proofed),
    valid_proof(Prems,Goal, [Op|Proofed], Tail).




% We want a list of proved stuff. Lets call it P. P for Proven. The Proven list. The list of Proof.
% a Prooflist.
% Prooflist entries []

% Premise
valid(Prem,[_,X, premise],_):- memeber(X,Prem).

%ASSumption == BOXYTIEMS
valid(Prems, [[Row,New , assumption] |FurthurBox],P):-
    valid_proof(Prems, New, [[Row,New,assumption] | P], FurthurBox).

% När boxen om har tagit slut, och dess bounds är läääämnat baaaaaaaaaak.
% Dåååå bounds'ar vi, då bounds'ar vi, båd utanför ooo iiiii ("IN!").
box_bounds([[Front|Box] | _], Front,L):- 
    last([Front|Box], L).
box_bounds([_|P], Front, Last):- 
    box_bounds(P,Front,Last).


% ImplicElim
valid(_, [_,To,impel(X,Y)], P):- 
    memeber([X,From,_],P),
    memeber([Y,imp(From,To),_],P).

% Impint, time to introduce the friend of introduction.
valid(_, [_,imp(From,To) ,impint(X,Y)],P):-
    box_bounds(P, [X, From, _], [Y, To, _]).

%  And elemination
valid(_, [_,K, andel1(X)],P):- memeber([X,and(K,_),_],P).
valid(_, [_,K, andel2(X)],P):- memeber([X,and(_,K),_],P).

% And then and INtroduction
valid(_, [_, and(X,Y), andint(Xr, Yr)], P):- 
    memeber([Xr, X, _], P),
    memeber([Yr, Y, _], P).

% ...Or elemination?
valid(_, [_,Reason,orel(Ororigin,Front1,Last1,Front2,Last2)], P):- 
    box_bounds(P, [Front1,Or1,_], [Last1,Reason,_]),
    box_bounds(P, [Front2,Or2,_], [Last2,Reason,_]),
    memeber([Ororigin,or(Or1, Or2),_],P).

valid(_,[_,or(X,_),orint1(Xr)],P) :- memeber([Xr,X,_],P).
valid(_,[_,or(_,X),orint2(Xr)],P) :- memeber([Xr,X,_],P).

%   Negation Negation? Negation removal? Negation Elemination!
%   Check if Q on row X is neg(Q) on row Y. Then, they have reverse negations, same inner. Contradicts.
valid(_, [_, cont, negel(X,Y)],P):- 
    memeber([X,Q,_],P), memeber([Y,neg(Q),_],P).

% Negation negation negation negation negaio....
valid(_,[_,X, negnegel(Y)], P):- memeber([Y, neg(neg(X)),_],P).
valid(_,[_,neg(neg(X)), negnegint(K)],P):- memeber([K, X, _], P).

% Contradiction elemination. The most weird thing since Datasektionen sang Vikingen as punchvisa....
valid(_, [_, _, contel(Cont)], P):- memeber([Cont, cont, _], P).

%   negation introduction
valid(_,[_, neg(X), negint(A,B)],P):- box_bounds(P,[A, X, _],[B, cont,_]).

% Contradiciton of Proof? No, reverse words blyat!
valid(_, [_,Elem, pbc(First,Last)],P):-
    box_bounds(P, [First, neg(Elem), _], [Last, cont,_]).

%   COPY (Butt no paste :(   )
valid(_,[_, Entry, copy(K)], P):- memeber([K,Entry,_],P).

% LEM should also join the party says validtest#15
valid(_,[_, or(X,neg(X)), lem],_).
valid(_,[_, or(neg(X),X), lem],_).

% Modus Tolens. Den gör saker.
valid(_,[_, neg(From), mt(X,Y)],P):- 
    memeber([Y, neg(To), _],P),
    memeber([X, imp(From,To), _],P).

copy(x).
andint(x,y).
andel1(x).
andel2(x).
orint1(x).
orel(x,y,u,v,w).
impint(x,y).
impel(x,y).
negel(x,y).
negnegint(x).
negnegel(x).
mt(x).
pbc(x,y).
