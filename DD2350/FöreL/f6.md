# Föreläsning 6

Korrekthetsbevis

- Testing
- Pre- o Postvillkår
- Invarianter
    - Loopinvarianter

## Grader av korrekthet

### Partiell korrekthet

En del av programmet fungerar garanterat, dvs den levererar alltid korrekt POST för varje indata PRE.

### Total korekthet

Programmet levererar alltid korrekt POST givet PRE.

## loopinvarianter

1. Initiering

    Loopinvarianten måste vara sann i första iterationen

2. Iterationenen

    Om invarianten är sann före iterationen, ska den också vara sann efter loopen.

3. Terminaiton

    När loopen terminerar, vilket den måste, ska invarianten säga något användbart som hjälper oss förstå algoritmen.

Detta följer av vanliga INduktionsbevis.
