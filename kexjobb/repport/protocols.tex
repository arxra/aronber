

\section*{Application Solutions} \label{sec:applictrans}
Application layer solutions for transferring media will be looked at from layer 7 of Table~\ref{fig:osimodel}.
These are all running over some combination of the previously mentioned transport protocols (TCP and UDP).
First, some well-known file transfer protocols mainly sending packets over TCP are presented. Then, more exotic 
protocols running over other protocols such as UDP or combinations of UDP and TCP are presented and lastly are 
proprietary protocols developed by dedicated companies with industry support.


\subsection{HTTPS}
Being the currently used protocol for media transfer by the ISGs as described in~\ref{sec:isgtoday} and as the 
backup for traversing firewalls other transfer protocols have a lot to prove over this one. HTTPS file transfers will
as such be the baseline of file transfers. It encrypted with TLS and then runs over TCP. 
A lot of effort has gone into optimizing the implementations of TCP for the different operating
systems which makes this protocol fairly attractive for its wide availability.

\subsection{FTPS}
FTPS, or FTP over SSL/TLS(\ref{sec:encryption})
is a secure version of the popular FTP which has been around longer than the Internet.
While old and reliable, it has a major downside: It uses multiple ports per file transfer~\cite{SFTPVsFTPS2016}.\\
This is not expected to be available in user systems, 
in the real world deployments of ISGs file transfers can at most 
use a handful of ports, but each transfer requires a port they would risk quickly running out or opening the network
to more ports than desired.

\subsection{SFTP}
SFTP is based of the SecureSHell(SSH) protocol, with SFTP expanding to SSH File Transfer Protocol.
While it has a similar name to FTPS, these two protocols should not be confused as they are completely different.
It is also important to note that there is also another protocol out called SFTP, the Simple File Transfer Protocol.
The simple version is not looked at in this part, or at all in this investigation.
First off, FTPS use TLS/SSL to remain secure with SFTP using private-public key pairs,
the same method as ssh~\cite{SFTPVsFTPS2016}.
While it may be a problem for some types of implementations to manage the keys necessary for this type of transfer,
this can be done securely against Iconik in the cloud over HTTPS.

Secondly, SFTP runs on the same conditions as SSH, which requires a single port to be available.
It also runs over TCP(as with SSH),
as such it should show a lot of similarities to the characteristics described in~\ref{sec:TCP}.
SFTP also allows a user to manage the file system as well, being able to list remote files and directories and remove 
them. 

\subsection{SCP}
Similar to SFTP this protocol also runs over SSH and sees many of the same features of SFTP, albeit with less access
to the file system itself. SCP focuses on just the file transfer. As the files will be indexed in the cloud, this is 
not a problem and might be preferable. Just as with SFTP it comes with the openssl package which is readily available.

\subsection{WDT}\label{proto:wdt}
The Warp speed Data Transfer protocol aims to use multiple TCP connections until the only possible bottleneck is 
hardware based on either disc or network link speed. Originally developed by Facebook in order to move their 
databases around in data centers directly from system memory,
it is now Open Source and available on GitHub under the BSD license
~\cite{FacebookWdt2019}.
It there comes as both a portable library and as a command line tool for testing and usage purposes directly.
It is also available as a system packet in some package managers, for example the arch Linux family of distributions~
\cite{AURWdt}.

Since WDT´s user base is small there is no more available information on how it works or performs.


\subsection{QUIC}
QUIC\footnote{Quick UDP Internet Connections}
is sometimes referred to as running TCP + TLS over UDP as it shares their characteristics.
It is in development primarily by Google and Mozilla and is under standardization by the 
IETF\footnote{Internet Engineering Task Force}\cite{Draftietfquictransport23QUICUDPBased}.
It is planned to be the transport protocol for HTTP/3 after it has been standardized.
It uses the same congestion control mechanisms that TCP uses,
which ensures that is treats network traffic fairly even though it is run over UDP.
Other benefits include that TLS Encryption is obligatory in the protocol which ensures that the packets are trusted 
and safe in Cyberspace.
While both TCP and UDP can only be used as underling protocols (both are missing native encryption),
QUIC might be able to be the single part in transferring media.
It is reliable, secure, authenticated, have low response times,
respect current traffic and does not suffer from the typical problems of TCP to the same degree, 
such as low performance when there is a high bandwidth/delay product \cite{leongTCPCongestionControl2017}. 
\citeauthor{leongTCPCongestionControl2017} also discussed a even higher performing congestion control algorithm in the 
same paper as the performance evaluation of high bandwidth/delay products, which both TCP and QUIC might use later.
\newline

Currently, QUIC only sees use between a selected few services,
such as Google services and in a synchronization application Syncthing.
These use QUIC with the primary aim to reduce latency between internet applications,
such as buffer times of newly opened YouTube videos, responsiveness of Google 
documents and securely transfer files~\cite{QUICQuickUDP}.

\subsection{PA-UDP}
Performance Adaptive-UDP, or just PA-UDP is a application level protocol implementation aiming towards making UDP
safe even at the highest of speeds. It uses a combination of variables to make sure to not overfill any system buffers:
\begin{enumerate}
  \item{Disk read and write}
    Both the maximum read speed of the sender and write speeds of the receiver are used as not to overload any of the
    operating system kernels with overfull UDP buffers. This speed limit is set at the beginning of the connection,
    and updated throughout the transfer with TCP control packets.
    If, for some reason, the buffers of the receiver would start to fill up a request for fewer packets is sent back
    to the host telling it to reduce speed.
  \item{Link speed}
    The available link speed is also accounted for by trying to keep track of how many packets are in the network at 
    any given time, packet loss and delays. This is due to how easy UDP can overfill the buffers of a network, causing
    heavy packet loss.
\end{enumerate}
PA-UDP is clearly detailed in a excellent paper by \citeauthor{eckartPerformanceAdaptiveUDP}
called \citetitle{eckartPerformanceAdaptiveUDP}\cite{eckartPerformanceAdaptiveUDP}.
However, there is no future work on the protocol, and it lacks official implementations. As such, the protocol is still
in its research phase, 10 years after its first appearance on the web. 

\subsection{UDT}
UDT runs completely over UDP, implementing most of what TCP such as congestion control and packet delivery guarantees.
In that sense, it is fairly similar to QUIC. However, the protocol is slightly older and focuses on just massive data
transfers. It also runs in userspace instead of kernel space(\ref{sec:kernspace}), such as TCP and the underlaying UDP.
It also runs over a single port and should as such have no problems traversing firewalls~
\cite{guUDTUDPbasedData2007}.
The protocol is under development by its original author since 2007, Yunhong Gu, who still assists in the open source
effort of the protocol. As it is listed as used in multiple high performance applications and winner of multiple 
high speed network transfer contests between 2007 and 2013, in both presentations and documents on the official git 
repository on dorkbox~\cite{dorkboxUDT} and its other official channels~\cite{UDTBreakingData}.

\subsection{UFTP}\label{proto:uftp}
"UFTP is an encrypted multicast file transfer program, designed to securely, reliably,
and efficiently transfer files to multiple receivers simultaneously."\cite{UftpBufferbloatNet}.\\
UFTP is based on MFTP, but runs over UDP instead of TCP, giving it clear advantages in high delay links, such as 
over satellite connections, which is its main targeted transfer link.
As it is more focused on reliability than anything else, it is unclear how it will compare
to the others in a speed sensitive investigation.
It supports very strong encryption for all the packets with variable encryption methods.
Since it is a multicast protocol, one could also send the same file to multiple points at the same time, only 
encrypting the file once on the sending side. This adds a slight amount of value, as it is not a common use case for
media management to keep the same file on multiple locations.

\subsection{FileCatalyst Direct}
FC Direct is a proprietary solution using multiple UDP ports on both sending and receiving sides, with a 
single open port of TCP. The solution shows very promising results in previous tests, and has some of the most 
consistent results in all tests, even those with relatively poor conditions. It comes with high levels of encryption, 
high link utilization and is has clients for all required operating systems.

FC Direct also suffers from some problems
related to both porting and implementation details. First, it is written in java which might be one of the reasons it 
takes longer to start and finish than other commercial protocols.
Secondly, it also uses a wide range of ports for 
achieving its speeds~\cite{kachanComparisonContemporarySolutions}.

\subsection{TIXstream}\label{proto:tixstream}
TIXstream is very similar to FileCatalyst from a marketing standpoint in their corresponding feature set.
However, there are some key differential factors.
TIXstream uses a single UDP port for data transfer over its optional RWTP UDP based proprietary protocol,
and as such does not impose a problem regarding firewall traversal as other solutions.
It is also built using faster underlaying architecture, 
having about half the start and stop times of FC Direct.
However, it does not handle packet loss and increased RTT's 
equally well. As packet loss goes towards 1\% and the RTT\footnote{Round trip time}
increases over 150 the performance starts to degrade
~\cite{kachanComparisonContemporarySolutions}.

It also cannot multistream the UDP based RWTP protocol, which is locked at one stream.
For multistream TIXStream handles TCP connections over XP, another proprietary protocol with a set of TCP ports
~\cite[Section~5.2.5]{TIXstreamMFTInstallation}.
XP is the default transfer method, to better allow administrators to control the flow of traffic and multistream the 
transfer.
Furthermore, it also just encrypts unencrypted media if the encryption is strong enough as is, resulting in less 
resources being wasted in the case of transferring encrypted media.

\subsection{FASP}\label{sec:fasp}
The Fast And Secure Protocol is similarly based on UDP with a row of different mechanisms in place to try to maximize 
its bandwidth. The most interesting mechanism of FASP is how it communicates with other clients in order to try and 
calculate how they can send data without interfering with each other~\cite{xuMethodSystemAggregate2017}.

FASPs shortcomings is in the expense of calculating all this extra data for each datagram, where studies have found
that the protocol in general does not exceed 1.8Gbit/s without jumbo frames, which can not be assumed available~
\cite{hagernas5GUserSatisfaction}.
