use super::config::{Conf, Proto};
use std::net::TcpListener;
use std::time::Duration;

mod tcp;
use tcp::handle_tcp_connection_unsec;

struct measurement {
    bytes: usize,
    time: Duration,
}

/// Opens a host socket based on a conf, without any setup encryption.
/// Used for testing if the network is stable and its speed of the link, so that the encrypted
/// version has a refrence point of lost speed

pub fn host_socket_unsecure(conf: Conf) {
    match conf.protocol {
        Proto::TCP => {
            let listener = TcpListener::bind(&conf.sockaddr).unwrap();
            for stream in listener.incoming() {
                let strem = stream.unwrap();
                let thread = std::thread::spawn(move || {
                    handle_tcp_connection_unsec(strem, conf.durration.clone())
                });
            }
        }
        Proto::UDP => {}
        Proto::SCTP => {}
        Proto::QUIC => {}
    }
}

pub fn host_socket(conf: Conf) {
    unimplemented!();
    match conf.protocol {
        Proto::TCP => {}
        Proto::UDP => {}
        Proto::SCTP => {}
        Proto::QUIC => {}
    }
}
