use crate::config::{Duration, DurationType};
use crate::out::last_results;
use std::io::prelude::*;
use std::net::TcpStream;

//Internal imports
/// Set a start timer, keep printing the data recieved.
pub fn handle_tcp_connection_unsec(mut tcpcon: TcpStream, dur: Duration) {
    let mut recieved_bytes: usize = 0;
    let mut buf = [0; 2000];

    while match tcpcon.read(&mut buf) {
        // Headers are excluded as this program aims to measure the speed of the filetransfer itself,
        // not the speed of the link. The data itself is irrelevant here, as it's unencrypted speed
        // testing.
        Ok(size) => {
            recieved_bytes += size;
            last_results(recieved_bytes, &dur);
            true
        }
        Err(_) => {
            eprintln!("Unknown revievved bytes. Terminating server");
            false
        }
    } {}
}
