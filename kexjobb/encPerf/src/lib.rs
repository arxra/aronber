//! encPerf aims to allow the benchmarking of the availalbe transfer speed between 2 nodes.
//! The point of this is to more easily see which algorithm to use for transfering your specific
//! data.
//!
//!
//! # Structure
//!
//! ## Cons
//! The cons module contain information on how the different protocols handle connections and
//! measures speed
//!
//! ## config
//! Too keep track of all the available variables there's a config struct with all the values
//! passed to the program as available parsed program input.
//!
//! ## out
//! Output parsing, to make sure it's uniform independant of the protocol in action.
//!

extern crate clap;

// Mod our crates
pub mod config;
pub mod cons;
pub mod out;

use crate::config::{Conf, Mode};
use crate::cons::{host_socket, host_socket_unsecure};
use crate::out::init_txt;

/// Runs test based on Conf
pub fn run(conf: Conf) {
    init_txt(&conf);

    match (&conf.mode, &conf.sec.enabled) {
        (Mode::SERVER, false) => {
            host_socket_unsecure(conf);
        }
        (Mode::SERVER, true) => loop {
            host_socket(conf)
        },
        (Mode::CLIENT, true) => unimplemented!(),
        (Mode::CLIENT, false) => unimplemented!(),
    }
}
