use crate::config::{Conf, Duration, DurationType};

use std::time;

pub fn init_txt(conf: &Conf) {
    match conf.durration.durrtype {
        DurationType::SECS => println!(
            "# Running test as {:?} using {:?}, for {} seconds",
            conf.mode, conf.protocol, conf.durration.durr
        ),
        DurationType::BYTES => println!(
            "# Running test as {:?} using {:?}, for {} bytes",
            conf.mode, conf.protocol, conf.durration.durr
        ),
        DurationType::FILE => println!(
            "# Running test as {:?} using {:?}, for {:?}",
            conf.mode, conf.protocol, conf.durration.durrtype
        ),
    }
}

/// Takes the data from the last second and regarding network speed and prints it to stdout
pub fn last_results(current: usize, total: &Duration) {}
