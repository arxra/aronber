use encPerf::config::Conf;

use clap::{App, Arg};
use std::net::IpAddr;

fn main() {
    let args = App::new("encspdtst")
        .version("Alpha")
        .about("Network speedtesting using multiple protocols and encryption methods")
        .author("Aron H.B.")
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Set level of verbosity"),
        )
        .arg(
            Arg::with_name("adress")
                .short("a")
                .long("adress")
                .validator(valid_ip)
                .help("Network address to connect to. Use -4 if this is a IP version 4 address.")
                .required_unless("server")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("server")
                .short("s")
                .multiple(false)
                .conflicts_with_all(&["bytes", "duration"])
                .help("Set server mode operation, opening ports instead of connecting remote"),
        )
        .arg(
            Arg::with_name("ipv4")
                .short("4")
                .long("ipv4")
                .help("Compatability flag for use in IPv4 mode.")
                .multiple(false),
        )
        .arg(
            Arg::with_name("protocol")
                .short("p")
                .long("protocol")
                .takes_value(true)
                .default_value("tcp")
                .possible_values(&["udp", "tcp", "quic", "sctp"]),
        )
        .arg(
            Arg::with_name("debug")
                .short("d")
                .long("debug")
                .multiple(false)
                .help("Prints verbose debuging information on set parameters and return values"),
        )
        .arg(
            Arg::with_name("bytes")
                .long("byte")
                .short("b")
                .takes_value(true)
                .conflicts_with("duration")
                .help("Runs the test for #bytes instead of X amounts of seconds"),
        )
        .arg(
            Arg::with_name("duration")
                .short("t")
                .takes_value(true)
                .help("The duration in seconds for the test."),
        )
        .arg(
            Arg::with_name("noencrypt")
                .short("u")
                .long("unsecure")
                .help("Disables encryption over communication channels"),
        )
        .arg(
            Arg::with_name("port")
                .short("P")
                .long("port")
                .takes_value(true)
                .help("Sets a non-default port. Useful for muliple concurrent tests.")
                .default_value("7878")
                .validator(|v| match v.parse::<u16>() {
                    Ok(_) => Ok(()),
                    Err(_) => Err("Not a valid port (must be u16)".to_string()),
                }),
        )
        .get_matches();

    if let Some(cnf) = Conf::new(args) {
        encPerf::run(cnf);
    } else {
        panic!("Not able to create configuration class");
    };
}

fn valid_ip(v: String) -> Result<(), String> {
    match v.parse::<IpAddr>() {
        Ok(_) => Ok(()),
        Err(_) => Err("Failed to parse IP, did you supply a valid IP address?".to_string()),
    }
}
