use clap::ArgMatches;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

/// Configuration
///
/// server if the instance is accepting connections, or is a client
/// adress where to connect, or localhost if it's a server
/// port where to connect, or where to allow connections.
/// parallell the number of parallell transfers.
///
pub struct Conf {
    pub mode: Mode,
    pub adress: std::net::IpAddr,
    pub sockaddr: std::net::SocketAddr,
    pub port: u16,
    pub parallell: i32,
    pub duration: i32,
    pub protocol: Proto,
    pub durration: Duration,
    pub sec: Secc,
}

/// Handles the durration of the test run.
#[derive(Clone)]
pub struct Duration {
    pub durr: usize,
    pub durrtype: DurationType,
}

/// Set the protocol version to run the tests.
#[derive(Debug)]
pub enum Proto {
    TCP,
    UDP,
    QUIC,
    SCTP,
}

#[derive(Debug)]
pub enum Mode {
    SERVER,
    CLIENT,
}

/// What type of transfer are we doing? Is it timed, sized or example files?
#[derive(Debug, Clone)]
pub enum DurationType {
    SECS,
    BYTES,
    FILE,
}

/// Security parameters. Running tests without these is *NOT* recommended.
#[derive(Default)]
pub struct Secc {
    pub enabled: bool,
    pub privkey: String,
    pub pubkey: String,
}

impl Default for Conf {
    fn default() -> Self {
        let adr = IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1));
        let defport = 25001;

        Conf {
            mode: Mode::CLIENT,
            adress: adr,
            port: defport,
            parallell: 1,
            sockaddr: SocketAddr::new(adr, defport),
            duration: 10,
            protocol: Proto::TCP,
            durration: Duration {
                durr: 10,
                durrtype: DurationType::SECS,
            },
            sec: Default::default(),
        }
    }
}

impl Conf {
    pub fn new(args: ArgMatches) -> Option<Conf> {
        let mut cnf: Self = Default::default();

        if let Ok(p) = args
            .value_of("adress")
            .unwrap_or("127.0.0.1")
            .parse::<IpAddr>()
        {
            cnf.adress = p
        }

        cnf.port = args.value_of("port").unwrap().parse::<u16>().unwrap();

        // Check for server operations
        if args.is_present("server") {
            cnf.mode = Mode::SERVER;
            cnf.sockaddr.set_port(cnf.port);
            if args.is_present("debug") {
                println!("Server mode set");
            }
            if args.is_present("adress") {
                match args.value_of("adress")?.parse::<IpAddr>() {
                    Ok(i) => cnf.sockaddr.set_ip(i),
                    Err(e) => eprintln!("Failed to set ip: {}", e),
                };
                if args.is_present("debug") {
                    println!("Set ip to: {}", cnf.sockaddr.ip());
                }
            }
        }
        if args.is_present("noencrypt") {
            cnf.sec.enabled = false;
        }

        if args.is_present("bytes") {
            cnf.durration.durrtype = DurationType::BYTES;
            if let Ok(p) = args
                .value_of("durration")
                .unwrap_or("10000000000")
                .parse::<usize>()
            {
                cnf.durration.durr = p;
            }
        }

        match args.value_of("protocol") {
            Some("udp") => cnf.protocol = Proto::UDP,
            Some("quic") => cnf.protocol = Proto::QUIC,
            Some("sctp") => cnf.protocol = Proto::SCTP,
            _ => cnf.protocol = Proto::TCP,
        }

        Some(cnf)
    }
}
