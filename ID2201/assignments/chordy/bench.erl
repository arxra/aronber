-module(bench).

-export([small/0, node/2]).

small()->
	Q = test:start(node2),
	test:start(node2,3, Q),
	timer:sleep(1000),
	Keys = test:keys(4000),
	test:add(Keys, Q),
	test:check(Keys, Q),
	ok.
node(Nodes, Nkeys)->
	Q = test:start(node2),
	test:start(node2, Nodes, Q),
	timer:sleep(Nodes * 10 + 100),
	io:format("Done waiting for node creation~n"),
	Keys = test:keys(Nkeys),
	test:add(Keys, Q),
	test:check(Keys, Q),
	ok.

