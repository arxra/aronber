-module(node2).
-export([start/1, start/2]).
-define(Stabilize, 100).
-define(Timeout, 10000).

% ------------------------------------------------------
% Starting nodes should be a thing
%
start(Id) -> 
	start(Id, nil).

start(Id, Peer) ->
	timer:start(),
	spawn(fun() -> init(Id, Peer) end).

init(Id, Peer) ->
	Predecessor = nil,
	{ok, Successor} = connect(Id, Peer),
	schedule_stabilize(),
	%io:format("creating (~w): Pre: ~w, Succ:~w~n", [Id, Predecessor, Successor]),
	Store = storage:create(),
	node(Id, Predecessor, Successor, Store).

% ------------------------------------------------------
% Initial connection to successor
%
connect(Id, nil) ->
	{ok, {Id, self()}};

connect(_, Peer) ->
	Qref = make_ref(),
	Peer ! {key, Qref, self()},
	receive
		{Qref, Skey} ->
			{ok, {Skey, Peer}}
	after ?Timeout -> 
		io:format("Timeout: no response~n")
	end.

% ------------------------------------------------------
% The node is the core of our chord ring.
%
node(Id, Predecessor, Successor, Store) ->
	%io:format("~w: ~w, ~w~n", [Id, Predecessor, Successor]),
	receive
		{key, Qref, Peer} ->
			%io:format("~w: Got a key! ref: ~w~n", [Id, Qref]),
			Peer ! {Qref, Id},
			node(Id, Predecessor, Successor, Store);
		{notify, New} ->
			{Pred, Store2} = notify(New, Id, Predecessor, Store),
			node(Id, Pred, Successor, Store2);
		{request, Peer} ->
			request(Peer, Predecessor),
			node(Id, Predecessor, Successor, Store);
		{status, Pred} ->
			Succ = stabilize(Pred, Id, Successor),
			node(Id, Predecessor, Succ, Store);
		stabilize ->
			%io:format("~w: ~w, ~w~n", [Id, Predecessor, Successor]),
			stabilize(Successor),
			node(Id, Predecessor, Successor, Store);
		probe ->
			create_probe(Id, Successor),
			node(Id, Predecessor, Successor, Store);
		{probe, Id, Nodes, T} ->
			remove_probe(T, Nodes),
			node(Id, Predecessor, Successor, Store);
		{probe, Ref, Nodes, T} ->
			forward_probe(Ref, T, Nodes, Id, Successor),
			node(Id, Predecessor, Successor, Store);
		{add, Key, Value, Qref, Client} ->
			%io:format("~w adding: ~w~n", [Id, [Key, Value, Qref ] ]),
			Added = add(Key, Value, Qref, Client,
			Id, Predecessor, Successor, Store),
			node(Id, Predecessor, Successor, Added);
		{lookup, Key, Qref, Client} ->
			lookup(Key, Qref, Client, Id, Predecessor, Successor, Store),
			node(Id, Predecessor, Successor, Store);
		{handover, Elements} ->
			Merged = storage:merge(Store, Elements),
			node(Id, Predecessor, Successor, Merged);

		A->
			io:format("Strange message recieved: ~w~n", [A])
	end.

% ------------------------------------------------------
% Add (possibly) new key to our store
%
add(Key, Value, Qref, Client, Id, {Pkey, _}, {_, Spid}, Store) ->
	case key:between(Key, Pkey, Id) of
		true ->
			Client ! {Qref, ok},
			storage:add(Key, Value, Store);
		false ->
			Spid! {add, Key, Value, Qref, Client},
			Store
	end.

% ------------------------------------------------------
% Lookup values.
%
lookup(Key, Qref, Client, Id, {Pkey, _}, Successor, Store) ->
	case key:between(Key, Pkey, Id) of
		true ->
			Result = storage:lookup(Key, Store),
			Client ! {Qref, Result};
		false ->
			{_, Spid} = Successor,
			Spid ! {lookup, Key, Qref, Client}
	end.

% ------------------------------------------------------
% Probe to go around the world.
%
create_probe(_, {_, nil}) ->
	io:format("No probe sent, only single node~n");
create_probe(Id, {_, Spid}) ->
	T = erlang:system_time(micro_seconds),
	io:format("Time: ~w~n", [T]),
	Spid ! {probe, Id, [Id], T}.

remove_probe(T, Nodes) ->
	R = erlang:system_time(micro_seconds) - T,
	io:format("The probe went around the world in ~w!~n~w~n", [R, Nodes]),
	ok.

forward_probe(Ref, T, Nodes, Id, {_, Spid}) ->
	Spid ! {probe, Ref, Nodes ++ [Id], T}.

% ------------------------------------------------------
% notify that your predecessor might have been updated.
% This happens when stabilize puts you in front of another
% node. 
%
notify({Nkey, Npid}, Id, Predecessor, Store) -> 
	case Predecessor of 
		nil ->
			Keep = handover(Id, Store, Nkey, Npid),
			{ {Nkey, Npid}, Keep };
		{Pkey, _} ->
			case key:between(Nkey, Pkey, Id) of
				true ->
					%io:format("~w is between ~w and ~w~n", [Nkey, Pkey, Id]),
					% True if I have a new predecessor.
					% Note: Also true if we are our own pred
					Keep = handover(Id, Store, Nkey, Npid),
					{{Nkey, Npid}, Keep};
				false ->
					%io:format("~w is not between ~w and ~w~n", [Nkey, Pkey, Id]),
					{ Predecessor, Store }
					% Nothing has changed
			end
	end.

handover(Id, Store, Nkey, Npid) ->
	{Rest, Keep} = storage:split(Id, Nkey, Store),
	Npid ! {handover, Rest},
	Keep.

% ------------------------------------------------------
% request
% Just return your Predecessor to Peer
%
request(Peer, Predecessor) -> 
	case Predecessor of 
		nil ->
			Peer ! {status, nil};
		{Pkey, Ppid} ->
			Peer ! {status, {Pkey, Ppid}}
	end.
% ------------------------------------------------------
% stabilize
% {status, Pred} contains the node of a previous node, 
% which possibly is my new successor node.
% If it should be inserted in front, then:
% Set its predecessor to me, my successor to it, and 
% my previous successors predecessor, which was me, 
% to the new node.
%
% Pred: 3
% Id: 5
% Succ: 8
% -> Succ
%
% Pred: 7
% Id: 5
% Succ: 8
% -> Pred
%
stabilize(Pred, Id, Successor) ->
	{Skey, Spid} = Successor,
	case Pred of
		nil-> 
		% In case I'm the only node, add the node as my successor
			Spid!{notify, {Id, self()}},
			Successor;
		{Id, _} ->
		% If self.id == Pred.id
			Successor;
		{Skey, _} ->
		% If the node is both my previous and next, return either
			Spid!{notify, {Id, self()}},
			Successor;
		{Xkey, Xpid} ->
			% Now we are getting somewhere.
			% Here, previous node sent a message that it is maybe
			% in front of us, or maybe behind us.
			case key:between(Xkey, Id, Skey) of
				% Xkey is between us and our successor, that is,
				% in our range of hashes.
				% Then, it should be added in front of us and informed to 
				% updated its successor and we should set it as successor.
				true ->
					%io:format("stab: ~w is between ~w and ~w~n", [Xkey, Id, Skey]),
					%Spid ! {notify, Pred},
					Xpid ! {request, self()},
					Pred;
				% Xkey is outside of our range.
				% it cannot become our successor, and should be 
				% passed on.
				false ->
					%io:format("stab: ~w is not between ~w and ~w~n will instead suggest Pred to ~w~n", [Xkey, Id, Skey, Spid]),
					%Spid ! {status, Pred},
					Spid!{notify, {Id, self()}},
					Successor
		end
	end.

% ------------------------------------------------------
% stabilizer timer
%
schedule_stabilize() ->
	timer:send_interval(?Stabilize, self(), stabilize).

% Returns the predecessor of successor as message to node()
stabilize({_, Spid}) ->
	Spid ! {request, self()}.
