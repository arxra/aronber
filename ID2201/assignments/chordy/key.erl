-module(key).
-export([generate/0, between/3]).

generate() ->
	rand:uniform(1000000000).

between(_, From, From)-> true;
between(Key, From, To) when To < From -> 
	if 
		Key > From->
			true;
		Key =< To ->
			true;
		true ->
			false
	end;
between(Key, From, To) when (From < Key) and (Key =< To) -> 
			true;
between(_,_,_) -> false.
