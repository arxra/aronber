-module(storage).

-export([create/0, add/3, lookup/2, split/3, merge/2]).


create()-> [].

% ------------------------------------------------------
% Add element to the store.
% Most important part is too keep it sorted for split and merge
% to work easily and cheaply. We cannot skimp on performance 
% here.
%
add(Key, Value, []) -> [{Key, Value}];
add(Key, Value, [{K,_}|Store]) when K > Key -> [{Key, Value}|Store];
add(Key, Value, [El|Rest]) -> [El| add(Key, Value, Rest)].

% ------------------------------------------------------
% Search for the key, return value.
% Easy enough to iterate through.
lookup(_, [])-> false;
lookup(Key, [{Key, Value}|_])-> Value;
lookup(Key, [_|Rest])-> lookup(Key, Rest).


split(From, To, Store) -> 
	Updated = lists:foldr(fun({K,V}, Acc) ->
														case key:between(K, From, To) of 
															true -> [{K,V} | Acc]; 
															false -> Acc end 
												end,
												[], Store),
	Rest = lists:subtract(Store, Updated),
	{Updated, Rest}.

% ------------------------------------------------------
% Make sure these merge in a safe manner, 
% keeping the list sorted.
%
merge([], Store)-> Store;
merge([{Key, Value}| Entries], Store)->
	Store2 = add(Key, Value, Store),
	merge(Entries, Store2).
