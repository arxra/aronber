-module(time).

-export([zero/0, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

zero() -> 0.

inc(Name, T) ->
  T +1.

merge(Ti, Tj) when Ti > Tj ->
  Ti;

merge(_, Tj) ->
  Tj.

leq(Ti, Tj) when Tj > Ti ->
  true;

leq(_,_) ->
  false.

clock(Nodes) -> 
  lists:flatmap(fun(X) -> [{X, zero()}] end, Nodes).

update(Node, Time, Clock) -> 
  lists:keyreplace(Node, 1, Clock, {Node, Time}).

safe(_, []) -> true;
safe(Time, [{_, Ti}|T]) when Ti > Time -> 
  safe(Time, T);
safe(Time, L) -> 
  %io:format("Time: ~w~nL:~w~n", [Time, L]),
  false.

