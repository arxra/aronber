-module(loggy).
-export([start/1, stop/1]).


start(Nodes) ->
  spawn_link(fun() -> init(Nodes)end).

stop(Logger) ->
  Logger ! stop.

init(Nodes) ->
  loop([], time:clock(Nodes)).

loop(Que, Clock) ->
  receive
    {log, From, Time, Msg} ->
      Clock1 = time:update(From, Time, Clock),
      Que1 = log(From, Time, Msg, Que, Clock1),
      loop(Que1, Clock1);
    stop ->
      io:format("The logger has been shut down~nPrinting rest of queue:~n"),
      printrest(lists:keysort(2, Que)),
      ok
  end.

% Append the new item to the queue, then check for each item if it is safe to log it.
log(From, Time, Msg, Que, Clock) ->
  Que1 = Que ++ [{From, Time, Msg}],
  printque( Que1, Clock).

printrest([]) -> ok;
printrest([{From, Time, Msg}|T]) ->
    io:format("log(rest): ~w ~w ~p~n", [Time, From, Msg]),
    printrest(T).


printque(Que, Clock) ->
  printque(Que, Clock, []).

printque([], _, Acc) ->
  Acc;
printque([{From, Time, Msg}|T], Clock, Acc) ->
  Cond = time:safe(Time, Clock),
  %io:format("Cond: ~w~n", [Cond]),
  if 
     Cond -> 
      io:format("log: ~w ~w ~p~n", [Time, From, Msg]),
      printque(T, Clock, Acc);
     true ->
      printque(T, Clock, [{From, Time, Msg}| Acc])
  end.
