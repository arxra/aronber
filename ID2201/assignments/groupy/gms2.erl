-module(gms2).
-define(timeout, 5000).
-define(arghh, 500).
-export([start/1, start/2]).

% -------------------------------------------------------
% Start the node, as either master or slave
%
start(Id) ->
	Rnd = random:uniform(1000),
	Self = self(),
	{ok, spawn_link(fun()-> init(Id, Self, Rnd) end)}.

start(Id, Grp) ->
	Rnd = random:uniform(1000),
	Self = self(),
	{ok, spawn_link(fun()-> init(Id, Grp, Self, Rnd) end)}.


% -------------------------------------------------------
% Set start values for the node
%
init(Id, Master, Rnd) ->
	random:seed(Rnd, Rnd, Rnd),
	leader(Id, Master, [], [Master]).

init(Id, Grp, Master, Rnd) ->
	Self = self(),
	Grp !{join, Master, Self},
	receive
		{view, [Leader|Slaves], Group} ->
			erlang:monitor(process, Leader),
			Master!{view, Group},
			slave(Id, Master, Leader, Slaves, Group)
	after ?timeout ->
		Master ! {error, "no reply from leader"}
	end.


% -------------------------------------------------------
% Elect the first available Slave as the new Leader
% Remove the first node as we know it is dead and should 
% not be elected the new leader.
%
%	One thing that we have to pay attention to is what we should do if, as a
%	slave, receive the view message from the new leader before we have noticed
%	that the old leader is dead. 
%	Should we refuse to handle view messages unless
%	we have seen the Down message from the leader 
%	or 
%	should we happily receive
%	accept the new view and then ignore trailing Down messages.

election(Id, Master, Slaves, [_|Group]) ->
	Self = self(),
	case Slaves of 
		[Self| Rest] ->
			bcast(Id, {view, Slaves, Group}, Rest),
			Master ! {view, Group},
			leader(Id, Master, Rest, Group);
		[Leader|Rest] ->
			erlang:monitor(process, Leader),
			slave(Id, Master, Leader, Rest, Group)
	end.

% -------------------------------------------------------
% Broadcast message to all slaves
%
bcast(Id, Msg, Nodes) ->
  lists:foreach(fun(Node) -> Node ! Msg, crash(Id) end, Nodes).

% -------------------------------------------------------
% Random crashes are not nice, but a necessary evil
%
crash(Id) ->
	case random:uniform(?arghh) of 
		?arghh ->
			io:format("leader ~w: crash~n", [Id]),
			exit(no_luck);
		_ ->
			ok
	end.
% -------------------------------------------------------
% The leader behaves slightly differently since it also handles 
% leader calls.
%
leader(Id, Master, Slaves, Group) ->
	receive
		{mcast, Msg} ->
			bcast(Id, {msg, Msg}, Slaves),
			Master!Msg,
			leader(Id, Master, Slaves, Group);
		{join, Wrk, Peer} ->
			Slaves2 = lists:append(Slaves, [Peer]),
			Group2 = lists:append(Group, [Wrk]),
			bcast(Id, {view, [self()|Slaves2], Group2}, Slaves2),
			Master ! {view, Group2},
			leader(Id, Master, Slaves2, Group2);
		stop ->
			ok
end.


% -------------------------------------------------------
% The leader behaves slightly differently since it also handles 
% leader calls.
% Handle the case that the leader is kill.
%
slave(Id, Master, Leader, Slaves, Group) ->
	receive
		{mcast, Msg} ->
			Leader ! {mcast, Msg},
			slave(Id, Master, Leader, Slaves, Group);
		{join, Wrk, Peer} ->
			Leader ! {join, Wrk, Peer},
			slave(Id, Master, Leader, Slaves, Group);
		{msg, Msg} ->
			Master ! Msg,
			slave(Id, Master, Leader, Slaves, Group);
		{'DOWN', _Ref, process, Leader, _Reason} ->
			election(Id, Master, Slaves, Group);
		{view, [Leader|Slaves2], Group2} ->
			Master ! {view, Group2},
			slave(Id, Master, Leader, Slaves2, Group2);
		stop ->
			ok
end.

