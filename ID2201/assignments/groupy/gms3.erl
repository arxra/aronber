-module(gms3).
-define(timeout, 10000).
-define(arghh, 500).
-export([start/1, start/2]).

% -------------------------------------------------------
% Start the node, as either master or slave
%
start(Id) ->
	Rnd = random:uniform(1000),
	Self = self(),
	{ok, spawn_link(fun()-> init(Id, Self, Rnd) end)}.

start(Id, Grp) ->
	Rnd = random:uniform(1000),
	Self = self(),
	{ok, spawn_link(fun()-> init(Id, Grp, Self, Rnd) end)}.


% -------------------------------------------------------
% Set start values for the node
%
init(Id, Master, Rnd) ->
	random:seed(Rnd, Rnd, Rnd),
	leader(Id, Master, 0, [], [Master]).

init(Id, Grp, Master, Rnd) ->
	random:seed(Rnd, Rnd, Rnd),
	Self = self(),
	Grp ! {join, Master, Self},
	receive
		{view, N, [Leader|Slaves], Group} ->
			erlang:monitor(process, Leader),
			Master!{view, Group},
			slave(Id, Master, Leader, N, 0, Slaves, Group)
	after ?timeout ->
		Master ! {error, "no reply from leader"}
	end.


% -------------------------------------------------------
% Elect the first available Slave as the new Leader
% Remove the first node as we know it is dead and should 
% not be elected the new leader.
%
election(Id, Master, N, Last, Slaves, [_|Group]) ->
	Self = self(),
	case Slaves of 
		[Self| Rest] ->
			bcast(Id, {view, N, Slaves, Group}, Rest),
			Master ! {view, Group},
			bcast(Id, {msg, N-1, Last}, Slaves),
			leader(Id, Master, N+1, Rest, Group);
		% The above [Self|Rest] match is imported to the latex report by line number
		% so if they change, update the report as well.
		[Leader|Rest] ->
			erlang:monitor(process, Leader),
			slave(Id, Master, Leader, N, Last, Rest, Group)
	end.

% -------------------------------------------------------
% Broadcast message to all slaves
%
bcast(Id, Msg, Nodes) ->
  lists:foreach(fun(Node) -> Node ! Msg, crash(Id) end, Nodes).

% -------------------------------------------------------
% Random crashes are not nice, but a necessary evil
%
crash(Id) ->
	case random:uniform(?arghh) of 
		?arghh ->
			io:format("leader ~w: crash~n", [Id]),
			exit(no_luck);
		_ ->
			ok
	end.
% -------------------------------------------------------
% The leader behaves slightly differently since it also handles 
% leader calls.
%
leader(Id, Master, N, Slaves, Group) ->
	receive
		{mcast, Msg} ->
			bcast(Id, {msg, N, Msg}, Slaves),
			Master!Msg,
			leader(Id, Master, N +1, Slaves, Group);
		{join, Wrk, Peer} ->
			Slaves2 = lists:append(Slaves, [Peer]),
			Group2 = lists:append(Group, [Wrk]),
			bcast(Id, {view, N, [self()|Slaves2], Group2}, Slaves2),
			Master ! {view, Group2},
			leader(Id, Master, N, Slaves2, Group2);
		stop ->
			ok
end.


% -------------------------------------------------------
% The leader behaves slightly differently since it also handles 
% leader calls.
% Handle the case that the leader is kill.
%
slave(Id, Master, Leader, N, Last, Slaves, Group) ->
	receive
		{mcast, Msg} ->
			Leader ! {mcast, Msg},
			slave(Id, Master, Leader, N, Last, Slaves, Group);
		{join, Wrk, Peer} ->
			Leader ! {join, Wrk, Peer},
			slave(Id, Master, Leader, N, Last, Slaves, Group);
		{msg, I, _} when I < N ->
			slave(Id, Master, Leader, N, Last, Slaves, Group);
		{msg, N, Msg} ->
			Master ! Msg,
			slave(Id, Master, Leader, N+1, Msg, Slaves, Group);
		{msg, I, Msg} ->
			io:format("~w: Got ahead message ~w while waiting for ~w~n", [Id, I, N]),
			slave(Id, Master, Leader, I+1, Msg, Slaves, Group);
		{'DOWN', _Ref, process, Leader, _Reason} ->
			election(Id, Master, N, Last, Slaves, Group);
		{view, N, [Leader|Slaves2], Group2} ->
			Master ! {view, Group2},
			slave(Id, Master, Leader, N+1, Last, Slaves2, Group2);
		stop ->
			ok
end.

