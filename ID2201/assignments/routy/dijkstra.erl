-module(dijkstra).
-export([ entry/2, replace/4, update/4, iterate/3, table/2, route/2 ]).
% -export([update/4,  table/2, route/2, iterate/3 ]).

% The format of the djikstra Nodes is
% {Node, int, Node}.
% The first value is the destination,
% The second value the distance,
% the last value the first hop from current router to get there.
%
% 
% So the Sorted list is a list of which Gateways are reachable,
% sorted by how quickly one can get there. It also contains where to 
% go to get there as fast as possible.
%
% The 'Table' is a list of mappings on how to route, say 
% [{paris, paris}, {stockholm, berlin}, {berlin, paris}]

entry(_, [])-> 0;
entry(Node, [{ Node, Dist, _ }| _])-> Dist;
entry(Node, [_| T])-> entry(Node, T).


replace(_, _, _, false, _) -> 
  io:format("Error! Tried replacing a non existant node in Dijsktra");

replace(Node, N, Gateway, [], Acc) ->
  T = lists:reverse([{Node, N, Gateway} | Acc]),
  T;

replace(Node, N, Gateway, [{Node1, N1, Gateway1} | T], Acc) when (N < N1) -> 
  lists:reverse([{Node, N, Gateway}|Acc]) ++ [{Node1, N1, Gateway1}| T];

replace(Node, N, Gateway, [H| T], Acc) -> 
  replace(Node, N, Gateway, T, [H|Acc]).

replace(Node, N, Gateway, Sorted) -> 
  replace(Node, N, Gateway, lists:keydelete(Node, 1, Sorted), []).


update(Node, N, Gateway, Sorted) -> 
  Dist = entry(Node, Sorted),
  if 
    N < Dist ->
      T = replace(Node, N, Gateway, Sorted),
      % io:format("updated Sorted ~n(~w)~nTo (~w)~n", [Sorted, T]),
      % timer:sleep(2000),
      T;
    true ->
      Sorted
  end.



iterate([{_,inf,_}|_], _, Table) -> Table;
iterate([], _, Table) -> Table;
iterate([{Node, _, Gateway}|[]], _, Table) ->
  [{Node, Gateway} | Table];

iterate([{Node, N, Gateway}|T], Map, Table) ->
  Reachnodes = map:reachable(Node, Map),
  Sorted = lists:foldl(fun(X, Acc0) -> update(X, N+1, Node, Acc0) end, T, Reachnodes),
  Tab = [{Node, Gateway} | Table],
  iterate(Sorted, lists:keydelete(Node, 1, Map), Tab).



% Gateways: [paris, madrid]
% Map:  [{madrid,[berlin]}, {paris, [rome,madrid]}]
% Input example :dijkstra:table([paris, madrid],[{madrid,[berlin]},{paris,[rome,madrid]}]).
% Output example:[{berlin,madrid},{rome,paris},{paris,paris},{madrid,madrid}]
table(Gateways, Map) -> 
  % Map: [{stockholm,[kista,copenhagen]},{sundsvall,[kista]},{kista,[sundsvall,stockholm]},{odense,[copenhagen]},{copenhagen,[odense,stockholm]}]
  % Gateways: [kista, copenhagen]
  % Get a list of all Nodes
  AllNodes = map:all_nodes(Map),
  % [kista,odense,sundsvall,stockholm,copenhagen]
  
  % Remove the gateways not inf from the list
  RestMap = lists:subtract(AllNodes, Gateways),
  % [odense,sundsvall,stockholm]
  
  % Set All non-gateways to inf length
  InfMap = lists:foldl(fun(X, Acc0) -> [{X, inf, unknown} | Acc0] end, [], RestMap),
  % [{stockholm,inf,unknown}, {sundsvall,inf,unknown}, {odense,inf,unknown}]

  % Add the gateways at 0 distance from themselves, completing the list
  Init = lists:foldl(fun(X, Acc0) -> [{X, 0, X} | Acc0] end, InfMap, Gateways),
  % [{copenhagen,0,copenhagen}, {kista,0,kista}, {stockholm,inf,unknown}, {sundsvall,inf,unknown}, {odense,inf,unknown}]


  % Use iterate to set the correct values and return the complete table.
  % Empty Table given by exercies.
  iterate(Init, Map, []).

route(false, _)-> notfound;
route({Gateway, Gateway}, _) -> 
  {ok, Gateway};
route({_, Other}, Table) -> 
  route(Other, Table);


route(Node, Table) ->
  route(lists:keyfind(Node, 1, Table), Table).
