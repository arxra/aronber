-module(routy).
-export([start/2, stop/1, status/1]).



start(Reg, Name) ->
    register(Reg, spawn(fun() -> init(Name) end)).

stop(Node) ->
    Node ! stop,
    unregister(Node).

init(Name) ->
  Intf = interfaces:new(),
  Map = map:new(),
  Table = dijkstra:table(Intf, Map),
  Hist = hist:new(Name),
  router(Name, 0, Hist, Intf, Table, Map).

status(Name) ->
  Name ! {status, self()},
  receive 
    {status, Response}->
      io:format("status of ~w:~w", [Name, Response])
  end.


router(Name, Counter, Hist, Intf, Table, Map) -> 
  receive
    {add, Node, Pid} ->
      io:format("Adding ~w ~w~n", [Node, Pid]),
      Ref = erlang:monitor(process, Pid),
      Intf1 = interfaces:add(Node, Ref, Pid, Intf),
      router(Name, Counter, Hist, Intf1, Table, Map);
    {remove, Node} ->
      {ok, Ref} = interfaces:ref(Node, Intf),
      erlang:demonitor(Ref),
      Intf1 = interfaces:remove(Node, Intf),
      router(Name, Counter, Hist, Intf1, Table, Map);
    {'DOWN', Ref, process, _, _} ->
      {ok, Down} = interfaces:name(Ref, Intf),
      io:format("~w: exit reciefved from ~w~n", [Name, Down ]),
      Intf1 = interfaces:remove(Down, Intf),
      router(Name, Counter, Hist, Intf1, Table, Map);
    {links, Node, R, Links} ->
      io:format("~w: recieved links: ~w~n", [Name, Links]),
      case hist:update(Node, R, Hist) of 
        {new, Hist1} -> 
          interfaces:broadcast({links, Node, R, Links}, Intf),
          Map1 = map:update(Node, Links, Map),
          router(Name, Counter, Hist1, Intf, Table, Map1);
        old ->
          router(Name, Counter, Hist, Intf, Table, Map)
      end;
    update ->
      Table1 = dijkstra:table(interfaces:list(Intf), Map),
      router(Name, Counter, Hist, Intf, Table1, Map);
    brodcast ->
      Message = {links, Name, Counter, interfaces:list(Intf)},
      io:format("~w is brodcasting its list: ~w~n", [Name, Message]),
      interfaces:broadcast(Message, Intf),
      router(Name, Counter+1, Hist, Intf, Table, Map);
    {route, Name, From, Message} ->
      io:format("~w: recieved message (~w) from ~w~n", [Name, Message, From]),
      router(Name, Counter, Hist, Intf, Table, Map);
    {route, To, From, Message} ->
      io:format("~w: routing message(~w)~n", [Name, Message]),
      case dijkstra:route(To, Table) of 
        {ok, Gw} ->
          case interfaces:lookup(Gw, Intf) of
            {ok, Pid} ->
              Pid ! {route, To, From, Message};
            notfound ->
              ok
          end;
        notfound ->
          ok
      end,
      router(Name, Counter, Hist, Intf, Table, Map);
    {send, To, Message} ->
      self() ! {route, To, Name, Message},
      router(Name, Counter, Hist, Intf, Table, Map);
    {status, From} ->
      From ! {status, {Name, Counter, Hist, Intf, Table, Map}},
      router(Name, Counter, Hist, Intf, Table, Map);
    stop ->
      ok
  end.

