-module(test).
-export([sweden/1, denmark/1, uswe/0, uden/0]).

sweden(Adr) ->
  routy:start(st, stockholm),
  routy:start(ks, kista),
  routy:start(sv, sundsvall),
  routy:start(gt, gothenburg),

  sv!{add, kista, {ks, Adr}},
  ks!{add, sundsvall,{sv, Adr}},
  ks!{add, stockholm, {st, Adr}},
  st!{add, kista, {ks, Adr}},
  st!{add, gothenburg, {gt, Adr}},
  gt!{add, stockholm, {st, Adr}},

  timer:sleep(5000),
  uswe().

uswe() ->
  sv!brodcast,
  ks!brodcast,
  st!brodcast,
  gt!brodcast,

  gt!update,
  sv!update,
  ks!update,
  st!update.

denmark(Adr) -> 
  routy:start(ch, copenhagen),
  routy:start(od, odense),

  ch!{add, odense, {od, Adr}},
  od!{add, copenhagen, {ch, Adr}},

  uden(),
  ok.

uden() ->
  ch!brodcast,
  od!brodcast,

  ch!update,
  od!update.

