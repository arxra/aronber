% MIT License

% Copyright (c) [2020] [Yannik Sander]

% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

-module(tests).

-export([stop_all/1, unit/0, test_dijkstra/0,
	 test_history/0, test_intf/0, test_map/0, test_routy/0,
	 test_routy_connect/0, test_routy_disconnect/0,
	 test_routy_link_state/0, test_routy_send_receive/0,
	 test_routy_update/0]).

unit() ->
    #{map => test_map(), dijkstra => test_dijkstra(),
      interface => test_intf(), history => test_history()}.

% =============== map tests =================
test_map() ->
    [catch test_map_new(), catch test_map_update(),
     catch test_map_reachable(), catch test_map_all_nodes()].

test_map_new() -> #{} = map:new(), ok.

test_map_update() ->
    Init = map:update(berlin, [london, paris], map:new()),
    #{berlin := [london, paris]} = Init,
    Updated = map:update(berlin, [stockholm, rome], Init),
    #{berlin := [stockholm, rome]} = Updated,
    Updated1 = map:update(vienna, [berlin, paris], Updated),
    #{berlin := [stockholm, rome],
      vienna := [berlin, paris]} =
	Updated1,
    ok.

test_map_reachable() ->
    Init = map:update(berlin, [london, paris], map:new()),
    [london, paris] = map:reachable(berlin, Init),
    [] = map:reachable(rome, Init),
    ok.

test_map_all_nodes() ->
    Init0 = map:update(berlin, [london, paris], map:new()),
    Init1 = map:update(vienna, [berlin, paris], Init0),
    true = lists:sort([vienna, london, paris, berlin]) =:=
	     lists:sort(map:all_nodes(Init1)),
    ok.

% =============== dijkstra tests =================

test_dijkstra() ->
    [catch test_dijkstra_entry(),
     catch test_dijkstra_replace(),
     catch test_dijkstra_update(),
     catch test_dijkstra_iterate(),
     catch test_dijkstra_table(),
     catch test_disjkstra_route()].

test_dijkstra_entry() ->
    Sorted = [{stockholm, 1, copenhagen}, {paris, 1, rome},
	      {rome, 2, berlin}, {berlin, 2, prague},
	      {minsk, 4, berlin}],
    2 = dijkstra:entry(berlin, Sorted),
    1 = dijkstra:entry(paris, Sorted),
    0 = dijkstra:entry(oslo, Sorted),
    ok.

test_dijkstra_replace() ->
    SortedPre = [{stockholm, 1, copenhagen},
		 {paris, 1, rome}, {rome, 2, berlin},
		 {berlin, 2, prague}, {minsk, 4, berlin}],
    SortedPost = [{stockholm, 1, copenhagen},
		  {paris, 1, rome}, {minsk, 1, prague}, {rome, 2, berlin},
		  {berlin, 2, prague}],
    SortedPost = dijkstra:replace(minsk, 1, prague,
				  SortedPre),
    ok.

test_dijkstra_update() ->
    [] = dijkstra:update(london, 2, amsterdam, []),
    [{london, 2, paris}] = dijkstra:update(london, 2,
					   amsterdam, [{london, 2, paris}]),
    [{london, 1, stockholm}, {berlin, 2, paris}] =
	dijkstra:update(london, 1, stockholm,
			[{berlin, 2, paris}, {london, 3, paris}]),
    ok.

test_dijkstra_iterate() ->
    [{paris, paris}, {berlin, paris}] =
	dijkstra:iterate([{paris, 0, paris},
			  {berlin, inf, unknown}],
			 map:update(paris, [berlin], map:new()), []),
    ok.

test_dijkstra_table() ->
    Map1 = map:update(madrid, [berlin], map:new()),
    Map2 = map:update(paris, [rome, madrid], Map1),
    Expect = [{berlin, madrid}, {rome, paris},
	      {madrid, madrid}, {paris, paris}],
    true = lists:sort(Expect) =:=
	     lists:sort(dijkstra:table([paris, madrid], Map2)),
    ok.

test_disjkstra_route() ->
    Table = [{berlin, madrid}, {rome, paris},
	     {madrid, madrid}, {paris, paris}, {stockholm, rome}],
    {ok, madrid} = dijkstra:route(berlin, Table),
    {ok, madrid} = dijkstra:route(madrid, Table),
    {ok, paris} = dijkstra:route(stockholm, Table),
    {ok, paris} = dijkstra:route(rome, Table),
    notfound = dijkstra:route(rio, Table),
    ok.

% =============== interface tests =================

test_intf() ->
    [catch test_intf_mods(), catch test_intf_names(),
     catch test_intf_broadcast()].

test_intf_mods() ->
    Intf0 = interface:new(),
    notfound = interface:lookup(name, Intf0),
    notfound = interface:ref(name, Intf0),
    notfound = interface:name(ref, Intf0),
    Intf1 = interface:add(name, ref, pid, Intf0),
    {ok, pid} = interface:lookup(name, Intf1),
    {ok, ref} = interface:ref(name, Intf1),
    {ok, name} = interface:name(ref, Intf1),
    Intf2 = interface:remove(name, Intf1),
    notfound = interface:lookup(name, Intf2),
    notfound = interface:ref(name, Intf2),
    notfound = interface:name(ref, Intf2),
    ok.

test_intf_names() ->
    Intf0 = interface:new(),
    Intf1 = interface:add(name1, ref1, pid1, Intf0),
    Intf2 = interface:add(name2, ref2, pid2, Intf1),
    Intf3 = interface:add(name3, ref3, pid3, Intf2),
    true = lists:sort([name3, name2, name1]) ==
	     lists:sort(interface:list(Intf3)),
    ok.

test_intf_broadcast() ->
    Message = "Test Message",
    Pid = self(),
    Recv = spawn(fun () -> broadcast_receive(3, Pid) end),
    [Pid1, Pid2, Pid3] = [spawn(fun () ->
					broadcast_client(Recv, Message)
				end)
			  || _ <- [0, 0, 0]],
    Intf0 = interface:new(),
    Intf1 = interface:add(name1, ref1, Pid1, Intf0),
    Intf2 = interface:add(name2, ref2, Pid2, Intf1),
    Intf3 = interface:add(name3, ref3, Pid3, Intf2),
    interface:broadcast(Message, Intf3),
    receive
      ok -> ok;
      Error -> Error
      after 1000 -> {error, timeout}
    end.

broadcast_client(Parent, Expect) ->
    receive
      Expect -> io:format("e"), Parent ! ok;
      Receive ->
	  io:format("u"), Parent ! {errot, self(), Receive}
    end.

broadcast_receive(0, Main) ->
    io:format("ok"), Main ! ok;
broadcast_receive(N, Main) ->
    io:format("~p", [N]),
    receive
      ok -> broadcast_receive(N - 1, Main);
      Error -> Main ! Error
    end.

% =============== history tests =================

test_history() -> [catch test_history_check_update()].

test_history_check_update() ->
    History0 = history:new(london),
    old = history:update(london, 1, History0),
    old = history:update(london, 1000, History0),
    old = history:update(london, -10, History0),
    {new, History1} = history:update(bern, 1, History0),
    {new, History2} = history:update(bern, 10, History1),
    old = history:update(bern, 1, History2),
    old = history:update(bern, 5, History2),
    old = history:update(bern, 10, History2),
    ok.

% =============== routy tests =================
test_routy() ->
    [catch manual_test("Connect", fun test_routy_connect/0),
     catch manual_test("Disconnect",
		       fun test_routy_disconnect/0),
     catch manual_test("Link State Messages",
		       fun test_routy_link_state/0),
     catch manual_test("Update", fun test_routy_update/0),
     catch manual_test("Send and receive",
		       fun test_routy_send_receive/0)].

test_routy_connect() ->
    routy:start(r1, stockholm),
    routy:start(r2, lund),
    routy:start(r3, paris),
    r1 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r1 ! {add, berlin, {r4, 'eu@127.0.0.1'}},
    r3 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    check(r1),
    check(r2),
    check(r3),
    try ok = ask() after stop_all([r1, r2, r3]) end.

test_routy_disconnect() ->
    routy:start(r1, stockholm),
    routy:start(r2, lund),
    routy:start(r3, paris),
    r1 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r2 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r3 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    check(r1),
    check(r2),
    check(r3),
    try io:format("Stopping stockholm...~n"),
	routy:stop(r1),
	timer:sleep(100),
	check(r2),
	check(r3),
	ok = ask()
    after
      stop_all([r1, r2, r3])
    end.

test_routy_link_state() ->
    routy:start(r1, stockholm),
    routy:start(r2, lund),
    routy:start(r3, paris),
    r1 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r2 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r3 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    try r1 ! broadcast,
	r2 ! broadcast,
	r3 ! broadcast,
	timer:sleep(100),
	check(r1),
	check(r2),
	check(r3),
	timer:sleep(100),
	ok = ask()
    after
      stop_all([r1, r2, r3])
    end.

test_routy_update() ->
    routy:start(r1, stockholm),
    routy:start(r2, lund),
    routy:start(r3, paris),
    routy:start(r4, zurich),
    routy:start(r5, berlin),
    r1 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r1 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r2 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r3 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r3 ! {add, zurich, {r4, 'eu@127.0.0.1'}},
    r3 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    r4 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    r5 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    try r1 ! broadcast,
	r2 ! broadcast,
	r3 ! broadcast,
	r4 ! broadcast,
	r5 ! broadcast,
	timer:sleep(100),
	r1 ! update,
	r2 ! update,
	r3 ! update,
	r4 ! update,
	r5 ! update,
	check(r1),
	check(r2),
	check(r3),
	check(r4),
	check(r5),
	timer:sleep(100),
	ok = ask()
    after
      stop_all([r1, r2, r3, r4, r5])
    end.

test_routy_send_receive() ->
    routy:start(r1, stockholm),
    routy:start(r2, lund),
    routy:start(r3, paris),
    routy:start(r4, zurich),
    routy:start(r5, berlin),
    r1 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r1 ! {add, lund, {r2, 'eu@127.0.0.1'}},
    r2 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r2 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    r3 ! {add, zurich, {r4, 'eu@127.0.0.1'}},
    r3 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    r4 ! {add, berlin, {r5, 'eu@127.0.0.1'}},
    r5 ! {add, zurich, {r4, 'eu@127.0.0.1'}},
    r5 ! {add, paris, {r3, 'eu@127.0.0.1'}},
    r5 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
    r1 ! broadcast,
    r2 ! broadcast,
    r3 ! broadcast,
    r4 ! broadcast,
    r5 ! broadcast,
    timer:sleep(200),
    r1 ! update,
    r2 ! update,
    r3 ! update,
    r4 ! update,
    r5 ! update,
    timer:sleep(200),
    check(r1),
    check(r2),
    check(r3),
    check(r4),
    check(r5),
    try r1 ! {send, stockholm, "Hello World!"},
	timer:sleep(100),
	ok = ask("Should only be received by `stockholm`"),
	r1 ! {send, paris, "Hello World!"},
	timer:sleep(100),
	ok = ask("Should be routed to `paris` through "
		 "`stockholm`"),
	r1 ! {send, berlin, "Hello World!"},
	timer:sleep(100),
	ok = ask("Should be routed to `berlin` through "
		 "`stockholm -> lund or paris`"),
	r1 ! {send, zurich, "Hello World!"},
	timer:sleep(100),
	ok = ask("Should fail to be routed to `zurich`. "
		 "Route is not broadcasted back to `stockholm`."),
	r3 ! {add, stockholm, {r1, 'eu@127.0.0.1'}},
	r3 ! update,
	r3 ! broadcast,
    timer:sleep(200),
	r1 ! {send, zurich, "Hello World!"},
    timer:sleep(100),
	ok = ask("Should find route now!\nShould be routed "
		 "to `zurich` through `stockholm -> paris`"),
	routy:stop(r3),
	timer:sleep(200),
	r1 ! {send, zurich, "Hello World!"},
	timer:sleep(100),
	ok = ask("Stop has propagated?\nShould be routed "
		 "to `zurich` thorugh `stockholm -> lund "
		 "-> berlin`")
    after
      stop_all([r1, r2, r3, r4, r5])
    end,
    ok.

%%%%%%%%%%%%%%%%%%%% Utils %%%%%%%%%%%%%%%%%%%%%%

check(Router) ->
    Pid = self(),
    Router ! {status, Pid},
    receive
      {status, Info} ->
	  io:format("status (~p): ~p~n", [Router, Info])
    end.

ask() ->
    {ok, Answer} = io:read("Ok? yes(y)/no(n)> "),
    io:format("~n"),
    case Answer of
      Yes
	  when Yes == "y"; Yes == "yes"; Yes == y; Yes == yes ->
	  ok;
      No when No == "n"; No == "no"; No == n; No == no ->
	  error;
      _ -> io:format("Sorry, what?~n"), ask()
    end.

ask(Question) -> io:format("~n~s~n", [Question]), ask().

manual_test(Name, Func) ->
    io:format("~n~n~n###################### TEST ~p "
	      "######################",
	      [Name]),
    Func(),
    io:format("#################### END TEST ~p ############"
	      "########~n~n~n",
	      [Name]).

stop_all([]) -> io:format("All stopped~n");
stop_all([Ref | More]) ->
    catch routy:stop(Ref), stop_all(More).
