-module(hist).
-export([new/1, update/3]).

% Why do I need to append a element to a empty list to get a list?
new(Name)-> [{Name, 0}|[]].

update(Node, N, History)->
  Key = lists:keyfind(Node, 1, History),
  case Key of 
    {_,K} -> 
      if
        K < N ->
          Clean =  lists:keydelete(Node, 1, History),
          {new, [{Node, N}|Clean]};
        true ->
          old
      end;
    false -> 
          {new, [{Node, N}| History]}
  end.
