-module(interfaces).
-export([new/0, add/4, remove/2, lookup/2, ref/2, name/2, list/1, broadcast/2]).

new()-> [].


% add a new entry to the set and return
% the new set of interfaces.
add(Name, Ref, Pid, Intf ) -> [{Name, Ref, Pid}|Intf].


remove(Name, Intf) -> lists:keydelete(Name, 1, Intf).

lookup(false) -> notfound;
lookup({_,_,Pid}) -> {ok, Pid}.
lookup(Name, Intf ) -> lookup(lists:keyfind(Name, 1, Intf)).

ref(false) -> notfound;
ref({_,Ref,_}) -> {ok, Ref}.
ref(Name, Intf ) -> ref(lists:keyfind(Name, 1, Intf)).

name(false) -> notfound;
name({Name, _, _}) -> {ok, Name}.
name(Ref, Intf ) -> name(lists:keyfind(Ref, 2, Intf)).

list(Intf) -> lists:foldl(fun({Name,_,_}, Acc0)-> [Name | Acc0] end, [], Intf).

broadcast(Message, Intf ) -> lists:foreach(fun({_,_,Pid}) -> Pid ! Message end, Intf).
