-module(map).
-export([new/0, update/3, reachable/2, all_nodes/1]).

% This module add support for our map struct.
% examples of instances of Map:
% [{berlin,[london,paris]}]
% [{berlin,[london,paris]}, {stockholm, [helsinki,paris]}]
%
% The map contains touples of keys and lists of directly connected cities.
% Think of Distance 1 in the Sorted list in Dijsktra where 
% [{berlin,[london,paris]}]
% would be 
% [{london, 1, berlin}, {paris, 1, berlin}]
%


% New is straight forward
new() ->
  [].

% Update the Map with a new node, linking to Links and put it in Map
% Note:  The old entry is removed on update.
% Examples:
%
% Input: berlin, [london, paris], []
% output: [{berlin,[london,paris]}]
%
% Input: (berlin, [madrid], [{berlin,[london,paris]}]).
% output: [{berlin, [madrid]}]

update(Node, Links, Map) ->
  [{Node, Links} | lists:keydelete(Node, 1, Map)].

% Given a Node, what nodes are reachable?
% input: (berlin, [{berlin,[london,paris]}]).
% output: ([london,paris])
reachable(false)-> [];
reachable({_, List}) -> List.
reachable(Node, Map) ->
  reachable(lists:keyfind(Node, 1, Map)).


% Flat the map into a list of all Nodes.
% Seems like foldl/3 was suggested here, but flatmap is cleaner imho.
%
% input: ([{berlin,[london,paris]}]).
% output: [paris,london,berlin]
all_nodes([]) -> [];
all_nodes(Map) -> 
  List = lists:flatmap(fun({A,B})-> [A] ++ B end, Map),
  Set = sets:from_list(List),
  sets:to_list(Set).
