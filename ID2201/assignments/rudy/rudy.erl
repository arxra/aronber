-module(rudy).
-export([start/2, stop/0]).

start(Port, N) ->
  % We take the amount of web threads as a argument to test easier
  register(rudy, spawn(fun() -> init(Port, N)end)).

stop() ->
  exit(whereis(rudy), "Time to die ").


init(Port, N) ->
  Opt = [list, {active, false}, {reuseaddr, true}],
  case gen_tcp:listen(Port, Opt) of
    {ok, Listen} ->
      handlers(Listen, N),
      waiter(),
      gen_tcp:close(Listen),
      ok;
    {error, _} ->
      error
  end.

waiter() ->
  receive
    stop ->
      ok
  end.

handlers(_, 0) -> ok;
handlers(Listen, N) ->
  spawn(fun() -> handler(Listen, N) end),
  handlers(Listen, N-1).

handler(Listen, K) ->
  case gen_tcp:accept(Listen) of 
    {ok, Client} ->
      request(Client);
    {error, _} ->
      error
  end,
  handler(Listen, K).

request(Client) ->
  Recv = gen_tcp:recv(Client, 0),
  case Recv of 
    {ok, Str} ->
      Request = http:parse_request(Str),
      Response = reply(Request),
      gen_tcp:send(Client, Response);
    {error, Error} ->
      io:format("rudy: error: ~w~n", [Error])
  end,
  gen_tcp:close(Client).


reply({{get, Uri, _}, _, _}) ->
  % timer:sleep(40),
  http:ok(Uri).
