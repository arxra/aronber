# ID2201 HT20-1 Distributed Systems, Basic Course

All course activities - lectures, help sessions and reporting sessions (seminars) — will be carried online according to the course schedule in the following ZoomLinks to an external site. rooms.

    Lectures https://kth-se.zoom.us/j/64636708568 (Links to an external site.)
    Help sessions and reporting sessions (seminars, hosted by TAs) https://kth-se.zoom.us/j/69679756122 (Links to an external site.)

See the course schedule here (Links to an external site.)

The course covers fundamental models for distributed systems, inter process communication and how to handle synchronization, consistency, replication, fault tolerance and security in a distributed system. The course consist of a series of lectures and practical homework assignments to be presented and demonstrated at online seminars (reporting sessions) over Zoom. The assignments will be programming tasks that exemplify problem statements examined in the course.
Intended learning outcomes

The students shall after the course be able to:

- explain important characteristics of distributed systems;
- describe architectural and fundamental models of distributed systems;
- explain and compare strategies for interprocess communication;
- explain and compare middleware models;
- explain and compare name services;
- explain the concept of logical time;
- use logical time to implement distributed algorithms.

Examination

    LAB1 - Laboratory Work, 1.5, grade scale: P, F
    TEN1 - Examination, 6.0, grade scale: A, B, C, D, E, FX, F

The examination consists of a written examination (TEN1) and programming assignments (LAB1).
Assignments should be done and presented at online seminar sessions over Zoom.
Final grade is based on the performance of the written exam and the programming assignments.

See also

    Examination page;
    Assignments

Course Book

Distributed Systems - Concepts and Design (Links to an external site.), by George Coulouris, Jean Dollimore, Tim Kindberg, Fifth Edition, Addison-Wesley, ISBN: 0-273-76059-9 [PDFPreview the document]

See also Contents of the Course Book
Lectures

There are 14 lectures that mainly follow the course book. Lectures are not compulsory but highly recommended.
Lecture 	Book chapter

    Introduction
    Erlang
    Networks and Interprocess Communication
    Remote Invocation
    Indirect Communication
    File Systems and Name Services
    Time
    Global State
    Coordination and Agreement
    Transactions and Concurrency Control
    Distributed Transactions
    Replication
    Distributed Hash Tables
    Summary and Paxos Consensus Protocol

	

    1-2
    3-4
    5
    6
    12-13
    14
    14
    15
    16
    17
    18
    10

Supplementary reading

    Distributed Systems - Principles and Paradigms, by Maarten Van Steen and Andrew S. Tannebaum, third (Links to an external site.)  or second (Links to an external site.) edition, Pearson Education, ISBN: 0-13-239227-5 
    This book could equally well have been chosen as the course book. It covers more or less the same material.

    Distributed Computing - Principles, Algorithms, and Systems, by Ajay D. Kshemkalyani and Mukesh Singhal, Cambridge Univeristy Press, ISBN: 978-0-521-87634-6
    A book that is more theoretical than the course book. The book covers topics that are handled in the advanced course. If you think that the theory is the most important part of distributed systems and know the basics of networks and programming systems this could be the book for you.

    Programming Erlang - Software for a Concurrent World, by Joe Armstrong, the Pragmatic Programmers, ISBN: 987-1-934356-00-5
    This is the book if you want to go further in Erlang programming. There is of course a lot of material on the web but why not take it from one of the most experienced Erlang programmer.

    Erlang Programming - A Concurrent Approach to Software Development, by Francesco Cesarini, Simon Thompson, June 2009 , O'Reilly, ISBN 10: 0-596-51818-8 | ISBN 13: 9780596518189
    Ont of the latest additions to Erlang books, now by the people who might have the most experience in teaching Erlang: Francesco from Erlang Training and Consulting and Simon from the University of Kent.


