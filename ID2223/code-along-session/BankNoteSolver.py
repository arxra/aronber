import tensorflow as tf
# import torch.BankNoteDataset as bnds
from tensorflow.keras import layers

# trainX, trainY, testX, testY = bnds.getData()
# print(testX.shape, testY.shape)

# model = tf.keras.Sequential()
# model.add(tf.keras.layers.Dense(64, 'relu'))
# model.add(tf.keras.layers.Dense(64, 'relu'))
# model.add(tf.keras.layers.Dense(1, 'sigmoid'))

inputLayer = layers.Input((4,))
d1 = layers.Dense(64, 'relu')
d2 = layers.Dense(16, 'tanh')
d3 = layers.Dense(16, 'sigmoid')
conccatLayer = layers.Concatenate()
finalLayer = layers.Dense(1, 'sigmoid')

y1 = d1(inputLayer)

y2 = d2(y1)
y3 = d3(y1)
concatOoutput = conccatLayer([y2, y3])
finalY = finalLayer(concatOoutput)

model = tf.keras.Model(inputLayer, finalY)

tf.keras.utils.plot_model(model, 'model.png')


# optimnizer = tf.optimizers.SGD(0.001)
# model.compile(optimzier=optimnizer, loss='binary_crossentropy', metrics=['accuracy'])

# tf.keras.utils.plot_model(model, 'MyTestMoedl.png', show_shapes=True)

# results = model.evalueate(trainX, trainY)
