import tensorflow as tf


def quadFund(x):
    return x ** 2 + 4 * x + 4


myVarX = tf.Variable(0, dtype=tf.float32)
targetY = 0
lr = 0.02

for i in range(1000):
    with tf.GradientTape() as tape:
        y = quadFund(myVarX)
        print(y)
        loss = (y - targetY) ** 2
        print("Y value: ", y)

        loss = (y - targetY) ** 2
        print("Delta: ", y - targetY)
        print("Loss: ", loss)

        gradient = tape.gradient(loss, myVarX)
        print("Gradient: ", gradient)
        myVarX.assign_sub(gradient * lr)

        print("------------------")
