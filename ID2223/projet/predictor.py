from time import sleep

import numpy as np
import pandas as pd
import scipy.io.wavfile
import sounddevice as sd
import tensorflow as tf
from feature_extractor import extractract_features as ef
from feature_extractor import genres as genres

model = tf.keras.models.load_model("our_model")
model.summary()

file_name = "/tmp/sample.wav"
channels = 1
sr = 22050  # Record at standard sampling rate
sd.default.device = 11

# print(sd.query_devices(7, 'input'))


data_features = pd.read_csv('songfeatures.csv')


print("Analyze audio?")
# input()

# Record some audio
# recording = sd.rec(int(sr*30), samplerate=sr, channels=channels)
stream = sd.InputStream(samplerate=sr, channels=channels)

while input() is not None:
    stream.start()
    print(genres)
    frames = np.array([])  # Initialize array to store frames

    # Store data in chunks for 30 seconds
    for i in range(1, 31):
        # Wait for the audio to complete
        frames = np.append(frames, stream.read(sr)[0])
        # print(len(frames))
        # print(frames)
        # print(type(frames))
        features = ef(frames, sr)
        # print(features.shape)
        pred = model.predict(np.array([features]))[0]
        index = np.where(pred == pred.max())[0][0]
        genres2 = np.delete(np.array(genres), index)
        pred2 = np.delete(pred, index)
        index2 = np.where(pred2 == pred2.max())[0][0]
        # print(f"pred: {pred}")
        # print(f"pred2: {pred2}")
        # print(f"index2: {index2}")
        # print(f"index: {index}")
        print(
            f"\tprediction at {i}: {genres[index]}({ int(pred[index] * 100) }%),"
            + f" {genres2[index2]}({ int(pred2[index2] * 100) }%)"
        )

    scipy.io.wavfile.write("karplus.wav", sr, frames)
    stream.stop()
    print(f"prediction for song: {genres[index]}")
    print(f"Enter anything for a re-run")
