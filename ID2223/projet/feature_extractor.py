import numpy as np
import pandas as pd
import librosa
import os

def extractract_features(x: np.ndarray, sr: int):
    # https://librosa.org/doc/latest/generated/librosa.feature.chroma_stft.html?highlight=chroma#librosa.feature.chroma_stft
    # stft = np.abs(librosa.stft(x))
    # chroma_stft = librosa.feature.chroma_stft(S=stft, sr=sr)
    # chroma_mean = chroma_stft.mean(axis=1).T
    # chroma_std = chroma_stft.std(axis=1).T
    # chroma_feature = np.hstack([chroma_mean, chroma_std])

    # https://medium.com/prathena/the-dummys-guide-to-mfcc-aceab2450fd 
    # https://towardsdatascience.com/how-to-apply-machine-learning-and-deep-learning-methods-to-audio-analysis-615e286fcbbc
    # https://librosa.org/doc/latest/generated/librosa.feature.mfcc.html
    mfcc = librosa.feature.mfcc(x, sr=sr, n_mfcc=40)
    mfcc_meaned = np.mean(mfcc.T,axis=0)
    return mfcc_meaned

def load_sample(file_name):
    x, sr= librosa.load(file_name)
    return (x, sr)


genres = ["blues", "classical", "country", "disco", "hiphop", "jazz", "metal", "pop", "reggae", "rock"] 
features = []
for gen in genres:
    for song in os.listdir("./genres/" + gen):
        (x, sr) = load_sample(f"./genres/{gen}/{song}")
        features.append([extractract_features(x, sr), gen])


featuresdf = pd.DataFrame(features, columns=['features', 'class_label'])
featuresdf.to_csv('songfeatures.csv', index=False)
