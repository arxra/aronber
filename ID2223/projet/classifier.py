import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.layers import Activation, Dense, LeakyReLU
from tensorflow.keras.models import Sequential

# from feature_extractor import genres as genres
from tensorflow.keras.utils import to_categorical


class MyModel(Sequential):
    def __init__(self, shape):
        super().__init__()
        self.add(tf.keras.Input(shape=(shape[1],)))
        # self.add(Dense(512, activation=LeakyReLU(alpha=0.1)))
        self.add(Dense(256, activation=LeakyReLU(alpha=0.1)))
        self.add(Dense(128, activation=LeakyReLU(alpha=0.1)))
        self.add(Dense(64, activation=LeakyReLU(alpha=0.1)))
        self.add(Dense(len_labels))
        self.add(Activation('softmax'))
        self.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='adam')


data_features = pd.read_csv('songfeatures.csv')

feature_list = data_features.features.tolist()

fets = []
for row in feature_list:
    a = np.array(list(filter(None, row.strip("\n[]").split(" ")))).astype(float)
    fets.append(a)

fets = np.array(fets)
classes = np.array(data_features.class_label.tolist())

# Encode labels
le = LabelEncoder()
cclasses = to_categorical(le.fit_transform(classes))

# split training and testing data
x_train, x_test, y_train, y_test = train_test_split(fets, cclasses, test_size=0.2)

len_labels = cclasses.shape[1]

print(x_train.shape[1])
model = MyModel(x_train.shape)
# Model summary
model.summary()

# # Calculate pre-training accuracy
# score = model.evaluate(x_test, y_test, verbose=0)
# accuracy = 100*score[1]
# print(f"score: {accuracy}")

num_epochs = 200
num_batch_size = 32
model.fit(
    x=x_train, y=y_train, batch_size=num_batch_size, epochs=num_epochs, validation_data=(x_test, y_test), verbose=2, shuffle=True
)

test_loss, test_acc = model.evaluate(x_test, y_test)
print(f"test loss: {test_loss}")
print(f"test acc: {test_acc}")

model.save("our_model")
