defmodule Listops do
  def nth(list,index) do
    [head|tail] = list
    if index==0 do
      head
    else
      nth(tail,index-1)
    end
  end

  def len(list) do
    len(list, 0)
  end

  def len([],count) do count end

  def len([_|tail], count) do
    len(tail, count+1)
  end

  def sum(list) do sum(list,0) end
  def sum([], n) do n end
  def sum([head|tail], n) do sum(tail, n+head) end

  def dupluicate([]) do

  end
  def dupluicate([head | tail]) do
    [head, head | dupluicate (tail)]
  end

  def add(x,[])       do [x] end
  def add(x,[x | t])  do [x | t] end
  def add(x,[h | t])  do [h| add(x,t)] end

  def remove(_, [])    do [] end
  def remove(x, [x|t]) do remove(x,t) end
  def remove(x, [h|t]) do [h|remove(x,t)] end

  def unique([h]) do [h] end
  def unique([h|t]) do
     t2 = remove(h,t)
     [h|unique(t2)]
  end
end
