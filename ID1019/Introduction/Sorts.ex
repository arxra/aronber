defmodule Sort do
   def insert(element, []) do
      [element]
   end
   def insert(element, [h|t]) do
      if (element<h) do
         [element|[h|t]]
      else
         [h|insert(element,t)]
      end
   end
   def isort([],l) do
      l
   end
   def isort([k|j], l) do
      isort(j,insert(k,l))
   end
   def isort(k) do
      isort(k, [])
   end
end
