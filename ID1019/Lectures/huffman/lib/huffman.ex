defmodule Huffman do
  @moduledoc """
  Huffman Doqumentations of codes
  """
  def sample do
    'the quick brown fox jumps over the lazy dog
    this is a sample text that we will use when we build
    up a table we will only handle lower case letters and
    no punctuation symbols the frequency will of course not
    represent english but it is probably not that far off'

  end

  def text, do: 'this is something that we should encode'

  def test do
    sample = sample()
    tree = tree(sample)
    encode = encode_table(tree)
    decode = decode_table(tree)
    text = text()
    seq = encode(text, encode)
    decode(seq, decode)

  end

  def tree(sample) do
    freq = freq(sample)
    huffman(freq)
  end

  def huffman(freq) do 
    list = List.keysort(freq,1) 
    huffman_tree(list)
  end

  def huffman_tree([{a,k}]) do a end

  def huffman_tree([{c1, k1},{c2, k2}|rest]) do
    nod = {{c1, c2}, k1+k2} 
    huffman_tree(insert(nod, rest))
  end

  def insert(elemt, []) do [elemt] end
  def insert({var, k}, [{kar, x}|tail]) do
    if (k<x) do 
      [{var, k},{kar, x}| tail]
    else
      [{kar, x}| insert({var, k}, tail)]
    end
  end

  def freq(sample) do freq(sample, []) end

  def freq([], freq) do freq end

  def freq([char | rest], freq) do freq(rest, upd(char, freq)) end


  def upd(char, []) do [{char, 1}] end
  def upd(char, [{char,  k}|rest]) do [{char, (k+1)}|rest] end
  def upd(char, [elemt | rest]) do [elemt| upd(char, rest)] end


  def encode_table(code, {c1, c2}) do 
    as = encode_table(code ++[0], c1)
    bs = encode_table(code ++[1], c2)
    as ++ bs
  end

  def encode_table({c1, c2}) do
    as = encode_table([0], c1)
    bs = encode_table([1], c2)
    as ++ bs
  end

  def encode_table(code,char) do
    [{char, code}]
  end

  def decode_table(tree) do
   encode_table(tree) 

  end

  def encode([], table) do [] end
  def encode([char|rest], table) do
    {_, code} = List.keyfind(table, char, 0)
    code ++ encode(rest, table)
  end
  def decode([], _) do
    []
  end

  def decode(seq, table) do
    {char, rest} = decode_char(seq, 1, table)
    [char | decode(rest, table)]
  end

  def decode_char(seq, n, table) do
    {code, rest} = Enum.split(seq, n)

    case List.keyfind(table, code, 1) do
      nil -> 
        IO.puts("#{n}th atempt!")
        decode_char(seq, n+1, table)
      {char, _} -> 
       {char, rest} 

    end
  end
end
