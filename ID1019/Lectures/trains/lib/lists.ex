defmodule Lists do 

  def take([],_)do [] end
  def take(_, 0) do [] end
  def take([h|t], n) when n>0 do
    [h] ++ take(t,n-1)
  end

  def drop([], _)do [] end
  def drop(xs, 0)do xs end

  def drop([_|t], n)when n>0 do 
    drop(t,n-1)
  end

  def append([],yt) do yt end
  def append(xs,[]) do xs end

  def append(xs, [yh|yt]) do 
    append([yh|xs], yt)
  end

  def member([],_), do: false
  def member([y|t], y), do: true
  def member([h|t], y), do: member(t,y)


  def postition([], _), do: :nil
  def postition([], _, _), do: :nil
  def postition([y|t], y), do: 1
  def postition([y|_], y, n), do: n
  def postition([h|t], y), do: postition(t,y,2)
  def postition([h|t], y, n), do: postition(t, y, n+1)


end
