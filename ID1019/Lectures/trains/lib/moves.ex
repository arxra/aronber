defmodule Moves do
  # n for numberr of changes, m=middle, o=lane one, t=lane two
  def single({:one,n}, {m,o,t}) do 
    if(n>0) do
      sinm(m,n,o)
    else
      sinm(o,n,m)
    end
  end

  def single({:two,n}, {m,o,t}) do 
    if(n>0) do
      k= sinm(m,n,t)
    else
      k= sinm(t,n,m)
    end
  end

  def sinm(a,b,c)do
    if (b<0) do 
      Lists.append(Lists.take(a,-b), c)
      Lists.drop(a,-b) 
    else      
      Lists.append(Lists.take(a,b), c)
      Lists.drop(a,b) 
    end
  end
end
