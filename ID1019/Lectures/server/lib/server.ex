defmodule Server do
  @moduledoc """
  Documentation for Server module.
  """

  @doc """
  Lecture and practise in Elixir, here we implement a basic webserver.

  """
  def parse(uri) do
    HTTP.parse_request(uri)
  end 

  def start(port) do
    Process.register(spawn(fn -> Rudy.init(port) end), :rudy)
  end

  def stop() do
    Process.exit(Process.whereis(:rudy), "Time to die!")
  end

end
