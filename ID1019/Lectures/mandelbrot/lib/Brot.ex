defmodule Brot do
   @spec test(integer, integer, integer, integer) :: integer
   @doc """
   i =
   z = Inittial complex number
   c = Start value
   m = Maximum iteration depth
   """

   def test(m, _, _, m) do 0 end
   def test(i, z, c, m) do
      if Cmplx.abs(z) < 2 do
         z1= Cmplx.add(Cmplx.sqr(z), c)
         test(i+1, z1, c, m)
      else i end
   end

   def mandelbrot(c,m) do test(0,Cmplx.new(0,0),c,m) end
end
