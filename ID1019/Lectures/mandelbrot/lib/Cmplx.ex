defmodule Cmplx do
   def new(r, i) do {r,i} end
   def add({a1, a2},{b1,b2}) do {a1+b1, a2+b2} end
   def sqr({a,b}) do {a*a-b*b, 2*a*b} end
   def sqrt({a,b}) do {:math.sqrt(a), :math.sqrt(b)} end
   def abs({a,b}) do :math.sqrt((a*a)+(b*b)) end
end
