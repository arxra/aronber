defmodule Colour do 

  def convert(depth, max) do
    red(depth, max )
  end

  def red(d, m) do 
    f = d / m

    # a is [0 - 4.0]
    a = f * 4

    # x is [0,1,2,3,4]
    x = trunc(a)

    # y is [0 - 255]
    y = trunc(255 * (a - x))
  
    case x do 
      0 ->
        {:rgb, x, 0, 0}
      1 ->
        {:rgb, 0, x, 0}
      2->
        {:rgb, 0, 0, 0}
      3 ->
        {:rgb, 255, 255, 255}
      4 ->
        {:rgb, 0, 0, 0}
  end 
end
