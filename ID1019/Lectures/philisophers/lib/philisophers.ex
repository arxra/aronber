defmodule Philisophers do
  @moduledoc """
  Documentation for Philisophers.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Philisophers.hello
      :world

  """
  def hello do
    :world
  end
end
