defmodule Chopstick do

  def start() do
    stick = spawn_link(fn -> init()end)
    {:stick, stick}
  end

  def quit(pid) do
    send(pid, :quit)
  end

  defp init() do
    available()
  end

  def request({:stick, pid}) do
    send(pid, {:request, self()})
    receive do
      :given -> :ok
    end
  end

  def request({:stick,pid}, timeout) do
    send(pid, {:request, self()})
    receive do
      :given ->:ok
    after timeout ->
      IO.puts("Returned a timedout stick")
      return({:stick, pid})
      :no

    end
  end

  def return({:stick, pid}) do
    send(pid, :return)
  end

  defp available() do
    receive do
      {:request, from} ->
        send(from, :given)
        gone()
      :quit -> :ok
    end
  end

  defp gone() do
    receive do
      :return -> available()
      :quit -> :ok
    end
  end
end
