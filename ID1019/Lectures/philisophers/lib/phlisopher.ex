defmodule Philosopher do
  @sleepy 250 
  @slow 1000

  def start(portions, right, left, name, ctrl) do
    IO.puts("#{name} is alive!")
    spawn_link(fn -> dream(portions, right, left, name, ctrl)end)
  end

  def init(portions, right, left, name, ctrl) do
    sleep(@sleepy)
    dream(portions, right, left, name, ctrl)
  end

  def sleep(t) do
    :timer.sleep(:rand.uniform(t))
  end

  defp dream(portions, right, left, name, ctrl) do
    sleep(@sleepy)
    eat(portions, right, left, name, ctrl)
  end

  def eat(0, _, _, _,ctrl) do
    send(ctrl, :done)
  end

  def eat2(portions, right, left, name, ctrl) do
    case Chopstick.request(right, left) do

    end
  
  end
  def eat(portions, right, left, name, ctrl) do
    case Chopstick.request(right, 20) do
      :ok ->
        sleep(@slow)
        case Chopstick.request(left, 20) do
          :ok ->
            #IO.puts("#{name} is eating")
            Chopstick.return( right)
            Chopstick.return( left)
            #IO.puts("#{name} returned h(is/er) sticks!")
            dream(portions-1, right, left, name, ctrl)
          :no ->
            Chopstick.return(right)
            dream(portions, right, left, name, ctrl)
        end
      :no ->
        dream(portions, right, left, name, ctrl)
    end
  end
end
