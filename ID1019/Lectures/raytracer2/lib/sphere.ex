defmodule Sphere do
  defstruct pos: {0, 0, 0}, radius: 2
end

defimpl Objectt do 
  def intersect(sphere = %Sphere{}, ray = %Ray{}) do 
    for r <- ray, s <- sphere do 
      k = Vector.sub(r.pos, s.pos))
      a = Vector.dot(k, s.pos)
      h = :math.sqrt(k*k-a*a)
      t2 = s.radius * s.radius - h*h
      if (t2 > 0) do
        t = :math.sqrt(t2)
        l = Vector.smul(r.dir, a)
        if(l+t < l-t && l+t > 0) do 
          l+t
        else if (l-t > 0) do 
          l-t
        else 
          :no
        end
      else 
        :no
      end
    end
  end
end

