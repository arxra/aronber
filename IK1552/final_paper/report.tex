\documentclass[10pt]{article}
\usepackage{listings}
\usepackage{geometry}
\usepackage{float}
\usepackage{tikz}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{fancyhdr}
\setlength\parindent{0pt}

\usepackage[backend=biber,style=ieee]{biblatex}
\addbibresource{lib.bib}

\title{%
    SCTP: Where? \\
}
\author{%
    Aron Hansen Berggren\\
    aronber@kth.se\\
    076 796 05 11
}
\begin{document}

\maketitle
\newpage

\section*{Abstract}
The now old protocol SCTP promised to merge the perks from both UDP and TCP while learning
from their mistakes. It does this well, it just missed the main point of any network protocol:
Performance. As it uses the congestion algorithms as TCP its speed never outgrew TCP in the 
general case and seems to have caused its lack of interest. During the research phase, a series 
on the popular platform YouTube caught the eye on how to implement TCP in a modern system 
level language, Rust. This led to some interesting findings in the complexity of network
protocols, and lead to another conclusion: SCTPs complexity may have hindered its 
probability of widespread adoption. Now, it seems a newer protocol QUIC might take on the 
challenges SCTP could never handle, replacing the age-old TCP protocol for standard network
communication. SCTP does still however see use for the area it was designed, as a tool for 
reliable realtime message delivery in telephony networks.

\section{Introduction}
With emerging technologies such as 5G and home fiber installations with 
speed towards 10Gps\cite{noauthor_prisvard_nodate},
the protocols run on these remain from the 1980's\cite{postel_transmission_nodate}.
New protocols exist in different forms, and such as Googles QUIC\cite{roskind_quic_nodate}
which aims to reduce latency and congestion on the web and is seeing some traction and might
finally become a mainstream TCP replacement in HTTP$\backslash$3\cite{de_biasio_quic_2019}.
However,there's almost 40 years since TCP entered the scene, and QUIC is not really live yet.
Half way in between, we have SCTP\footnote{Stream Control Transmission Protocol}, being
defined in RFC2960\cite{taylor_stream_nodate} in 2000 which was obsoleted in 2007
by RFC4960.\cite{stewart_rrscisco.com_stream_nodate} SCTP mainly now sees use in MANETs
\footnote{Mobile Ad Hoc Network} for moving messages around, but why did it stop there?

\subsection{Problem Definition}
So what caused SCTP to not gain any traction? It promises to solve most of the shortcomings
of both TCP and UDP, which it does on paper. So did it succeed in being a worthy successor,
or was there something not quite right with it? And what are these extra features?

\subsection{Purpose}
The aim of this paper is to investigate what happened to SCTP and see why it never got any
major traction. As the investigation went on, the topic of how to actually 
implement a network protocol arose. This was done to assist in the understand of how a 
protocol would be implemented and the methods used in case one was to do so themselves,
as well as to remove the layer of magic surrounding the protocols making the internet work.

\subsection{Research methodology}
One of the most interesting points of network protocols is how to actually sit down and 
implement one, as it is impossible to say you know how one works if you don't. As such,
the first step of this paper was to start following a series on how to implement one of 
the protocols SCTP aims to replace and see how it works, in this case Jon Gjengset
excellent streams on implementing TCP in a never systems language Rust.
\cite{jon_gjengset_implementing_nodate} The aim of this
part of the research was threefold, one to get a deeper understanding on how one would 
follow a RFC in order to understand a standard for a networking protocol. Second,
how TCP actually works as he goes through debugging TCP connections, implementation
details and the specifics of TCP. Third, the language Rust is appealing from a 
networking perspective as the language guarantees a lot of features like zero-cost
abstractions, safe multi-threading and precise memory control which were previously 
only found in C/C++.\\

Further, the invaluable tool Google scholar was used to search for articles, 
and the KTH Online Library was used to access the reports and papers.
The corresponding RFCs was also used to refer to the protocols.


\section{Background}
There are many parts as to why some use SCTP, and while most applications don't. Here,
background on some of those reasons are explained.

\subsection{Features (compared to TCP and UDP)}
As eluded to in the Introduction, SCTP was written as a way to get away from the
shortcomings of TCP and UDP. But what are those shortcomings?

\subsubsection{Reliability}
One of the most important parts of TCP that SCTP still offers is reliability.
This is something most applications hope for when trying to send data over 
a network, and something UDP lacks. It is here we find one of the major improvements over
TCP in that SCTP is able to ACK some packages while saying it is missing some of them,
know better as SCTP has eliminated the head-of-line-blocking that TCP suffers from.
This does in theory give SCTP a performance gain on high latency networks, such as MANETs.

\subsubsection{Sessions and response time}
As with TCP, SCTP is session based, meaning a connection must be established 
before data can be sent over the link. This is something that can be argued for and 
against depending on the application, however, most applications prefer sessions 
over shorter response times. The pro-point in response goes to UDP, as the first message
can carry all the data. In TCP, data can only be sent after the initialization is complete.
In SCTP, the data is somewhere in between: it uses a 4-way handshake where the 2 last messages
both are able to carry data.

\subsubsection{Data delivery}
Both UDP and SCTP(by default) are message based protocols,
meaning they package everything into messages 
that can be sent and delivered by some module. TCP is stream based, meaning you can hand it 
any stream of data, and it will deliver it as soon as it can. TCP does this in order, so if
any packet gets lost on the way, all others sent after it will not be counted as delivered 
until the lost packet has been retransmitted and acknowledged.\\

However, SCTP further has a multi-streaming feature. Here, similar to TCP, SCTP is able to 
deliver a stream of bytes but is able to differentiate different streams over a single 
connection. This makes SCTP suitable for connections where multiple different streams are 
present, such as loading web pages that depends on multiple graphic file transfers, in theory.
\cite{ong_introduction_2002}

\subsubsection{Congestion Control}
UDP does simply not care what is on the network, it will try to deliver packets at the speed 
sent from the client/host. This causes major congestion for large files, but has no additional
latency since no checks are performed.\\

Both TCP and SCTP come with similar algorithms, in fact SCTPs congestion control definition is
the same RFC as that for TCP in part 7 of its RFC: RFC2581\cite{stevens_tcp_nodate}.\label{alg:ccalgo}

\subsection{What is new at the table}

\subsubsection{multi-homing}
Unique to SCTP is multi homing, the ability to have redundant available data paths for each
connection. The purpose of this is to make sure packages always get through as soon as possible,
in case of failure on a link or network. However, it does not support multiple paths sharing 
the load of a single connection as a path is selected as the main path.\\

This is however far behind what is possible in the realm of multi homing. See ASPERA's FASP for 
example, which was investigated in~\citetitle{berggren_interesting_nodate}. Here, 
multi-homing is not only used per connection, but for a FASP network in order to control 
congestion more efficiently.

\subsubsection{Cookies}
Since UDP is stateless, it avoids the problems  associated with connection-based protocols.
For TCP, it is a known problem that a attacker kan flood a host with partially established
connections,
rendering the host unresponsive. Cookies however, are cheap to manufacture in processor 
cycles and 
space, and are the mechanism SCTP use to avoid this problem. As part of the 4-way initial
handshake SCTP generates a cookie based on some key and sends the hash of it to the client.
Only when the cookie has been acknowledged by the client to the server does the connection
complete, and this can only occur if the client is exploited and will not work if the client
is impersonated. This greatly weakens the attack-point for SCTP based handshake 
exploitations.

\subsection{Performance}\label{chpt:backperf}
When comparing SCTP to TCP over the intended use case of sending data in a MANET where nodes are moving
no actual gains over TCP are seen in simulations, it in fact performs slower.\cite{kumar_sctp_2004}\\

Not only there, but in general SCTP is slightly worse performant than TCP as the congestion algorithms
are the same(\ref{alg:ccalgo}) according to some\cite{bandaru_performance_2011}, and better than TCP
by a large margin in other cases such as when the loss rate goes up\cite{bickhart_transparent_nodate}.
There does not seem to be any concrete proof for the difference between these, as simulations of 
different kinds give different results.


\subsection{Uses today}
SCTP sees main use is what it was designed for, as the main protocol employed by SS7, the protocol 
suite defined by SIGTRAN to carry telephony signals and messages over medium such as IP.
The multi homing abilities,reliability and multi-streaming are key features that make the protocol 
suitable for this use, as head-of-line blocking is unacceptable.\cite{immonen_sigtran_2005}\\


It also sees use as a general purpose network transfer protocol for WebRTC which have chosen SCTP for 
messages and non-video/audio transfers. Being able to use congestion control for messages without 
head-of-line blocking makes messages come through if even if the previous got lost, and the client
does not need to tell the host the message was lost due to the configurability of SCTP.
The congestion control is also necessary for the video and audio feeds to reach the destination in 
real time, and as such TCP could not be used.
\cite{noauthor_why_2014}\cite{jesup_webrtc_nodate}\


\subsection{Implementation}
As part of this paper it was also investigated what is required in order to implement and debug network 
protocols. This was investigated with the help of
Jon Gjengset's streams on TCP. After following the first streaming session of 5.5
hours we already had a TCP socket able to complete the 3-way handshake and to cleanly close connections.
There were two parts to this task, first off being not knowing the programing language used and second
being the non-trivial part of implementing the protocol itself.\\

% TODO how to implement TCP
% TODO other sources than RFC793, for eample "common tcp problems"
The baseline for the implementation was following RFC793, which has further been updated by 4 more RFCs.
The setcap Linux utility was used to give the protocol a virtual network interface.
To check how it was doing in terms of TCP implementation stages,
a combination of the terminal based application tcpdump and
the GUI\footnote{Graphical User Interface} version of Wireshark were used to see which message was 
responding to which and what errors were occurring, as Wireshark clearly marks which packets have issues
with the values in their headers. Further, RFC 2525 on known TCP implementation Problems were consulted
if problems arose where RFC793 fell short.\\

Implementing SCTP sounds like a daunting task as there are not many applications running the protocol 
readily available compared to TCP,
but you can however map other applications to bind their system calls to SCTP 
instead of TCP through a transparent layer as shown
by \citeauthor{bickhart_transparent_nodate} in \citetitle{bickhart_transparent_nodate}.
They also conclude that SCTP could see much more use if more used their shim-kernel layer, as well
as major gain in transfer speeds when the loss-rate goes up in addition to all other features provided
by SCTP.


\section{Analysis}
\subsection{Features}
When it comes to features, SCTP is about as big of a step over TCP as TCP is over UDP.
It is very apparent the features have a certain goal: to maximise redundancy over MANETs.
It does so well with the ability to fall over to other routes if the main connection is broken.
This multi-homing is one of the features which makes SCTP appealing, but its limitation to only offer
fallback instead of splitting a connection over multiple streams is certainly a problem.
Multi-homing could have been used to increase bandwidth when the current link is saturated. This would
have enabled higher transferrates in cases where multiple paths are available, such as in networks 
with many interconnected nodes, see FASPs Congestion Control for a good example on how such a network 
might be managed. However, this depends on there being more nodes available, something only reliably
occurring on a application level such as FASP.\\


\subsection{Implementation}\label{chpt:implan}
Implementing TCP would be a daunting task for a single person to complete in a week, as the entire 
TCP stack with congestion, managing multiple connections per port and programing in raw sockets for 
different operating systems would require more time than feasible for a student, just implementing 
the first steps and setting up a environment where you can test makes you at least confident in what
you are researching. This showed that network protocols are more than a few rules of 
"do this and this", and removed the layer of magic in between the topic and the student.
Just setting up virtual sockets and using them for testing was a very helpful exercise, and something
that will probably some to use later for other testing purposes. Further, the complexity of the 
security regarding protocols were made apparent as every time the topic of security arose during 
the implementation the response was always "This is too advanced for this series", and that's
coming from someone who is clearly capable of implementing a entire network stack. 
Other problems were looked into in RFC2525 on common implementation problems. However,
in each case where that RFC was consulted the answer was so far always found in the original RFC
under a different section on the same mechanism.\\

The choice of the language Rust seems to be appropiate for the task of handling networking, as its
main features are performance (sitting in between c and c++ for general tasks), memory safety
(checked at compile time, less ingress points for attackers), thread safety 
(useful for handling multiple gigabit connections) and has been built from the ground with 
C/C++ integration in mind, meaning it should be trivial to include for other programs.
They even talk about this on their main page, https://www.rust-lang.org/what/networking.\\

Implementing networking protocols is a good way to get a deeper understanding of many different
areas, such as kernel/user spaces, programing languages, network protocols and network interfaces.
How to handle all the different parts of this chain upp to the application level is the main 
take-away here. If you do not understand this entire stack, how are you ever going to be able
to analyse new solutions, old solutions or start researching your own solutions?

\subsection{Performance}
As seen in \ref{chpt:backperf} the performance of SCTP seems to be unclear. On one hand, we have
simulations of MANETs where TCP shows to be stronger, and on the other we have lab with a 
intermediate node to simulate network conditions showing a strong gain for SCTP when the loss
rate goes up. The second case makes more sense looking at how TCP have head-of-line blocking 
while SCTP does not as it supports individual chunk ACKs. This is one of the problems SCTP tried 
to eliminate, and it looks like it has done so in the case of packet loss. It is this lack of 
head-of-line blocking that makes SCTP more appealing towards realtime applications, see 
WebRTC.


\section{Further work}
\subsection{Multi-homing split connections}
The ability to use multi-homing is very useful in cases where it can provide redundancy for the 
telecom operators, but for organizations the largest gains would be if these underutilized 
connections could be used to split the load for the connections. If the congestion control 
mechanism instead of sending less traffic started to send traffic not acknowledged over a second
link, would this be able to increase throughput over TCP? This topic is certainly interesting,
but does require a modifiable SCTP codebase, which requires a lot of work(see \ref{chpt:implan}).

\subsection{Congestion Control}
Similar to TCP, congestion control is one of the main reasons for its poor performance over 
"good" protocols such as FASP. How would TCP and more interestingly SCTP perform with regards 
to different congestion control algorithms such as Googles BBR? Or with addition of using 
algorithms to sense if the packets delay instead of if they start to drop? And how would 
these changes play with multi-homing split connections in the previous section?

\subsection{Performance}
As seen in~\ref{chpt:backperf} there does not seem to be a common opinion on which performs the 
best under which circumstances and this should be investigated deeper.
   


\section{Conclusion}
SCTP seems to have been missed by the industry for one major reason: the poor algorithms used 
for congestion control. As the performance is similar to that of TCP, there was never any gain from 
spending valuable developer hours at implementing SCTP for mainstream operating systems and
applications. The additional features offered are not as appealing as a simpler stack with,
in the general use cases, better performance as performance is key to network protocols
(and reliability, which TCP already provided). The features only appeal to a certain market, mainly 
real-time and multi-homing dependant applications as those found here while real-time is 
only found in UDP and not in TCP, while multi-homing is a small percentage of use cases which gain 
from utilizing. The only group which benefit greatly and is not included are mobile device 
connections, these would greatly gain from the handover that is between their WIFI adress
and their wireless 4/5G connections. However, they seem to have ended up extending TCP to handle
the specific use cases instead of SCTP due to the complexity of the SCTP stack.\\





\printbibliography
\end{document}
