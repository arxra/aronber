use std::io;

enum State {
    //Closed,
    //Listen,
    SynRecv,
    Estab,
    FinWait1,
    FinWait2,
    Closing,
    Timewait,
}

impl State {
    fn is_synch(&self) -> bool {
        match *self {
            State::SynRecv => false,
            State::Estab => true,
            State::FinWait1 => true,
            State::FinWait2 => true,
            State::Closing => true,
            State::Timewait => true,
        }
    }
}

pub struct Connection {
    state: State,
    sss: SendSequenceSpace,
    rss: RecvSequenceSpace,
    ip: etherparse::Ipv4Header,
    tcph: etherparse::TcpHeader,
}

struct SendSequenceSpace {
    /// send unacknowledged
    una: u32,
    /// send next
    nxt: u32,
    /// send window
    wnd: u16,
    /// segment sequence number used for last windows update
    wl1: usize,
    /// segment acknowledgment number used for last window update
    wl2: usize,
    /// Initial Send Sequence number
    iss: u32,
    /// send urgent pointer
    up: bool,
}

struct RecvSequenceSpace {
    /// Recieve next
    next: u32,
    /// Recieve window
    wnd: u16,
    /// Recieve urgent pointer
    up: bool,
    /// Initial recieve sequence number
    irs: u32,
}

impl Connection {
    pub fn accept<'a>(
        nic: &mut tun_tap::Iface,
        iph: etherparse::Ipv4HeaderSlice<'a>,
        tcph: etherparse::TcpHeaderSlice<'a>,
        data: &'a [u8],
    ) -> io::Result<Option<Self>> {
        if !tcph.syn() {
            //only expected SYN packet
            return Ok(None);
        }

        let iss = 0;
        let wnd = 10;
        let mut c = Connection {
            state: State::SynRecv,
            sss: SendSequenceSpace {
                iss,
                una: iss,
                nxt: iss + 1,
                wnd: wnd,
                up: false,
                wl1: 0,
                wl2: 0,
            },
            rss: RecvSequenceSpace {
                wnd: tcph.window_size(),
                next: tcph.sequence_number() + 1,
                up: false,
                irs: tcph.sequence_number(),
            },
            ip: etherparse::Ipv4Header::new(
                0,
                64,
                etherparse::IpTrafficClass::Tcp,
                [
                    iph.destination()[0],
                    iph.destination()[1],
                    iph.destination()[2],
                    iph.destination()[3],
                ],
                [
                    iph.source()[0],
                    iph.source()[1],
                    iph.source()[2],
                    iph.source()[3],
                ],
            ),
            tcph: etherparse::TcpHeader::new(tcph.destination_port(), tcph.source_port(), iss, wnd),
        };

        //Time to esatablish connection
        c.tcph.syn = true;
        c.tcph.ack = true;
        c.write(nic, &[])?;

        Ok(Some(c))
    }

    fn write(&mut self, nic: &mut tun_tap::Iface, payload: &[u8]) -> io::Result<usize> {
        let mut buf = [0u8; 1500];
        self.tcph.sequence_number = self.sss.nxt;
        self.tcph.acknowledgment_number = self.rss.next;

        let size: usize = std::cmp::min(
            buf.len(),
            self.tcph.header_len() as usize + self.ip.header_len() + payload.len(),
        );
        self.ip.set_payload_len(size - self.ip.header_len());

        // Calculate checksum
        self.tcph.checksum = self
            .tcph
            .calc_checksum_ipv4(&self.ip, &[])
            .expect("Failed to compute checksum");

        // write out the headers.
        use std::io::Write;
        let mut unwritten = &mut buf[..];
        self.ip.write(&mut unwritten);
        self.tcph.write(&mut unwritten);
        let payload_bytes = unwritten.write(payload)?;
        let unwritten = unwritten.len();

        // set next seq nr
        self.sss.nxt = self.sss.nxt.wrapping_add(1);
        if self.tcph.syn {
            self.sss.nxt = self.sss.nxt.wrapping_add(1);
            self.tcph.syn = false;
        }
        if self.tcph.fin {
            self.sss.nxt = self.sss.nxt.wrapping_add(1);
            self.tcph.fin = false;
        }

        // send packet
        nic.send(&buf[..buf.len() - unwritten])?;
        Ok(payload_bytes)
    }

    pub fn send_rst(&mut self, nic: &mut tun_tap::Iface) -> io::Result<()> {
        // TODO fix seq nr here
        // TODO Handle synchronized reset
        self.tcph.rst = true;
        self.tcph.sequence_number = 0;
        self.tcph.acknowledgment_number = 0;
        self.write(nic, &[])?;
        Ok(())
    }

    pub fn on_packet<'a>(
        &mut self,
        nic: &mut tun_tap::Iface,
        iph: etherparse::Ipv4HeaderSlice<'a>,
        tcph: etherparse::TcpHeaderSlice<'a>,
        data: &'a [u8],
    ) -> io::Result<()> {
        eprintln!("Got package");
        // first check that seq nr are valid (RFC 793 s3.3)
        // acceptable ack check
        // snd.una < seg.ack =< snd.nxt
        //
        let ackn = tcph.acknowledgment_number();
        let wend = self.rss.next.wrapping_add(self.rss.wnd as u32);
        if !is_between_wrapped(self.sss.una, ackn, self.sss.nxt.wrapping_add(1)) {
            return Ok(());
        }
        self.sss.una = ackn;

        let mut slen = data.len() as u32;
        if tcph.fin() {
            slen += 1;
        };
        if tcph.syn() {
            slen += 1;
        };

        //enter if not wrapping valid segement check
        let seqn = tcph.sequence_number();
        if slen == 0 {
            // zero-lentgth segment has separat rules for Ok
            if self.rss.wnd == 0 {
                if seqn != self.rss.next {
                    false;
                } else if !is_between_wrapped(self.rss.next.wrapping_sub(1), seqn, wend) {
                    false;
                } else {
                    true;
                }
            }
        } else {
            if self.rss.wnd == 0 {
                false;
            } else if !is_between_wrapped(self.rss.next.wrapping_sub(1), seqn, wend)
                && !is_between_wrapped(
                    self.rss.next.wrapping_sub(1),
                    seqn.wrapping_add(slen - 1),
                    wend,
                )
            {
                false;
            } else {
                true;
            }
        };

        self.rss.next = seqn.wrapping_add(slen);
        // TODO if _not__acceptable, send ACK.

        if !tcph.ack() {
            return Ok(());
        }

        if let State::SynRecv = self.state {
            if is_between_wrapped(
                self.sss.una.wrapping_sub(1),
                ackn,
                self.sss.nxt.wrapping_add(1),
            ) {
                // must have ACKed our SYN, since detected at least one acked byte == how many we
                // have sent at this point.
                // The only one byte we have sent is now acked, annd the connection is complete.
                self.state = State::Estab;
            } else {
                // TODO RST
            }
        }

        if let State::Estab | State::FinWait1 | State::FinWait2 = self.state {
            if !is_between_wrapped(self.sss.una, ackn, self.sss.nxt.wrapping_add(1)) {
                return Ok(());
            }
            self.sss.una = ackn;
            // TODO bunch o fstuff
            assert!(data.is_empty());
            //now lets terminate connection
            //TODO Needs to be stored in the retransmissions queue
            //Otherwise, we might lose data if we are not done sending not acked data.
            //
            if let State::Estab = self.state {
                self.tcph.fin = true;
                self.write(nic, &[]);
                self.state = State::FinWait1;
            }
        }

        if let State::FinWait1 = self.state {
            if self.sss.una == self.sss.iss + 2 {
                self.state = State::FinWait2;
                // our FIN has ben acked.
            }
        }

        if tcph.fin() {
            match self.state {
                State::FinWait2 => {
                    //Done with connection
                    self.write(nic, &[]);
                    self.state = State::Timewait;
                }
                _ => unreachable!(),
            }
        }

        if let State::FinWait2 = self.state {
            if !tcph.fin() || !data.is_empty() {
                unimplemented!();
            }

            // must have ACKed our FIN, since detected at least one acked byte == how many we
            // sent in the FIN packet.
            self.tcph.fin = false;
            self.write(nic, &[])?;
            self.state = State::FinWait1;
        }
        Ok(())
    }
}

/// Check if the sequence_number is valid.
/// start   : unacknowledged
/// x       : ack
/// nxt#    : end
fn is_between_wrapped(start: u32, x: u32, end: u32) -> bool {
    use std::cmp::Ordering;
    match start.cmp(&x) {
        Ordering::Equal => return false,
        Ordering::Less => {
            // we have wraparound :
            //  |------s------x--------------|
            //
            // We want to accept if x, our # to ack is
            // in one of theses cases:
            //
            //  |------s------x-e------------|
            //  |----e-s------x--------------|
            //
            //  but *not* here:
            //
            //  |------s--e---x--------------|
            //  |------|------x--------------|
            //
            // or, in other words, iff !(s <= e <= x )
            if end >= start && end <= x {
                return false;
            }
        }
        Ordering::Greater => {
            // we have the opposite of above:
            //  |------x------s--------------|
            //
            // We want to accept if x, our # to ack is
            // in one of this case:
            //
            //  |------x---e--s--------------|
            //
            //  but *not* here:
            //
            //  |------s--e---x--------------|
            //  |------|------x--------------|
            //
            // or, in other words, iff s < e < x
            if end < start && x < end {
            } else {
                return false;
            }
        }
    }
    return true;
}
