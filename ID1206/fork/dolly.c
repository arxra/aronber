#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main() {

  int pid;
  int x = 123;
  pid = fork();

  if (pid == 0) {
    printf("child: x is %d\n", x);
    sleep(1);
    printf("child: x is %d\n", x);
  } else {
    printf("mother: x is %d\n", x);
    x = 13;
    printf("mother: x is %d\n", x);
    wait(NULL);
  }

  return 0;
}
