#include <sys/mman.h>
#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>


#define MIN 5
#define LEVELS 8
#define PAGE 4096
#define TESTCYCLE 1000000
#define REALLOC 15

int pages = 0;

enum flag {Free, Taken};

struct head {
    enum flag status;
    short int level;
    struct head *next;
    struct head *prev;
};

struct head *freeBlocks[LEVELS]={NULL};

struct head *new(){
    struct head *new = (struct head *) mmap(
            NULL, PAGE, PROT_READ| PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0
            );
    if(new==MAP_FAILED){
        return NULL;
    }

    assert(((long int)new & 0xfff) ==0); //last 12 bits should be 0
    new->status = Free;
    new->level = LEVELS -1;
    pages++;
    return new;
}

int pagesinuse(){
    return pages;
}

void printFreeStructure(){
    struct head *tmp;
    for (int i =0; i< LEVELS;i++){
        tmp = freeBlocks[i];
        fprintf(stderr,"Level: %d\t",i);
        while(tmp != NULL){
            fprintf(stderr, "\t%p", tmp);
            tmp = tmp->next;
        }
        fprintf(stderr, "\n");
    }
}

int smallrandom(){
    return (rand()% (PAGE - 96 + MIN)) + MIN;
}

struct head *buddy (struct head* block){
    int index = block->level;
    long int mask = 0x1 << (index + MIN);
    return (struct head*)((long int)block^mask);
}

struct head *split(struct head *block){
    int index = block->level-1;
    int mask = 0x1<<(index+MIN);
    //The new adress of the *head
    struct head* split = (struct head*)((long int)block|mask);
    //Set index for new head and it's buddy
    split->level = index;
    split->next = buddy(split);
    split->prev = NULL;

    //also set the buddy's level
    split->next->level=index;
    split->next->next = NULL;
    split->next->status=Free;
    fprintf(stderr, "Split: %p\tBuddy: %p\n", split, split->next);
    return split;
}

struct head *primary(struct head* block){
    int index = block->level;
    long int mask = 0xffffffffffffffff << (1+index + MIN);
    return (struct head*)((long int)block&mask);
}

void *hide(struct head* block){
    return (void*)(block+1);
}

struct head *magic(void *memory){
    return ((struct head*) memory -1);
}

int level(int req){
    int total = req + sizeof(struct head);

    int i = 0;
    int size = 1 <<MIN;
    while(total > size){
        size <<=1;
        i++;
    }
    return i;
}

struct head *find(int index){
    struct head *response = NULL;
    int ind = index;


    while(ind < LEVELS){
        if(freeBlocks[ind]== NULL){
            if(ind == LEVELS-1){
                freeBlocks[LEVELS-1] = new();
            } else 
                ind++;
        } else {
            if(index == ind){
                //fprintf(stderr, "index == ind\n");
                response = freeBlocks[ind];
                //fprintf(stderr, "Response: %p\n", response);
                freeBlocks[ind] = response->next;
                //fprintf(stderr, "Next in freeblocks asigned: %p\n", freeBlocks[ind]);
                if(freeBlocks[ind] !=NULL)
                    freeBlocks[ind]->prev = NULL;
                return response;
            }else{
                fprintf(stderr, "Time to split! \n");
                response = freeBlocks[ind]->next;
                fprintf(stderr,"Response:%p\nNext should be empty: %p", response, freeBlocks[ind-1]);
                freeBlocks[ind-1] = split(freeBlocks[ind]);
                fprintf(stderr,"freeblock-1: %p\n", freeBlocks[ind-1]);
                fprintf(stderr,"This block should have a next: %p\nButt that's next is null: %p\n", 
                        freeBlocks[ind-1]->next, freeBlocks[ind-1]->next->next);
                freeBlocks[ind] = response;
                response = NULL;

                // go down one layer in the loop
                ind--;
            }
        }
    }

    return NULL;
}

void *balloc(size_t size){
    if(size ==0){
        return NULL;
    }
    int clevel = level(size);
    //fprintf(stderr, "Block of size %d requested, providing one of level: %d\n", (int)size, clevel);
    struct head *taken = find(clevel);
    //printf("Returning block of level %d to a requested size: %d\n", taken->level, (int)size);
    printFreeStructure();
    taken->status=Taken;
    return hide(taken);
}

void insmax(struct head * prim){
    if(freeBlocks[LEVELS-1] == NULL){
        prim->next = NULL;
        prim->prev=NULL;
        prim->status = Free;
        freeBlocks[LEVELS-1] = prim;
        return;
    }

    struct head *tmp = NULL;

    if(freeBlocks[LEVELS-1] != NULL){
        *tmp = *freeBlocks[LEVELS-1];

        int i = 0;
        while(tmp->next != NULL){
            tmp = tmp->next;
            i++;
        }
        fprintf(stderr, "i: %d\n", i);
        if (i> 2){
            munmap(prim, PAGE);
            pages--;
            return;
        }

        tmp->next = prim;
    }
    prim->prev = tmp;
    prim->status = Free;

}

void insert(struct head *block){
    printFreeStructure();
    struct head *tmp;
    if(block->level == LEVELS-1){
        insmax(block);
        return;
    }

    if(freeBlocks[block->level] == NULL){
        fprintf(stderr, "available sapce for me!\n");
        block->status=Free;
        block->next = NULL;
        block->prev = NULL;
        return;}

    if(buddy(block)->status == Taken){
        fprintf(stderr, "buddy was taken :(\n");
        tmp = freeBlocks[block->level];
        while(tmp->next != NULL)
            tmp = tmp->next;
        tmp->next = block;
        block->next=NULL;
        block->prev = tmp;
        block->status=Free;
    } else {
        fprintf(stderr, "Buddy available on level: %d\n", block->level);
        tmp= freeBlocks[block->level];

        fprintf(stderr,"Expected neighbor: %p\n" ,buddy(block));
        while(tmp != buddy(block))
            tmp = tmp->next;

        fprintf(stderr, "tmp:%p \tblock:%p", tmp, block);

        if(freeBlocks[block->level] != tmp)
            tmp->prev->next = tmp->next;
        if(tmp->next != NULL)
            tmp->next->prev = tmp->prev;
        if(freeBlocks[block->level] == tmp)
            freeBlocks[block->level]= tmp->next;

        struct head *prim = primary(block);
        memset(hide(prim), 0, (1<<(block->level +MIN - 3)));

        prim->level++;
        prim->status=Free;

        if(prim->level != LEVELS-1){
            insert(prim);
            return;
        }else{
            insmax(prim);
            return;
        }
    }
}

void bfree(void *memory){

    fprintf(stderr, "Time to clear a refrence: %p\n", memory);
    if(memory !=NULL){
        struct head *block = magic(memory);
        fprintf(stderr, "And now ref is: %p\n", block);
        insert(block);
    }

    return;
}

void dotedline(){
    printf("\n");
    for(int i = 0;i<35;i++)
        printf("-");
    printf("\n");
}



void test(int size){
    int *memstart;
    int* mem;

    /*
    clock_t ballocstart = clock();
    for (int i =0;i<REALLOC;i++){
        mem = balloc(rand);
        fprintf(stderr, "mem: %p\n", mem);
        *mem = 12345;
        //bfree(mem);
    }
    clock_t ballocend = clock();


    clock_t mallocstart = clock();
    for(int i =0;i<REALLOC; i++){
        mem = malloc(smallrandom());
        *mem = 12345;
        *mem = *mem +1;
        free(mem);
    }
    clock_t mallocend=clock();
    */

    void *current, *init=sbrk(0);

    for(int j =0; j< TESTCYCLE; j+=TESTCYCLE/10){
        clock_t start = clock();

        for(int i =0; i<j/100; i++){
            int *memory;

            memory = balloc(size);

            if(memory == NULL){
                fprintf(stderr, "Malloc Failed!\n");
            }
            *memory = 123;
            //free(memory);
        }

        clock_t done = clock();
        printf(" %d, %f\n", j, (float)(done-start)/CLOCKS_PER_SEC);

    }
    printf("\n");
}
