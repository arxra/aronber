#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

char global[] = "This is a global string";
const int read_onlyg = 12345;

void zot(unsigned long *stop) {
  unsigned long r = 0x3;

  unsigned long *i;
  for (i = &r; i <= stop; i++) {
    printf(" %p    0x%lx\n", i, *i);
  }
}

void foo(unsigned long *stop) {
  unsigned long q = 0x2;

  zot(stop);
}

/*
 * A code segment, also known as a text segment or simply as text, is a portion
 * of an object file or the corresponding section of the program's virtual
 * adress space that contains the executable instructions
 *
 */

int main() {

  int pid = getpid();
  const int read_only = 12345;

  unsigned long p = 0x1;

  foo(&p);

back:
  // gets put in the executable part of the code segment in memory r-xp
  printf("Process id: %d\n", pid);
  printf("global string: %p\n", &global);
  printf("The code: %p\n", &&back);
  printf("read_only local: %p\n", &read_only);
  printf("read_only global: %p\n", &read_onlyg);
  printf("  p (0x%lx ): %p \n", p, &p);

  printf("\n\n /proc/%d/maps \n\n", pid);

  char command[50];
  sprintf(command, "cat /proc/%d/maps", pid);
  system(command);

  fgetc(stdin);
  return 0;
}
