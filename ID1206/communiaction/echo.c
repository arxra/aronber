#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <netinet/ip.h>

#define PORT 8080
#define MAX 512


int main() {
    int sock;
    char buffer[MAX];

    //create socket
    assert((sock = socket(AF_INET, SOCK_DGRAM, 0))!=0);

    struct sockaddr_in name;

    name.sin_family = AF_INET;
    name.sin_port = htons(PORT);
    name.sin_addr.s_addr = htonl(INADDR_ANY);

    assert(bind(sock, (struct sockaddr *) &name, sizeof(name))!= -1);
    int size = sizeof(struct sockaddr_un);

    while(1){
        int n;
        n = recvfrom(sock, buffer, MAX-1,0, (struct sockaddr *) &name, size);
        if(n==-1)
            perror("server");

        buffer[n] = 0;
        printf("Server: received: %s\n", buffer);
        printf("Server: from destinatnion %s %d\n", inet_ntoa(name.sin_addr), ntohs(name.sin_addr.s_addr));

        for(int i =0;i<n;i++)
            buffer[i] = tolower((unsigned char)buffer[i]);
    }

}
