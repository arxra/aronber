#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#define TEST "This is a TEST to SEE if iT wORKs"
#define CLIENT "help"
#define SERVER_PORT 8080
#define SERVER_IP "127.0.0.1"
#define MAX 512

int main (void){
    int n = sizeof(TEST);
    int sock;
    char buffer[MAX];

    struct sockaddr_in name;
    struct sockaddr_in server ;
    int size = sizeof(struct sockaddr_in);

    name.sin_family = AF_INET;
    name.sin_port = 0;
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    assert(bind(sock, (struct sockaddr *) &name, size)!=-1);

    server.sin_family = AF_INET;
    server.sin_port = htons(SERVER_PORT);
    server.sin_addr.s_addr = inet_addr(SERVER_IP);

    assert(sendto(sock, TEST, n, 0, (struct sockaddr *) &server, size)!=-1);
    n = recvfrom(sock, buffer, MAX-1, 0, (struct sockaddr *)&server, &size);
    if(n ==-1)
        perror("server");

    buffer[n];
    printf("Client side recieved: %s\n", buffer);
    unlink(CLIENT);
    exit(0);
}
