#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>

#define TEST "This is a TEST to SEE if iT wORKs"
#define CLIENT "help"
#define SERVER "lower"
#define MAX 512

int main (void){
    int n = sizeof(TEST);
    int sock;
    char buffer[MAX];

    unlink(CLIENT);
    struct sockaddr_un name = {AF_UNIX, CLIENT};
    struct sockaddr_un server = {AF_UNIX, SERVER};
    int size = sizeof(struct sockaddr_un);

    assert((sock = socket(AF_UNIX, SOCK_DGRAM,0))!=-1);

    assert(bind(sock, (struct sockaddr *)&name, sizeof(struct sockaddr_un))!=-1);

    assert(sendto(sock, TEST, n, 0, (struct sockaddr *) &server, size)!=-1);
    n = recvfrom(sock, buffer, MAX-1, 0, (struct sockaddr *)&server, &size);
    if(n ==-1)
        perror("server");

    buffer[n];
    printf("Client side recieved: %s\n", buffer);
    unlink(CLIENT);
    exit(0);
}
