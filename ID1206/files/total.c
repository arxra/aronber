#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

unsigned long count(char *path) {
  unsigned long total = 0;

  DIR *dirp = opendir(path);

  char subdir[1024];
  struct dirent *entry;
  struct stat file_st;

  if(dirp==NULL){
    printf("Not able to open %s\n", path);
    return 0;
  }

  while ((entry = readdir(dirp)) != NULL) {
    if ((strcmp(entry->d_name, ".") != 0) & (strcmp(entry->d_name, "..") != 0))
      switch (entry->d_type) {
      case DT_DIR:
        sprintf(subdir, "%s,%s", path, entry->d_name);
        count(entry->d_name);
        total++;
        break;
      case DT_REG:
        total++;
        break;
      default:
        break;
      }
  }
  closedir(dirp);
  return total;
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    perror("usage: total <dir>\n");
    return -1;
  }

  char *path = argv[1];
  unsigned long total = count(path);
  printf("The directory %s contains %lu files\n", path, total);
}
