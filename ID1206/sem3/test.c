#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include "green.h"

#define TESTINGDELAY 1000

int flag =0;

green_cond_t cond;

void *test(void *arg){
    int id = *(int*)arg;
    int loop = 50;
    while(loop >0){
        if(flag==id){
            printf("Thread %d: %d\n", id, loop);
            for(int i =0;i<TESTINGDELAY; )
                i++;
            loop--;
            flag = (id+1)%2;
            fprintf(stderr, "#%d:Calling signal\n",id);
            green_cond_signal(&cond);
        }else{
            fprintf(stderr, "#%d:Calling wait\n",id);
            green_cond_wait(&cond);
        }
    }
    return arg;
}

int main(){
    green_t g0, g1;
    //green_t g2;
    int a0=0;
    int a1=1;
    //int a2=2;
    cond.first = NULL;

    green_create(&g0, test, &a0);
    green_create(&g1, test, &a1);
    //green_create(&g2, test, &a2);

    assert(green_join(&g0) == 0);
    assert(green_join(&g1) == 0);
    end_timer();
    printf("All Done!\n");

    return 0;
}
