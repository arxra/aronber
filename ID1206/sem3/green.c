#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <assert.h>
#include "green.h"
#include <signal.h>
#include <sys/time.h>


#define TRUE 1
#define FALSE 0
#define STACK_SIZE (1 << 12)
#define PERIOD 1


static sigset_t block ;
void timer_handler(int);
static ucontext_t main_cntx = {0};
static green_t main_green = { &main_cntx, NULL, NULL, NULL, NULL, FALSE };

static green_t *running = &main_green;
static green_t *last = &main_green;

static void init() __attribute__ ((constructor));



void timer_handler(int sig){
    fprintf(stderr, "TIMER! ---------------------------------------\n");
    green_t *tmp = running;
    do{
        fprintf(stderr, "Queue: %p (%p)-> %p\n", (void*)tmp, (void**)tmp->arg, (void*)tmp->next);
        tmp = tmp->next;
    }while(tmp->next->next != running);
    green_yield();
}

void end_timer(){
    sigprocmask(SIG_BLOCK, &block, NULL);
}
void start_timer(){
    sigprocmask(SIG_UNBLOCK, &block, NULL);
}

void init(){
    getcontext(&main_cntx);
    fprintf(stderr, "init --------\n");
    sigemptyset(&block) ;
    sigaddset (&block,SIGVTALRM ) ;

    struct sigaction act;
    struct timeval interval;
    struct itimerval period;

    act.sa_handler = timer_handler;
    act.sa_flags = SA_SIGINFO;
    assert (sigaction (SIGVTALRM, &act, NULL) == 0 );

    interval.tv_sec = 0;
    interval.tv_usec = PERIOD;
    period.it_interval = interval;
    period.it_value = interval;

    setitimer (ITIMER_VIRTUAL, &period, NULL);
    //sigprocmask(SIG_UNBLOCK, &block, NULL);
    fprintf(stderr, "init --------\n");
}
//----------
// 		mutex
//----------
int green_mutex_init ( green_mutex_t *mutex ) {
    mutex->taken = FALSE;
    mutex->susp = NULL;
    return 0;
}

int green_mutex_lock(green_mutex_t *mutex){
    end_timer();
    last->next = running->next;
    while ( mutex->taken == TRUE) {
        if(mutex->susp == NULL)
            mutex->susp = running;
        else{
            green_t *tmp = mutex->susp;
            while(tmp->next != NULL)
                tmp = tmp->next;
            tmp->next = running;
        }
        running->next = NULL;
        //Yield starts the timer, we don't want to be interrupten in between.
        green_yield();
    }
    //takethelock
    mutex->taken = TRUE;

    start_timer();
    return 0;
}

int green_mutex_unlock(green_mutex_t *mutex){
    end_timer();
    if(!mutex->taken){
        start_timer();
        return 0;}

    last->next = mutex->susp;
    mutex->susp = mutex->susp->next;
    last->next->next = running;

    mutex->taken = FALSE;
    start_timer();
    return 0;
}

// -------------
//      Conditionals
// -------------
// Here we create a new conditonal list.
void green_cond_init(green_cond_t *conds){
    conds->first = NULL;
}

void green_cond_wait(green_cond_t *conds){
    end_timer();
    if(running == running->next){
        green_yield();
    }

    green_t *curr = running;
    last->next = running->next;

    if(conds->first == NULL){
        conds->first = running;
    } else {
        green_t *tmp = conds->first;
        while(tmp->next != NULL){
            tmp = tmp->next;
        }
        tmp->next = running;
    }
    running->next = NULL;
    running = last->next;
    start_timer();
    swapcontext(curr->context, running->context);
}

void green_cond_signal(green_cond_t *conds){
    if(conds->first == NULL)
        return;

    end_timer();

    green_t *tmp = running->next;
    last = running;
    running->next = conds->first;
    conds->first = conds->first->next;
    running = running->next;
    running->next = tmp;

    start_timer();
    swapcontext(last->context, running->context);
}

// -------------
//      Threads
// ------------
int green_yield(){
    sigprocmask(SIG_BLOCK, &block , NULL ) ;
    if(running == running->next)
        return 0;
    last = running;

    running = running->next;
    while((running->join != NULL && running->join->zombie == FALSE) || running->zombie)
        running = running->next;
    sigprocmask (SIG_UNBLOCK, &block , NULL );
    swapcontext(last->context, running->context);
    return 0;
}

int green_join(green_t *thread){
    while(thread->zombie == FALSE)
        green_yield();

    return 0;
}

void green_thread(){
    green_t *this = running;

    (*this->fun)(this->arg);


    // If there's somoeone waiting for me, assign it as the next thread in the ready queue.
    // This requiers us to find the last next item of joined item and setting it's next to
    // or previous next
    green_t* tmp = this->join;
    if(this->join != NULL){
        while (tmp->next != NULL && tmp->next->zombie == FALSE)
            tmp = tmp->next;
        tmp->next = this->next;
        this->next = this->join;
        this->join = NULL;
    }

    //Free our allocated memory structures.
    //This is our stack, allocated on the heap.
    free(this->context->uc_stack.ss_sp);

    // we zombie now. Stack is missing.
    this->zombie = TRUE;

    //Next thread to run?
    tmp = this->next;
    // Don't start a thread who's waiting for another living thread
    while(tmp->join != NULL && tmp->join->zombie == TRUE)
        tmp = tmp->next;
    tmp->join = NULL;

    last = running;
    running = tmp;
    setcontext(running->next->context);
}

//Creating thread
int green_create(green_t *new, void *(*fun)(void *), void *arg){
    fprintf(stderr, "Creating Thread: ");
    ucontext_t *cntx = (ucontext_t *)malloc(sizeof(ucontext_t));
    getcontext(cntx);

    void *stack = malloc(STACK_SIZE);

    cntx->uc_stack.ss_sp = stack;
    cntx->uc_stack.ss_size = STACK_SIZE;

    makecontext(cntx, green_thread, 0);
    new->context = cntx;
    new->fun = fun;
    new->arg = arg;
    new->next = NULL;
    new->join = NULL;
    new->zombie = FALSE;

    green_t *tmp = running->next;
    running->next = new;
    if(tmp == NULL)
        new->next = running;
    else
        new->next = tmp;

    fprintf(stderr, "OK!\n");
    return 0;
}
