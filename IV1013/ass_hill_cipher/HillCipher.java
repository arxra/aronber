import java.io.*;
import java.util.ArrayList;


public class HillCipher {
    public static class IntegerParseAlphaExcseption extends Exception{
        public IntegerParseAlphaExcseption(String cha){
            super("Not a char in legal range: " + cha);
        }
    }

    public static class NotValidRadixException extends Exception{

        public NotValidRadixException(int radixCode){
            super("Not a valid radix: " + radixCode);
        }
    }

    public static class NotValidBlockSizeException extends Exception{

        public NotValidBlockSizeException(int i){
            super("Not a valid blocksize: " + i);
        }
    }

    public static class NotValidKeyfileException extends Exception{

        public NotValidKeyfileException(String string){
            super("Not a valid keyfile: " + string);
        }
    }

    static int radix(String s) throws NotValidRadixException{
        int check = 0;
        try {
            check = Integer.parseInt(s);
        } catch (NumberFormatException e ){
            System.out.println(e);
        }
        //list of valid radix
        ArrayList validRadix = new ArrayList<>();
        validRadix.add(26);
        validRadix.add(128);
        validRadix.add(256);

        if(!validRadix.contains(check)){
            throw new NotValidRadixException(check);
        }
        return check;
    }

    static int blocksize (String s) throws NumberFormatException,NotValidBlockSizeException{
        int check = 0;
        try {
            check = Integer.parseInt(s);
        } catch (NumberFormatException e ){
            System.out.println(e);
        }
        if(check <  1)
            throw new NotValidBlockSizeException(check);
        return check;
    }

    static String keyfile (String s, int size) throws NotValidKeyfileException, FileNotFoundException{
        FileReader fr;
        BufferedReader br;
        try{
            fr = new FileReader(s);
        }catch (Exception e){
            throw new FileNotFoundException(s);
        }
        try{
            br = new BufferedReader(fr);
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            int lines =0;

            while (line != null) {
                lines++;
                sb.append(line);
                sb.append(System.lineSeparator());
                String[] numbers = line.split(" ");
                if(numbers.length != size)
                    throw new NotValidBlockSizeException(numbers.length);
                line = br.readLine();
            }

            br.close();
            if (lines != size)
                throw new NotValidKeyfileException(s + " with #"+ lines);
            

            return sb.toString();

        } catch (Exception e){
            throw new NotValidKeyfileException(s + " - Could not read file.");
        } 
    }

    static int[][] setKey(String s, int j) throws NotValidBlockSizeException{
        if(j < 1)
            throw new NotValidBlockSizeException(j);

        int[][] key = new int[j][j];
        String[][] keyString = new String[j][j];
        String[] smallString = new String[j];

        //System.out.println(s);

        smallString = s.split("\\r?\\n");

        for(int i = 0; i < j; i++){
            keyString[i] = smallString[i].split(" ");
        }

        try{
            for(int i = 0; i < j; i++){
                for(int ii = 0; ii < j; ii++){
                    key[i][ii] = Integer.parseInt(keyString[i][ii]);
                }
            }
            return key;
        } catch(Exception e){
            throw new NumberFormatException();
        }
    }

    static String readMessage(String fn) throws IOException, FileNotFoundException{
        FileReader fr;
        BufferedReader br;

        try{
            fr = new FileReader(fn);
        }catch (Exception e){
            throw new FileNotFoundException(fn);
        }
        try{
            br = new BufferedReader(fr);
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }

            br.close();
            return sb.toString();
        } catch (Exception e){
            throw new IOException(fn + " - Could not read file.");
        } 
    }

    static short[] messageToInt(String s) throws IntegerParseAlphaExcseption{
        char b = 'A';
        short[] message = new short[s.length()];
        short tm;
        
        
        for(int i =0; i< s.length(); i++){
            tm = (short)(s.charAt(i) -b);
            if(tm < 0 || tm > ('Z'-b)){
                throw new IntegerParseAlphaExcseption(s.charAt(i) + " to " + tm);
            }else {
                //System.out.println("Parsed " + s.charAt(i) + " to " + tm);
                message[i] = tm;
            }
        }
        return message;
    }

    static short[] truncateMessafe (short[] mes, int bsize){
        int mod = mes.length % bsize;
        for(int i = mes.length; i > mes.length-mod; i--)
            mes = new short[mes.length-1];
        return mes;
    }

    static String messageToString(short[] s) {
        char b = 'A';
        String ans = "";
        
        for(int i =0; i< s.length; i++){
            ans += (char)(s[i] + b);
        }

        return ans;
    }


    static short[] cypher(short[] text, int[][] key, int rad){
        short[] answer = new short[text.length];
        short[] tmp = new short [key.length];


        for (int i = 0; i < text.length; i +=key.length){
            for(int ii=0; ii < key.length; ii++){
                tmp[ii] = 0;}
            for(int ii = 0; ii < key.length; ii++){
                //System.out.print("Text: " + text[ii+i] + " ");
                for(int iii = 0; iii < key.length; iii++){
                    tmp[ii] += text[i+iii] * key[ii][iii];
                }
            }
            //add tmp to answer
            for(int ii = 0; ii < key.length; ii++){
                answer[i+ii] = (short)(tmp[ii] % rad);
                //System.out.print("answer: " + "(" + tmp[ii] + ")"+answer[ii+i] + " ");
            }
        //System.out.println();
        }
        return answer;
    }

    static void printAnswers(String ans, String fn){
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(fn));
            bw.write(ans);
            bw.close();
        }catch (IOException e){
            System.out.println(e);
        }
    }


    public static void main(String[] args){
        int rad = 0; //26, 128 or 256
        int bsize = 0; // 1-MAXINT, keyfile dimensions
        String keyread = ""; 
        short[] message;

        try{
            rad = radix(args[0]);
            bsize = blocksize(args[1]);
            keyread = keyfile(args[2], bsize);
        }
        catch(Exception e) {
            System.out.println(e);
            return;
        }

        int[][] key = new int[bsize][bsize];
        try{
            key = setKey(keyread, bsize);
        } catch (Exception e){
            System.out.println("Failed to parse keyfile.");
            return;
        }
        
        try{
            message = new short[args[3].length()];
            message = messageToInt(readMessage(args[3]));
            //System.out.println(message);//DEBUG
        } catch (Exception e){
            System.out.println(e);
            return;
        }

        message = cypher(message, key, rad);
        //System.out.println("Message length: " + message.length);
        String mes = messageToString(truncateMessafe(message, bsize));

        if(args.length < 5){
            System.out.println(mes);
        } else {
            printAnswers(mes, args[4]);
        }

        //System.out.println("Program exited correctly.");

    }
}
