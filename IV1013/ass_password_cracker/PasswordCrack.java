import java.io.*;
import java.util.*;

public class PasswordCrack {

   static class saltHash {
      public saltHash(String _salt, String _hash) {
         salt = _salt;
         hash = _hash;
         both = _salt + _hash;
      }

      String salt;
      String hash;
      String both;
   }

   private static String[] myDickT() {
      String[] myD = { "123456", "password", "12345678", "12345", "111111", "1234567", "sunshine", "qwerty", "iloveyou",
            "princess", "admin", "welcome", "666666", "abc123", "football", "123123", "monkey", "654321", "!@#$%^&*",
            "charlie", "aa123456", "donald", "password1", "qwerty123", "pussy", "secret", "dragon", "welcome", "ginger",
            "sparky", "helpme", "blowjob", "nicole", "justin", "camaro", "johnson", "yamaha", "midnight", "chris" };
      return myD;
   }

   private static String readFile(String fn) throws FileNotFoundException, Exception {
      FileReader fr;
      BufferedReader br;

      try {
         fr = new FileReader(fn);
      } catch (Exception e) {
         throw new FileNotFoundException(fn);
      }

      StringBuilder sb = new StringBuilder();
      try {
         br = new BufferedReader(fr);
         String line = br.readLine();

         while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
         }
         br.close();
      } catch (Exception e) {
         throw new Exception(e);
      } finally {
         try {
            fr.close();
         } catch (Exception e) {
            throw new Exception(e);
         }
      }

      return sb.toString();
   }

   public static String[] retriveHashes(String passwd) {
      List<String> hashes = new ArrayList<String>();
      String[] ans = { "" };
      for (String line : passwd.split(System.lineSeparator())) {
         hashes.add(line.split(":")[1]);
      }
      return hashes.toArray(ans);
   }

   private static String[] permute(String inp) {
      List<String> arr = new ArrayList<String>();

      for (int i = 0; i <= 9; i++) {
         arr.add(((char) 48 + i) + inp);
         arr.add(inp + ((char) 48 + i));
      }
      for (int i = 65; i <= 90; i++) {
         arr.add(((char) i) + inp);
         arr.add(inp + ((char) i));
      }
      for (int i = 97; i <= 122; i++) {
         arr.add(((char) i) + inp);
         arr.add(inp + ((char) i));
      }

      if (inp.length() > 1)
         arr.add(inp.substring(1));
      if (inp.length() > 1)
         arr.add(inp.substring(0, inp.length() - 1));

      String rev = new StringBuilder(inp).reverse().toString();
      arr.add(rev);
      arr.add(inp + inp);
      arr.add(inp + rev);
      arr.add(rev + inp);

      arr.add(inp.toUpperCase());

      // Capitilize string
      if (inp.charAt(0) - 97 >= 0) {
         if (inp.length() > 1)
            arr.add(((char) inp.charAt(0) - 32) + inp.substring(1, inp.length()));
         else
            arr.add(((char) (inp.charAt(0) - 32)) + "");
      }

      // nCAPITILIZE, borrows rev variable
      if (inp.length() > 1) {
         rev = inp.toLowerCase();
         arr.add(rev.charAt(0) + inp.substring(1, inp.length()).toUpperCase());
      }

      // toggle case of string
      rev = inp;
      for (int i = 0; i < inp.length(); i++) {
         if (rev.charAt(i) - 90 <= 0)
            rev = inp.substring(0, i) + (char) (rev.charAt(i) + 32)
                  + (i >= inp.length() - 1 ? "" : inp.substring(i + 1, rev.length()));
         else if (rev.charAt(i) - 97 >= 0)
            rev = inp.substring(0, i) + (char) (rev.charAt(i) - 32)
                  + (i >= inp.length() - 1 ? "" : inp.substring(i + 1, rev.length()));
      }
      StringBuilder s = new StringBuilder();
      for (int i = 0; i < inp.length(); i += 2) {
         if (Character.isUpperCase(inp.charAt(i)))
            s.append(Character.toLowerCase(inp.charAt(i)));
         else if (Character.isLowerCase(inp.charAt(i)))
            s.append(Character.toUpperCase(inp.charAt(i)));
      }
      arr.add(s.toString());
      for (int i = 1; i < inp.length(); i += 2) {
         if (Character.isUpperCase(inp.charAt(i)))
            s.append(Character.toLowerCase(inp.charAt(i)));
         else if (Character.isLowerCase(inp.charAt(i)))
            s.append(Character.toUpperCase(inp.charAt(i)));
      }
      arr.add(s.toString());
      arr.add(rev);

      String[] ans = new String[arr.size()];

      return arr.toArray(ans);
   }

   private static ArrayList<String> passwdNames(String s) {
      ArrayList<String> arr = new ArrayList<>();
      for (String line : s.split(System.lineSeparator())) {
         for (String word : line.split(":")[4].split(" ")) {
            arr.add(word);
         }
         arr.add(line.split(":")[0]);
      }
      return arr;
   }

   private static boolean ispermutation(saltHash sh, String pass) {
      return (sh.both.equals(jcrypt.crypt(sh.salt, pass))) ? true : false;
   }

   public static void main(String[] args) {
      // args[0] = dictionary
      // args[1] = passwd
      List<saltHash> raw = new ArrayList<saltHash>();
      String dict = "";
      String passwdentries = "";

      try {// Read files
         dict = readFile(args[0]);
         passwdentries = readFile(args[1]);
         ;
      } catch (Exception e) {
         return;
      }

      List<String> arr = new ArrayList<String>(); // Wordlist should be an ArrayList
      for (String s : dict.split(System.lineSeparator()))
         arr.add(s);

      for (String s : myDickT())
         arr.add(s);

      // add peoples names
      for (String s : passwdNames(passwdentries))
         arr.add(s);

      // save the hash and salt seperatly
      for (String line : retriveHashes(passwdentries)) {
         raw.add(new saltHash(line.substring(0, 2), line.substring(2)));
      }

      // One mangle and origianl words
      int rawsize = 0;
      int ii = 0;
      for (String dictent : arr) {
         for (rawsize = raw.size(), ii = 0; ii < rawsize; ii++) {
            if (ispermutation(raw.get(ii), dictent)) {
               raw.remove(ii);
               ii--;
               rawsize--;
               System.out.println(dictent);
            }
         }
      }

      System.err.println("Finished going through dict, starting first mangle.... # stderrprint");
      for (String dictent : arr) {
         for (String man : permute(dictent)) {
            for (rawsize = raw.size(), ii = 0; ii < rawsize; ii++) {
               if (ispermutation(raw.get(ii), man)) {
                  raw.remove(ii);
                  ii--;
                  rawsize--;
                  System.out.println(man);
               }
            }
         }
      }

      System.err.println("Finished first mangle, string double loop. This might take forever.... # stderrprint");
      for (String dictent : arr) {
         for (String man1 : permute(dictent)) {
            for (String man : permute(man1)) {
               for (rawsize = raw.size(), ii = 0; ii < rawsize; ii++) {
                  if (ispermutation(raw.get(ii), man)) {
                     raw.remove(ii);
                     ii--;
                     rawsize--;
                     System.out.println(man);
                  }
               }
            }
         }
      }
   }
}
